export const environment = {
  production: false,
  demo: true,
  dev: false,
  BASE_API: 'https://demo.compassoneacuity.com/ask.v2.api/',
  BASE_URL: 'https://demo.compassoneacuity.com/connect/ask.v2'
};
