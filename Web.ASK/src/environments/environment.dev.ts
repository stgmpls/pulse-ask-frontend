export const environment = {
  production: false,
  demo: false,
  dev: true,
  BASE_API: 'https://dev.compassoneacuity.com/connect/',
  BASE_URL: 'https://dev.compassoneacuity.com/connect/ask'
};
