import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPage } from './core/components';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: './survey-setup/survey-setup.module#SurveySetupPageModule',
    data: { preload: true }
  },
  {
    path: 'login',
    loadChildren: './login/login-page.module#LoginPageModule',
    data: { preload: true }
  },
  {
    path: 'user',
    loadChildren: './survey-taking/survey-taking.module#SurveyTakingPageModule',
    data: { preload: true }
  },
  {
    path: 'report',
    loadChildren: './reporting/reporting.module#ReportingModule',
    data: { preload: true }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
