import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'create-report',
    loadChildren:
      './pages/create-report/create-report.module#CreateReportPageModule',
    data: { preload: true }
  },
  {
    path: 'user-locations',
    loadChildren:
      './pages/user-locations/user-locations.module#UserLocationsPageModule',
    data: { preload: true }
  },
  {
    path: 'survey-report',
    loadChildren:
      './pages/survey-report/survey-report.module#SurveyReportPageModule',
    data: { preload: true }
  },
  {
    path: '',
    redirectTo: 'survey-report'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportingRoutingModule {}
