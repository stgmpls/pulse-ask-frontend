import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SurveyReportPage } from './survey-report.page';
import { CoreModule } from '../../../core';

const routes: Routes = [
  {
    path: '',
    component: SurveyReportPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoreModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SurveyReportPage]
})
export class SurveyReportPageModule {}
