import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

import {
  HelperService,
  NetworkService,
  AuthenticationService,
  SurveyService
} from '../../../core/services';
import { Location } from '../../../core/entities';
import { TIMEZONE as timeZone } from '../../../../app/core/constant/constants';
import { SurveyReportService } from '../../services/survey-report.service';
import { SurveySetupService } from '../../../survey-setup/survey-setup.service';

import * as moment from 'moment-timezone';
import * as CryptoJS from 'crypto-js';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-survey-report',
  templateUrl: './survey-report.page.html',
  styleUrls: ['./survey-report.page.scss']
})
export class SurveyReportPage implements OnInit, OnDestroy {
  sectorId: string;
  location: Location;
  themeName: string;
  listOfSurveys: any = [];
  dbSurveyId: any = -1;
  public secret = 'report@11';
  selectedSurveyFromList: any;
  emotions: any;
  width: any = '30%';
  locationId: number | string = null;
  surveyId: number | string = null;
  selectedEmotionIndex = -1;
  costCenter: string;
  bgColor = ['#23ed75', '#45e6d6', '#e2df20', '#6184d6', '#fe3a3b'];

  @ViewChild('pieCanvas') pieCanvas;
  @ViewChild('barCanvas') barCanvas;

  @ViewChild('toDateTime') toDateTime;
  @ViewChild('fromDateTime') fromDateTime;

  pieChart: any;
  barChart: any;

  labels = [];
  piecChartlabels = [];
  pieChartData = [];
  emotionType = [];

  barChartData = [];
  barChartLabel = [];

  today = new Date();
  hours = 2;
  locationName = '';
  totalCount;
  selectedSurveyId: number | string = null;
  public foodTypes: Array<any> = [];
  resp: any;
  isMobile: boolean;
  startTime = moment();
  endTime = moment();
  isInternal: boolean;
  timer;
  surveySegment: any;
  selectedFoodType = '';
  public secretForReport = 'report@11';
  noData = false;
  startDateString = '';
  previousStartDateString = '';
  endDateString = '';
  previousEndDateString = '';
  minimumDateString = '';
  maximumDateString = '';
  areDatesValid = true;
  numberOfCharacters = 12;
  roleId: number;
  username: string;
  distinctQuestions: Array<any> = [];
  selectedQuestion = '';
  isSingleQuestionSurvey = false;
  emotionArray: Array<any> = [];
  followUpResponses: Array<any> = [];
  selectedCategory = '';
  pieChartDataSum = 0;
  constructor(
    private helper: HelperService,
    private platform: Platform,
    private activatedRoute: ActivatedRoute,
    private surveyReportService: SurveyReportService,
    private networkMgr: NetworkService,
    private authService: AuthenticationService,
    private router: Router,
    private surveyService: SurveyService,
    private surveySetupService: SurveySetupService
  ) {
    // By retrieving the route parameters, in the constructor, ngOnInit will not execute until the route parameter is retrieved
    // https://stackoverflow.com/questions/42323644/angular2-activatedroute-parameter-is-undefined/42324758#42324758
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.activatedRoute.params.subscribe(routeParams => {
        this.processParams(routeParams, queryParams);
      });
    });
  }

  processParams(routeParams, queryParams) {
    const params = { ...queryParams, ...routeParams };
    const bytes = CryptoJS.AES.decrypt(params.reportData, this.secretForReport);
    const reportData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

    this.surveyId = reportData['surveyId'];
    this.locationId = reportData['locationId'];
    this.isInternal = reportData['isInternal'];
    this.location = reportData['locationData'];
    this.selectedSurveyId = this.surveyId;
    if (this.locationId && this.surveyId) {
      if (this.location) {
        this.costCenter = this.location.LocationDescription;
      } else {
        if (this.networkMgr.isAvailiable && this.networkMgr.isConnected) {
          this.surveyReportService
            .searchCostCenter(this.locationId.toString(), '', '', '', '')
            .subscribe(data => {
              if (data.Data && data.Data.length > 0) {
                this.costCenter = data.Data[0].LocationDescription;
              }
            });
        }
      }
      clearTimeout(this.timer);
      this.setDataInteval();
      this.getSurveyResult();
    }
  }
  ngOnInit() {
    this.setDatePicker();
    this.isMobile = this.platform.is('mobile');
    this.sectorId = this.location && this.location.SectorAdamId;

    this.roleId = Number(localStorage.getItem('roleId'));
    this.username = localStorage.getItem('username');
  }
  ionViewWillLeave() {
    clearTimeout(this.timer);
  }

  setDatePicker() {
    this.minimumDateString = moment()
      .subtract(1, 'year')
      .format('YYYY-MM-DD');
    this.maximumDateString = moment().format('YYYY-MM-DD');

    if (this.previousStartDateString === '') {
      this.startDateString = moment()
        .subtract(1, 'day')
        .format('YYYY-MM-DD');
    } else if (this.previousStartDateString !== this.startDateString) {
      this.startDateString = this.previousStartDateString;
    }

    if (this.previousEndDateString === '') {
      this.endDateString = moment().format('YYYY-MM-DD');
    } else if (this.previousEndDateString !== this.endDateString) {
      this.endDateString = this.previousEndDateString;
    }
  }

  getSurveyName(survey) {
    if (survey.FeedbackSurveyTypeId == 1) {
      return (
        survey.surveyTitle + ' (Predefined - ' + survey.feedbackSurveyId + ')'
      );
    } else if (survey.FeedbackSurveyTypeId == 2) {
      return (
        survey.surveyTitle + ' (Tasting - ' + survey.feedbackSurveyId + ')'
      );
    } else if (survey.FeedbackSurveyTypeId == 3) {
      return survey.surveyTitle + ' (Custom - ' + survey.feedbackSurveyId + ')';
    }
  }

  getSurveyResult() {
    if (this.networkMgr.isAvailiable && this.networkMgr.isConnected) {
      this.surveyReportService
        .getLocationSurveysForReport(this.locationId)
        .subscribe(data => {
          const arr = data.Data.map(function(val) {
            return {
              surveyTitle: val.Description,
              feedbackSurveyId: val.SurveyId,
              category: val.Category,
              categoryName: val.CategoryName
            };
          });
          const filteredArr = [];
          arr.forEach(function(element) {
            if (
              filteredArr.find(
                x =>
                  x.surveyTitle === element.surveyTitle &&
                  x.feedbackSurveyId === element.feedbackSurveyId
              ) == undefined
            ) {
              filteredArr.push(element);
            }
          });
          this.listOfSurveys = filteredArr;
          if (!this.listOfSurveys || this.listOfSurveys.length == 0) {
            this.helper.showAlert(
              'No Survey configured for selected location.'
            );
          }
          this.getSurveyReport(false);
        });
    } else {
      this.helper.showAlert(
        'No internet connection available. Please check your settings.'
      );
    }
  }

  setDataInteval() {
    this.timer = setInterval(() => {
      this.getSurveyReport(true);
    }, 30000);
  }

  goback() {
    this.router.navigate(['home/searchcostcenter']);
  }

  checkDatesValidity() {
    const fromDate = moment(this.startDateString).startOf('day');
    const endDate = moment(this.endDateString).startOf('day');

    const minimumDateMessage = moment(this.minimumDateString).format(
      'DD-MMMM-YYYY'
    );
    const maximumDateMessage = moment(this.maximumDateString).format(
      'DD-MMMM-YYYY'
    );
    if (
      !fromDate.isBetween(
        this.minimumDateString,
        this.maximumDateString,
        null,
        '[]'
      )
    ) {
      this.helper.showAlert(
        'Invalid From date. From date should be greater than ' +
          minimumDateMessage +
          ' and less than ' +
          maximumDateMessage +
          '. From and To dates reset to default date.'
      );
      this.areDatesValid = false;
      return;
    }

    if (
      !endDate.isBetween(
        fromDate.format('YYYY-MM-DD'),
        this.maximumDateString,
        null,
        '[]'
      )
    ) {
      this.helper.showAlert(
        'Invalid To date. To date should be greater than ' +
          minimumDateMessage +
          ' and less than ' +
          maximumDateMessage +
          '. From and To dates reset to default date.'
      );
      this.areDatesValid = false;
      return;
    }
    this.areDatesValid = true;
  }

  getSurveyReport(canHideSpinner) {
    const format = 'hh:mm:ss';
    const now = moment(),
      breakFastTime = moment('05:30:00', format),
      lunchTime = moment('10:30:00', format),
      dinnerTime = moment('15:00:00', format),
      nextDayBreakFastTime = moment('05:30:00', format).add(1, 'days');

    if (!this.surveySegment) {
      if (now.isBetween(breakFastTime, lunchTime)) {
        this.surveySegment = 'Breakfast';
      } else if (now.isBetween(lunchTime, dinnerTime)) {
        this.surveySegment = 'Lunch';
      } else if (now.isAfter(dinnerTime)) {
        this.surveySegment = 'Dinner';
      } else {
        this.surveySegment = 'Cumulative';
      }
    }

    if (this.surveySegment === 'Breakfast') {
      this.startTime = breakFastTime;
      this.endTime = lunchTime;
      this.areDatesValid = true;
    } else if (this.surveySegment === 'Lunch') {
      this.startTime = lunchTime;
      this.endTime = dinnerTime;
      this.areDatesValid = true;
    } else if (this.surveySegment === 'Dinner') {
      this.startTime = dinnerTime;
      this.endTime = nextDayBreakFastTime;
      this.areDatesValid = true;
    } else if (this.surveySegment === 'Cumulative') {
      this.checkDatesValidity();
      this.startTime = moment(this.startDateString);
      this.startTime.set({ hour: 5, minute: 30, seconds: 0, milliseconds: 0 });
      this.endTime = moment(this.endDateString).add(1, 'days');
      this.endTime.set({ hour: 5, minute: 29, seconds: 59, milliseconds: 999 });
    }

    if (this.areDatesValid) {
      this.previousStartDateString = this.startDateString;
      this.previousEndDateString = this.endDateString;
      clearTimeout(this.timer);
      this.setDataInteval();
      const start = this.startTime
        .clone()
        .tz(timeZone)
        .format('YYYY-MM-DD HH:mm');
      const end = this.endTime
        .clone()
        .tz(timeZone)
        .format('YYYY-MM-DD HH:mm');
      this.surveyReportService
        .getSurveyReport(
          this.locationId,
          this.surveyId,
          start,
          end,
          canHideSpinner
        )
        .subscribe(
          data => {
            const response = data.Data;
            if (response) {
              this.resp = response;
              this.totalCount = this.resp.TotalCount;
              this.locationName = this.resp.LocationName;
              this.selectedSurveyId = this.resp.SurveyId;
              if (this.resp.FoodTypes && this.resp.FoodTypes.length > 0) {
                this.foodTypes = this.resp.FoodTypes.map(e => e['FoodType'])
                  .map((e, i, final) => final.indexOf(e) === i && i)
                  .filter(e => this.resp.FoodTypes[e])
                  .map(e => this.resp.FoodTypes[e]);

                if (this.foodTypes.length > 1) {
                  this.foodTypes.push({ FoodType: 'All' });
                }

                if (this.foodTypes.length > 0) {
                  if (this.foodTypes.length == 1) {
                    this.selectedFoodType = this.foodTypes[0].FoodType;
                  } else if (this.selectedFoodType == '') {
                    this.selectedFoodType = 'All';
                  }
                }
              } else {
                this.foodTypes = [];
              }
              this.handleFollowupResponses();
              if (this.resp && this.resp.SurveyName) {
                this.getReport();
              }

              this.loadPiechart();
              if (this.selectedEmotionIndex == -1) {
                this.loadBarChartForAll();
              } else {
                this.loadBarChartData(this.selectedEmotionIndex);
              }
            }
          },
          error => {
            clearTimeout(this.timer);
          }
        );
    } else {
      this.startDateString = moment()
        .subtract(1, 'day')
        .format();
      this.endDateString = moment().format();
      if (this.timer) {
        clearTimeout(this.timer);
      }
    }
  }

  private handleFollowupResponses() {
    this.selectedCategory = this.listOfSurveys.find(
      x => x.feedbackSurveyId === this.selectedSurveyId
    ).categoryName;
    this.distinctQuestions = [];
    this.followUpResponses = [];
    this.emotionArray = [];
    if (this.resp.FollowUpResponses && this.resp.FollowUpResponses.length > 0) {
      this.distinctQuestions = Array.from(
        new Set(
          this.resp.FollowUpResponses.filter(item => item.ItemText !== '').map(
            item => item.ItemText
          )
        )
      );
      if (this.distinctQuestions.length > 0) {
        if (this.distinctQuestions.length === 1) {
          this.selectedQuestion = this.distinctQuestions[0];
          this.isSingleQuestionSurvey = true;
        } else {
          this.isSingleQuestionSurvey = false;
        }
        if (this.selectedQuestion === '') {
          this.selectedQuestion = this.distinctQuestions[0];
        }
        this.followUpResponses = this.resp.FollowUpResponses.filter(
          x => x.ItemText === this.selectedQuestion
        );
        this.followUpResponses.forEach(element => {
          if (
            this.emotionArray.find(x => x.EmotionId === element.EmotionId) ===
            undefined
          ) {
            if (element.EmotionId > 0) {
              this.emotionArray.push({
                EmotionId: element.EmotionId,
                EmotionType: element.EmotionType,
                Count: element.Count
              });
            }
          } else {
            const item = this.emotionArray.find(
              x => x.EmotionId === element.EmotionId
            );
            item.Count = item.Count + element.Count;
          }
        });
      }
    }
  }

  private groupBy(list, keyGetter) {
    const map = new Map();
    list.forEach(item => {
      const key = keyGetter(item);
      const collection = map.get(key);
      if (!collection) {
        map.set(key, [item]);
      } else {
        collection.push(item);
      }
    });
    return map;
  }

  getReport() {
    let emotionArr = [];
    let allEmotions = [];
    if (this.foodTypes.length == 0) {
      emotionArr = this.emotionArray;
    } else {
      allEmotions = this.resp.FoodTypes.map(function(val) {
        return { EmotionType: val.EmotionType, Count: 0 };
      }).filter((value, index, self) => {
        return (
          self.findIndex(object => object.EmotionType === value.EmotionType) ===
          index
        );
      });

      emotionArr = this.resp.FoodTypes.filter(
        obj =>
          obj.FoodType ==
          (this.selectedFoodType == 'All'
            ? obj.FoodType
            : this.selectedFoodType)
      ).map(function(val) {
        return {
          FoodType: val.FoodType,
          EmotionType: val.EmotionType,
          Count: val.Count
        };
      });

      this.emotionArray.forEach((item, index) => {
        const emotion = emotionArr.find(
          obj => obj.EmotionType === item.EmotionType
        );
        if (!emotion) {
          emotionArr.push({
            EmotionType: item.EmotionType,
            Count: 0,
            EmotionId: item.EmotionId
          });
        }
      });
    }
    let data = [];
    data = emotionArr.filter(obj => obj.Count == 0);
    if (data.length == emotionArr.length) {
      this.noData = true;
    } else {
      this.noData = false;
    }
    const grouped = this.groupBy(emotionArr, emotion => emotion.EmotionType);
    const groupIterator = grouped.entries();
    let i = 0;
    for (i = 0; i < grouped.size; i++) {
      let arr = [];
      const key = groupIterator.next().value[0];
      arr = grouped.get(key);
      if (arr) {
        let count = 0;
        for (let counter = 0; counter < arr.length; counter++) {
          count = count + arr[counter].Count;
        }
        if (arr.length > 0) {
          const newElement = {
            EmotionType: arr[0].EmotionType,
            Count: count
          };
          const filteredAry = emotionArr.filter(
            x => x.EmotionType !== arr[0].EmotionType
          );
          filteredAry.push(newElement);
          emotionArr = filteredAry;
        }
      }
    }

    const modifiedArr = [];
    if (emotionArr.length < allEmotions.length) {
      allEmotions.forEach((item, index) => {
        const i = emotionArr.findIndex(
          object => object.EmotionType === item.EmotionType
        );
        if (i == -1) {
          modifiedArr[index] = item;
        } else {
          modifiedArr[index] = emotionArr[i];
        }
      });
    }
    if (modifiedArr.length > 0) {
      emotionArr = modifiedArr;
    }
    this.bgColor = [];
    this.labels = [];
    this.emotionType = [];
    this.pieChartData = [];
    this.piecChartlabels = [];
    emotionArr.forEach((item, index) => {
      this.labels.push(item.EmotionType);
      if (item.EmotionType && item.EmotionType.includes('VERY')) {
        const emotion = item.EmotionType.split('VERY');
        if (emotion.length == 2) {
          this.piecChartlabels.push('Very ' + this.capitalize(emotion[1]));
        }
      } else if (item.EmotionType) {
        this.piecChartlabels.push(this.capitalize(item.EmotionType));
      }
      this.pieChartData.push(item.Count);
      this.emotionType.push(item.EmotionType);
      switch (item.EmotionType) {
        case 'Very Positive':
          this.bgColor.push('#23ed75');
          break;
        case 'Positive':
          this.bgColor.push('#45e6d6');
          break;
        case 'Satisfied':
          this.bgColor.push('#e2df20');
          break;
        case 'Negative':
          this.bgColor.push('#6184d6');
          break;
        case 'Very Negative':
          this.bgColor.push('#fe3a3b');
          break;
        default:
          this.bgColor.push('#23ed75');
      }
    });

    this.calculatePieChartDataSum();
  }

  public chartClicked(element: any): void {
    this.barChartLabel = [];
    this.barChartData = [];
    if (element && element.length > 0) {
      this.loadBarChartData(element[0]._index);
    }
  }

  loadBarChartData(index) {
    this.selectedEmotionIndex = index;
    clearTimeout(this.timer);
    this.setDataInteval();
    const emotionType = this.emotionType[index];
    this.barChartLabel = [];
    this.barChartData = [];
    const res = this.getFollowUpResponses();

    const allFollowUpResponses = this.filterAllFollowUpResponses();

    const followUpResp = res.filter(t => t.EmotionType == emotionType);

    if (followUpResp.length > 0) {
      this.generateBarChartLabel(allFollowUpResponses);

      const uniqueFollowUpQuestions = this.getUniqueFollowUpQuestions(
        allFollowUpResponses
      );

      uniqueFollowUpQuestions.forEach(item => {
        this.barChartData.push(0);
      });

      uniqueFollowUpQuestions.forEach((item, index) => {
        const sameTextQuestions = followUpResp.filter(
          obj => obj.FollowupResponse == item.FollowupResponse
        );
        let totalCount = 0;
        sameTextQuestions.forEach((item, index) => {
          totalCount = totalCount + item.Count;
        });

        this.barChartData[index] = totalCount;
      });
    }
    this.loadBarChart(index);
  }

  loadBarChartForAll() {
    const res = this.getFollowUpResponses();

    const datasets = [];
    this.barChartLabel = [];

    const allFollowUpResponses = this.filterAllFollowUpResponses();

    this.generateBarChartLabel(allFollowUpResponses);

    this.emotionType.forEach((id, index) => {
      let dataset: any;
      const followUpResp = res.filter(t => t.EmotionType == id);
      dataset = {
        label: '',
        data: [],
        backgroundColor: this.bgColor[index],
        borderColor: this.bgColor[index],
        borderWidth: 1
      };

      const uniqueFollowUpQuestions = this.getUniqueFollowUpQuestions(
        allFollowUpResponses
      );

      uniqueFollowUpQuestions.forEach(item => {
        dataset.data.push(0);
      });

      uniqueFollowUpQuestions.forEach((item, index) => {
        const sameTextQuestions = followUpResp.filter(
          obj => obj.FollowupResponse == item.FollowupResponse
        );
        let totalCount = 0;
        sameTextQuestions.forEach((item, index) => {
          totalCount = totalCount + item.Count;
        });
        dataset.data[index] = totalCount;
      });
      datasets.push(dataset);
    });

    this.loadBarChart(0);
    const questionLabels = this.barChartLabel.map(function(value) {
      return value.FollowupResponse;
    });
    let filteredLabels = questionLabels.filter((value, index, self) => {
      return self.indexOf(value) === index;
    });
    filteredLabels = this.splitStringIterator(filteredLabels);

    this.barChart.data = {
      labels: filteredLabels,
      datasets: datasets
    };
    this.barChart.update();
  }

  splitString(string) {
    const re = new RegExp('.{1,' + this.numberOfCharacters + '}', 'g');
    const result = string.match(re) || [];
    result.forEach(function(value, index, array) {
      const originalValue = value;
      value = value.trim();
      if (array.length > index + 1) {
        if (!originalValue.endsWith(' ') && !array[index + 1].startsWith(' ')) {
          value = value + '-';
        }
      }
      array[index] = value;
    });
    return result;
  }

  loadBarChart(index) {
    if (this.barChart) {
      const questionLabels = this.barChartLabel.map(function(value) {
        return value.FollowupResponse;
      });
      let filteredLabels = questionLabels.filter((value, index, self) => {
        return self.indexOf(value) === index;
      });
      filteredLabels = this.splitStringIterator(filteredLabels);

      this.barChart.data = {
        labels: filteredLabels,
        datasets: [
          {
            label: '',
            data: this.barChartData,
            backgroundColor: this.bgColor[index],
            borderColor: this.bgColor[index],
            borderWidth: 1
          }
        ]
      };
      this.barChart.update();
      return;
    }
    const questionLabelsForNewChart = this.barChartLabel.map(function(value) {
      return value.FollowupResponse;
    });
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: 'bar',
      data: {
        labels: questionLabelsForNewChart.filter((value, index, self) => {
          return self.indexOf(value) === index;
        }),
        datasets: [
          {
            label: '',
            data: this.barChartData,
            backgroundColor: this.bgColor[index],
            borderColor: this.bgColor[index],
            borderWidth: 1
          }
        ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        scales: {
          yAxes: [
            {
              stacked: true,
              ticks: {
                min: 0,
                callback: function(value, index, values) {
                  if (Math.floor(value) === value) {
                    return value;
                  }
                }
              }
            }
          ],
          xAxes: [
            {
              stacked: true,
              barPercentage: 0.3,
              ticks: {
                autoSkip: false
              }
            }
          ]
        }
      },
      plugins: [
        {
          beforeInit: chart => {
            chart.data.labels = this.splitStringIterator(chart.data.labels);
          }
        }
      ]
    });
  }

  loadPiechart() {
    if (typeof this.pieChart !== 'undefined') {
      this.pieChart.destroy();
    }
    this.pieChart = new Chart(this.pieCanvas.nativeElement, {
      type: 'doughnut',
      data: {
        labels: this.piecChartlabels,
        datasets: [
          {
            label: '',
            data: this.pieChartData,
            backgroundColor: this.bgColor,
            borderColor: this.bgColor,
            borderWidth: 1
          }
        ]
      },
      options: {
        onClick: (e, element) => {
          this.chartClicked(element);
        },
        legend: {
          display: false
        },
        responsive: true,
        maintainAspectRatio: false
      }
    });
  }

  allData() {
    this.selectedEmotionIndex = -1;
    clearTimeout(this.timer);
    this.setDataInteval();
    this.getSurveyReport(false);
  }

  ionViewWillUnload() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  }

  capitalize(input) {
    return !!input
      ? input
          .split(' ')
          .map(function(wrd) {
            return wrd.charAt(0).toUpperCase() + wrd.substr(1).toLowerCase();
          })
          .join(' ')
      : '';
  }

  gotoURLGenerator() {
    const createReportData = {
      locationId: this.location.LocationId,
      surveyId: this.surveyId,
      isInternal: false,
      locationData: this.location
    };

    const createReportDataEncrypted = CryptoJS.AES.encrypt(
      JSON.stringify(createReportData),
      this.secretForReport
    ).toString();

    this.router.navigate([
      'report/create-report',
      { reportData: createReportDataEncrypted }
    ]);
  }

  onCuisineChange() {
    this.getReport();
    this.loadPiechart();
    if (this.selectedEmotionIndex == -1) {
      this.loadBarChartForAll();
    } else {
      this.loadBarChartData(this.selectedEmotionIndex);
    }
  }

  changeSurveyId() {
    this.clearFilters();
    const reportData = {};
    const survey = this.listOfSurveys.find(
      x => x.feedbackSurveyId == this.selectedSurveyId
    );
    if (
      survey &&
      survey.feedbackSurveyId &&
      this.surveyId != survey.feedbackSurveyId.toString()
    ) {
      this.selectedCategory = survey.categoryName;
      reportData['locationId'] = this.locationId;
      reportData['surveyId'] = this.selectedSurveyId;
      reportData['isInternal'] = this.isInternal;
      reportData['locationData'] = this.location;

      const reportDataEncrypted = CryptoJS.AES.encrypt(
        JSON.stringify(reportData),
        this.secretForReport
      ).toString();

      this.router.navigate([
        '/report/survey-report',
        { reportData: reportDataEncrypted }
      ]);
    }
  }

  private clearFilters() {
    this.selectedQuestion = '';
    this.selectedFoodType = '';
    this.selectedCategory = '';
    this.foodTypes = [];
    this.distinctQuestions = [];
  }

  sync() {
    this.surveySetupService
      .sync(this.username, this.sectorId, this.roleId)
      .then(() => {
        this.helper.showAlert('Data sync successfully.');
      });
  }

  logout() {
    this.authService.logout();
    this.surveyService.surveyId = 0;
    localStorage.setItem('latestSurveyId', '0');
    this.router.navigate(['/login']);
  }

  onQuestionChange() {
    this.handleFollowupResponses();
    this.getReport();
    this.loadPiechart();
    if (this.selectedEmotionIndex === -1) {
      this.loadBarChartForAll();
    } else {
      this.loadBarChartData(this.selectedEmotionIndex);
    }
  }

  calculatePieChartDataSum() {
    let total = 0;
    this.pieChartData.forEach(function(element) {
      total = total + element;
    });
    this.pieChartDataSum = total;
  }

  ngOnDestroy() {
    clearTimeout(this.timer);
  }

  getFollowUpResponses() {
    return this.followUpResponses.filter(
      obj =>
        obj.FoodType ==
          (this.selectedFoodType === 'All'
            ? obj.FoodType
            : this.selectedFoodType) && obj.FollowupResponse !== ''
    );
  }

  filterAllFollowUpResponses() {
    return this.followUpResponses
      .filter(x => x.FollowupResponse !== '')
      .map(function(item) {
        return {
          FollowupResponse: item.FollowupResponse,
          EmotionType: item.EmotionType
        };
      })
      .filter((value, index, self) => {
        return (
          self.findIndex(
            object =>
              object.FollowupResponse === value.FollowupResponse &&
              object.EmotionType === value.EmotionType
          ) === index
        );
      });
  }

  generateBarChartLabel(allFollowUpResponses) {
    allFollowUpResponses.forEach((item, index) => {
      this.barChartLabel.push({
        FollowupResponse: item.FollowupResponse,
        EmotionType: item.EmotionType
      });
    });
  }

  getUniqueFollowUpQuestions(allFollowUpResponses) {
    return allFollowUpResponses.filter((value, index, self) => {
      return (
        self.findIndex(
          object => object.FollowupResponse === value.FollowupResponse
        ) === index
      );
    });
  }

  splitStringIterator(inputData) {
    inputData.forEach((value, index, array) => {
      let a = [];
      a = this.splitString(value);
      array[index] = a;
    });
    return inputData;
  }
}
