import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EmailService } from './email.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-email',
  templateUrl: './email-form.component.html',
  styleUrls: ['./email-form.component.scss']
})
export class EmailFormComponent {
  emailForm = new FormGroup({
    emailAddress: new FormControl('', [
      Validators.required,
      Validators.pattern(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
    ]),
    subject: new FormControl(
      this.emailService.emailData.subject,
      Validators.required
    ),
    mailMessage: new FormControl(
      this.emailService.emailData.mailMessage,
      Validators.required
    )
  });

  constructor(
    public emailService: EmailService,
    public toastController: ToastController
  ) {}

  sendEmailMessage() {
    this.emailService.sendEmailMessage(this.emailForm.value).subscribe(
      res => {
        if (res.Data) {
          this.presentToast('Email sent successfully');
          this.closeModal();
        } else {
          this.presentToast('Error occured while sending Email');
        }
      },
      err => {
        console.error(err);
        this.presentToast('Error occured while sending Email');
      }
    );
  }

  async presentToast(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  closeModal() {
    this.emailService.modal.dismiss();
  }
}
