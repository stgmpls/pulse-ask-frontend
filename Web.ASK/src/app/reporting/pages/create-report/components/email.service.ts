import { Injectable } from '@angular/core';
import { APIRequest } from '../../../../core/entities';
import { APIMethod, DataService } from '../../../../core/services';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Injectable({ providedIn: 'root' })
export class EmailService {
  modal: any;
  emailData: any = {};
  constructor(private dataService: DataService) {}

  public sendEmailMessage(mailObj) {
    const emailMessage = {
      ToEmailAddress: String(mailObj.emailAddress),
      Subject: String(mailObj.subject),
      Body: String(mailObj.mailMessage)
    };
    const request: APIRequest = new APIRequest(
      'api/mail/sendEmail',
      APIMethod.POST
    );
    request.addAll(JSON.stringify(emailMessage));
    request.setContentType('application/json');
    return this.dataService.execute(request);
  }
}
