import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './../../../core/services/authentication.service';
import { NavController, ModalController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { SurveyService } from 'src/app/core/services';
import { environment } from 'src/environments/environment';
import { Location } from '@angular/common';
import { EmailFormComponent } from './components/email-form.component';
import { EmailService } from './components/email.service';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-create-report',
  templateUrl: './create-report.page.html',
  styleUrls: ['./create-report.page.scss']
})
export class CreateReportPage implements OnInit {
  reportUrl: string;
  public secret = 'Compass@11';
  public secretForReport = 'report@11';
  subject = 'Please find survey details';
  mailto = '';
  parameters: any;
  emailModal: any;
  constructor(
    private authService: AuthenticationService,
    public activatedRoute: ActivatedRoute,
    public surveyService: SurveyService,
    private router: Router,
    private location: Location,
    private modalCtrl: ModalController,
    private emailService: EmailService
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.parameters = params;
    });

    const reportURLTree = this.router.createUrlTree(['/report'], {
      queryParams: this.parameters
    });

    const bytes = CryptoJS.AES.decrypt(
      this.parameters.reportData,
      this.secretForReport
    );
    const reportData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    const surveyId = reportData['surveyId'];
    this.reportUrl = environment.BASE_URL + reportURLTree.toString();

    this.mailto += `<span style="font-size:14px;word-break: break-all;">Hi,<br/><br/>Please find survey details
    below (SID ${surveyId}):<br/><br/>
    URL for report:  <a>${this.reportUrl}</a><br/><br/>  Thanks <br> Acuity Sentiment Kiosk</span>`;
  }

  logout() {
    this.authService.logout();
    this.surveyService.surveyId = 0;
    localStorage.setItem('latestSurveyId', '0');
    // [ts] Property 'setRoot' does not exist on type 'NavController
    this.router.navigate(['login']);
  }
  generateQR() {}

  back() {
    // [ts] Property 'pop' does not exist on type 'NavController
    this.location.back();
  }

  public async onEmail() {
    this.emailService.emailData.mailMessage = this.mailto;
    this.emailService.emailData.subject = this.subject;
    this.emailService.modal = await this.modalCtrl.create({
      component: EmailFormComponent,
      cssClass: 'email-modal',
      backdropDismiss: false
    });
    await this.emailService.modal.present();
  }
}
