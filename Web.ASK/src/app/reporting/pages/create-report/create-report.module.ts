import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CreateReportPage } from './create-report.page';
import { EmailFormComponent } from './components/email-form.component';
import { EmailService } from './components/email.service';

const routes: Routes = [
  {
    path: '',
    component: CreateReportPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CreateReportPage, EmailFormComponent],
  providers: [EmailService],
  entryComponents: [EmailFormComponent]
})
export class CreateReportPageModule {}
