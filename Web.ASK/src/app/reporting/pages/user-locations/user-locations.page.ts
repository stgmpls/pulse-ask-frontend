import { Component, OnInit } from '@angular/core';

import { ILocationSearch, Location, User } from '../../../core/entities';

import {
  HelperService,
  NetworkService,
  AuthenticationService
} from '../../../core/services';

@Component({
  selector: 'app-user-locations',
  templateUrl: './user-locations.page.html'
})
export class UserLocationsPage implements OnInit {
  constructor() {}

  ngOnInit() {}
}
