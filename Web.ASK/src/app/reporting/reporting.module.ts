import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { ReportingRoutingModule } from './reporting-routing.module';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../core';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    IonicModule,
    ReportingRoutingModule,
    FormsModule,
    CoreModule
  ]
})
export class ReportingModule {}
