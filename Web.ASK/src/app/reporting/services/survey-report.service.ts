import { Injectable } from '@angular/core';
import { DataService, APIMethod } from '../../core/services/data.service';
import { APIRequest } from '../../core/entities/apirequest';

@Injectable({
  providedIn: 'root'
})
export class SurveyReportService {
  constructor(private dataService: DataService) {}

  searchCostCenter(number, city, state, name, zip_code) {
    const params: string =
      'api/location/searchLocations?sectorAdamId=&costCenterNumber=' +
      number +
      '&city=' +
      city +
      '&state=' +
      state +
      '&costCenterDescription=' +
      name +
      '&zipCode=' +
      zip_code;
    const request: APIRequest = new APIRequest(params, APIMethod.GET);
    return this.dataService.executeAPI(request);
  }

  getLocationSurveys(locationId) {
    const params: string =
      'api/Survey/GetFeedbackSurveysByLocationId?locationId=' + locationId;
    const request: APIRequest = new APIRequest(params, APIMethod.GET);
    return this.dataService.executeAPI(request);
  }

  getSurveyReport(locationId, surveyId, startTime, endTime, canHideSpinner) {
    const params = `api/survey/getSurveyResponseReport?locationId=${locationId}&surveyId=${surveyId}
    &startTime=${startTime}&endTime=${endTime}`;
    const request: APIRequest = new APIRequest(
      params,
      APIMethod.GET,
      canHideSpinner
    );
    return this.dataService.executeAPI(request);
  }

  syncClientLogs(logs) {
    const request: APIRequest = new APIRequest(
      'api/Survey/SyncAskClientLogs',
      APIMethod.POST
    );
    request.addAll(logs);
    return this.dataService.executeAPI(request);
  }

  public getLocationSurveysForReport(locationId) {
    const params: string =
      'api/Survey/SurveysForReportByLocationId/' + locationId;
    const request: APIRequest = new APIRequest(params, APIMethod.GET);
    return this.dataService.executeAPI(request);
  }
}
