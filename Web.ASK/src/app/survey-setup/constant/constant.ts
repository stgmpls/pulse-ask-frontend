export enum SURVEY_TYPE {
  PREDEFINED = 'Predefined',
  TASTING = 'Tasting',
  CUSTOM = 'Custom',
  ACUITY = 'Acuity Connect Content'
}

export const SURVEY_TYPE_CONFIG = [
  {
    name: 'SETUP PRE DEFINED FORM',
    type: SURVEY_TYPE.PREDEFINED,
    expanded: false
  },
  {
    name: 'SETUP TASTING EVENT',
    type: SURVEY_TYPE.TASTING,
    expanded: false
  },
  {
    name: 'SETUP CUSTOM FORM',
    type: SURVEY_TYPE.CUSTOM,
    expanded: false
  },
  {
    name: 'ACUITY CONNECT FOR ASK',
    type: SURVEY_TYPE.ACUITY,
    expanded: false
  }
];

export enum WIZARD_ITEM_TYPES {
  COST_CENTER = 'COST-CENTER',
  SETUP_SURVEY = 'SETUP-SURVEY',
  ADD_QUESTION = 'ADD-QUESTION'
}

export const WIZARD_CONFIG = [
  {
    name: 'Select Cost Center',
    type: WIZARD_ITEM_TYPES.COST_CENTER
  },
  {
    name: 'Setup Form',
    type: WIZARD_ITEM_TYPES.SETUP_SURVEY
  },
  {
    name: 'Add Question',
    type: WIZARD_ITEM_TYPES.ADD_QUESTION
  }
];
