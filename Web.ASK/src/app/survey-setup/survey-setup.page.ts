import { Component, OnInit } from '@angular/core';
import { SurveySetupService } from './survey-setup.service';
import { SurveyService } from '../core/services';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { LocationService } from '../core/services/location.service';
import { HelperService } from '../core/services';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-survey-setup',
  templateUrl: 'survey-setup.page.html',
  styleUrls: ['survey-setup.page.scss']
})
export class SurveySetupPage implements OnInit {
  public loading: any;
  public location: any;
  public secretForReport = 'report@11';
  public surveyId: number;
  roleId: number;
  sectorId: string;
  username: string;
  public isLocalStorgeSet = false;

  constructor(
    private surveySetupService: SurveySetupService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private surveyService: SurveyService,
    private locationService: LocationService,
    private helperService: HelperService
  ) {
    this.roleId = Number(localStorage.getItem('roleId'));
    this.sectorId = localStorage.getItem('sectorId');
    this.username = localStorage.getItem('username');
  }

  ngOnInit() {
    this.surveyService.getLocation().then((data: any) => {
      if (data.rows.length > 0) {
        this.location = data.rows.item(0);
        if (!this.sectorId) {
          this.getLocationById(this.location);
        } else {
          this.isLocalStorgeSet = true;
        }
      } else {
        this.isLocalStorgeSet = true;
      }
    });
    const surveyIdLocalStorage = localStorage.getItem('latestSurveyId');
    if (surveyIdLocalStorage) {
      this.surveyService
        .getSurveyById(Number(surveyIdLocalStorage))
        .then((data: any) => {
          if (data.rows.length > 0) {
            if (data.rows.item(0).survey_category !== 'ACCO') {
              this.surveyId = data.rows.item(0).survey_id;
            } else {
              this.getLatestAskSurvey();
            }
          }
        });
    } else {
      this.getLatestAskSurvey();
    }
  }

  private getLatestAskSurvey() {
    this.surveyService.getSurvey().then((data: any) => {
      if (data.rows.length > 0) {
        this.surveyId = data.rows.item(0).survey_id;
      }
    });
  }

  logout() {
    this.surveySetupService.logout().then((data: any) => {
      this.surveyService.surveyId = 0;
      localStorage.setItem('latestSurveyId', '0');
      const lastSyncTime = localStorage.getItem('lastSyncTime');
      localStorage.clear();
      localStorage.setItem('lastSyncTime', lastSyncTime);
      this.router.navigate(['/login']);
    });
  }

  sync() {
    this.surveySetupService
      .sync(this.username, this.sectorId, this.roleId)
      .then(() => {
        this.showLoader('Data sync successfully.');
      });
  }

  gotoReport() {
    this.surveyService.getLocation().then((data: any) => {
      if (data.rows.length > 0) {
        this.location = data.rows.item(0);
        const reportData = {};

        reportData['locationId'] = this.location.LocationId;
        reportData['surveyId'] = this.surveyId;
        reportData['isInternal'] = true;
        reportData['locationData'] = this.location;
        const reportDataEncrypted = CryptoJS.AES.encrypt(
          JSON.stringify(reportData),
          this.secretForReport
        ).toString();
        const reportURLTree = this.router.createUrlTree(['/report'], {
          queryParams: { reportData: reportDataEncrypted }
        });
        this.router.navigateByUrl(reportURLTree.toString());
      }
    });
  }

  async showLoader(message = '') {
    this.loading = await this.loadingCtrl.create({
      message: message ? message : 'Coming Soon..........',
      duration: 2000
    });
    await this.loading.present();
  }

  public getLocationById(location) {
    this.locationService
      .searchCostCenter(location.LocationId, '', '', '', '')
      .subscribe(data => {
        if (data.Data.length === 0) {
          this.helperService.showAlert(
            'No locations found. Please select Location'
          );
        } else {
          this.setLocationDataInStorage(data.Data[0]);
        }
      });
  }

  private setLocationDataInStorage(location) {
    this.locationService
      .getAskAdminRole(location.SectorAdamId)
      .subscribe(response => {
        if (response.Data && response.Data.RoleID > 0) {
          localStorage.setItem('roleId', response.Data.RoleID);
          localStorage.setItem('theme', 'theme-' + location.SectorAdamId);
          localStorage.setItem('sectorId', location.SectorAdamId);
          localStorage.setItem('selectedLocation', JSON.stringify(location));
          localStorage.setItem('selectedLocationId', location.LocationId);
          this.isLocalStorgeSet = true;
        } else {
          this.isLocalStorgeSet = true;
          this.showLocationError();
        }
      });
  }

  async showLocationError() {
    this.loading = await this.loadingCtrl.create({
      message:
        'Invalid location! ASK admin role is not configured for this sector.',
      duration: 3000
    });
    await this.loading.present();
  }
}
