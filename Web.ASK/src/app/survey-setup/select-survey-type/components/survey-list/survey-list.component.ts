import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.scss']
})
export class SurveyListComponent {
  @Input() data = [];
  @Input() dataField = '';
  @Input() dataKey = '';
  @Output() selectSurvey: EventEmitter<any> = new EventEmitter();
  @Input() isACSurvey = false;

  onClickItem(e, item) {
    this.selectSurvey.emit(item);
    e.stopPropagation();
  }
}
