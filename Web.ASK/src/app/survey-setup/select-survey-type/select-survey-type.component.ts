import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';

@Component({
  selector: 'select-survey-type',
  templateUrl: './select-survey-type.component.html',
  styleUrls: ['./select-survey-type.component.scss']
})
export class SelectSurveyTypeComponent implements OnDestroy {
  constructor() {}
  @Input() items = [];
  @Input() data = [];
  @Input() itemTitleField = '';
  @Input() itemKey = '';
  @Input() dataField = '';
  @Input() dataKey = '';
  @Input() isACSurvey = false;

  @Output() selectSurvey: EventEmitter<any> = new EventEmitter();
  @Output() selectSurveyType: EventEmitter<any> = new EventEmitter();
  private activeItem;

  expandItem(item) {
    this.selectSurveyType.emit(item.type);
    this.activeItem = {};
    if (!item.expanded) {
      this.activeItem = item;
    }
    this.expand(item);
  }

  expand(item) {
    this.items.forEach(listItem => {
      if (item === listItem) {
        listItem.expanded = !listItem.expanded;
      } else {
        listItem.expanded = false;
      }
      return listItem;
    });
  }

  onSelectSurvey(survey) {
    this.selectSurvey.emit(survey);
  }

  ngOnDestroy() {
    this.expand(this.activeItem);
  }
}
