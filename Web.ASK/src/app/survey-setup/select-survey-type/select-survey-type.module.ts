import { NgModule } from '@angular/core';
import { SurveyListComponent } from './components/survey-list/survey-list.component';
import { SelectSurveyTypeComponent } from './select-survey-type.component';
import { AccordionModule } from '../../core/components';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [SelectSurveyTypeComponent, SurveyListComponent],
  exports: [SelectSurveyTypeComponent, SurveyListComponent],
  imports: [AccordionModule, IonicModule, CommonModule]
})
export class SelectSurveyTypeModule {}
