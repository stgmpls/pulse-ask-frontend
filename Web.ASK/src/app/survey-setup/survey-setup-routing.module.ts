import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SurveySetupPage } from './survey-setup.page';

const routes: Routes = [
  {
    path: '',
    component: SurveySetupPage,
    children: [
      {
        path: 'searchcostcenter',
        children: [
          {
            path: '',
            loadChildren:
              './survey-location/survey-location.module#SurveyLocationPageModule'
          }
        ]
      },
      {
        path: 'setupsurvey',
        children: [
          {
            path: '',
            loadChildren:
              './setup-survey/setup-survey.module#SetupSurveyPageModule',
            data: { preload: true }
          },
          {
            path: ':id',
            loadChildren:
              './setup-survey/setup-survey.module#SetupSurveyPageModule',
            data: { preload: true }
          }
        ]
      },
      {
        path: '',
        redirectTo: 'searchcostcenter',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'searchcostcenter',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SurveySetupModule {}
