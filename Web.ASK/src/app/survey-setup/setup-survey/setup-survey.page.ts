import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import {
  HelperService,
  NetworkService,
  SurveyService
} from '../../core/services';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { SurveySetupService } from '../survey-setup.service';
import {
  SURVEY_TYPE_CONFIG,
  WIZARD_CONFIG,
  WIZARD_ITEM_TYPES,
  SURVEY_TYPE
} from '../constant/constant';
import { SurveyComponent } from '../../core/components';
import { Device } from '@ionic-native/device/ngx';

@Component({
  selector: 'app-setup-survey',
  templateUrl: './setup-survey.page.html',
  styleUrls: ['./setup-survey.page.scss']
})
export class SetupSurveyPage implements OnInit, OnDestroy {
  locationSubscription: Subscription;
  public showSearchCostCenter: boolean;
  public isLocationAlreadyPresent: boolean;
  public typeOfSurvey = '';
  surveySegmentSubscription: Subscription;
  showSearchCostCenterSubscription: Subscription;
  surveyTypeSubscription: Subscription;
  showSurveyListSubscription: Subscription;
  configSurveySubscription: Subscription;
  surveysByCategorySubscription: Subscription;
  private readonly newSegment = 'new';
  public surveySegment = 'new';
  roleId: number;
  sectorId: string;
  username: string;
  deviceTypeCode = 'T';
  predefinedMasterSurveys: any = [];
  tastingMasterSurveys: any = [];
  customMasterSurveys: any = [];
  surveys: any = [];
  public surveysByCategory: any = [];
  public showSurveyList: boolean;
  surveyId: any;
  public configSurvey: any;
  public loading: any;
  private readonly setupSurveyPagePathEditMode = 'setupsurvey/';
  private readonly setupSurveyPagePath = '/home/setupsurvey';
  private readonly searchCostCenterPath = '/home/searchcostcenter';
  private readonly locationSegment = 'location';
  private readonly deviceSegment = 'device';
  public isSurveyAtThisDevice = false;
  public surveyItems: any = [];
  public surveyTypeConfig = SURVEY_TYPE_CONFIG;
  public listOfSurvey = [];
  public wizardInfo = WIZARD_CONFIG;
  public activeWizardItemType = '';
  public surveyType = SURVEY_TYPE;
  public isSurveyActive = false;
  public acuityConnectMasterSurveys = [];
  private readonly userPagePath = '/user';

  @ViewChild('appSurvey') appSurveyComponent: SurveyComponent;

  ngOnInit() {
    this.initialize();
  }

  private initialize() {
    this.surveyItems = this.surveyService.surveyData;
    const categories = [];
    this.surveySetupService.selectSurveysByCategory(categories);
    this.surveySetupService.selectSurveySegment(this.newSegment);
    this.route.params.subscribe((data: any) => {
      if (data.id) {
        this.surveyId = data.id;
        this.surveyService.isEditMode = true;
        this.setActiveWizardItemTypeQuestion();
        this.getSurveyById(this.surveyId);
      } else {
        this.surveyId = 0;
        this.surveyService.isEditMode = false;
        this.setActiveWizardItemTypeSurvey();
      }
    });
    if (this.surveyId === 0) {
      this.getAllSurveys();
    }
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.unsubscribeFromServiceVariables();
  }
  constructor(
    private helperService: HelperService,
    private networkMgr: NetworkService,
    private route: ActivatedRoute,
    private router: Router,
    private surveySetupService: SurveySetupService,
    private surveyService: SurveyService,
    private loadingCtrl: LoadingController,
    private device: Device
  ) {
    this.fetchDataFromLocalStorage();
    this.subscribeToServiceVariables(surveySetupService);
  }

  private unsubscribeFromServiceVariables() {
    if (this.surveySegmentSubscription) {
      this.surveySegmentSubscription.unsubscribe();
    }
    if (this.locationSubscription) {
      this.locationSubscription.unsubscribe();
    }
    if (this.showSearchCostCenterSubscription) {
      this.showSearchCostCenterSubscription.unsubscribe();
    }
    if (this.surveyTypeSubscription) {
      this.surveyTypeSubscription.unsubscribe();
    }

    if (this.showSurveyListSubscription) {
      this.showSurveyListSubscription.unsubscribe();
    }
    if (this.configSurveySubscription) {
      this.configSurveySubscription.unsubscribe();
    }

    if (this.surveysByCategorySubscription) {
      this.surveysByCategorySubscription.unsubscribe();
    }
  }

  private fetchDataFromLocalStorage() {
    this.roleId = Number(localStorage.getItem('roleId'));
    this.sectorId = localStorage.getItem('sectorId');
    this.username = localStorage.getItem('username');
  }

  private subscribeToServiceVariables(surveySetupService: SurveySetupService) {
    this.surveySegmentSubscription = surveySetupService.surveySegment$.subscribe(
      surveySegment => {
        this.surveySegment = surveySegment;
        this.isSurveyAtThisDevice = false;
        if (this.surveySegment === 'device') {
          this.isSurveyAtThisDevice = true;
        }
        /* if any segment other than 'setup new' has been selected, empty 'surveyItems' array
         which contains items for that survey which was selected earlier. */
        if (this.surveySegment !== this.newSegment) {
          this.surveyService.surveyData = [];
          this.surveyItems = this.surveyService.surveyData;
        }
      }
    );
    this.locationSubscription = surveySetupService.isLocationSelected$.subscribe(
      isLocationAlreadyPresent => {
        this.isLocationAlreadyPresent = isLocationAlreadyPresent;
      }
    );
    this.showSearchCostCenterSubscription = surveySetupService.showSearchCostCenter$.subscribe(
      showSearchCostCenter => {
        this.showSearchCostCenter = showSearchCostCenter;
      }
    );
    this.surveyTypeSubscription = surveySetupService.typeOfSurvey$.subscribe(
      typeOfSurvey => {
        this.typeOfSurvey = typeOfSurvey;
        if (this.typeOfSurvey === SURVEY_TYPE.PREDEFINED) {
          this.surveysByCategory = this.predefinedMasterSurveys;
        } else if (this.typeOfSurvey === SURVEY_TYPE.TASTING) {
          this.surveysByCategory = this.tastingMasterSurveys;
        } else if (this.typeOfSurvey === SURVEY_TYPE.CUSTOM) {
          this.surveysByCategory = this.customMasterSurveys;
        } else if (this.typeOfSurvey === SURVEY_TYPE.ACUITY) {
          this.surveysByCategory = this.acuityConnectMasterSurveys;
        }
        this.surveySetupService.selectSurveysByCategory(this.surveysByCategory);
      }
    );

    this.showSurveyListSubscription = surveySetupService.showSurveyList$.subscribe(
      showSurveyList => {
        this.showSurveyList = showSurveyList;
      }
    );
    this.configSurveySubscription = surveySetupService.configSurvey$.subscribe(
      configSurvey => {
        this.configSurvey = configSurvey;
      }
    );

    this.surveysByCategorySubscription = this.surveySetupService.surveysByCategory$.subscribe(
      surveysByCategory => {
        this.listOfSurvey = this.prepareListOfSurveys(surveysByCategory);
      }
    );
  }

  public getAllSurveys() {
    if (this.networkMgr.isAvailiable) {
      // Get all surveys of selected role and sector id.
      this.surveyService
        .getAllSurveys(
          this.roleId,
          this.sectorId,
          this.deviceTypeCode,
          true,
          null
        )
        .subscribe(data => {
          this.surveys = data.Data;
          if (this.surveys.length > 0) {
            this.setMasterSurveysByCategory(this.surveys);
            this.insertSurveys(this.surveys, 0);
          }
        });
    } else {
      this.helperService.showAlert(
        'No internet connection available. Please check your settings.'
      );
    }
  }

  public async insertSurveys(surveyList, surveyCount) {
    this.surveyService
      .insertSurveys(
        surveyList,
        surveyCount,
        this.username,
        this.sectorId,
        this.roleId
      )
      .then(
        result => {
          if (surveyCount + 1 === surveyList.length) {
            this.router.navigate([
              this.userPagePath,
              surveyList[surveyCount].Header.SurveyId
            ]);
          }
        },
        error => {
          console.log(error.message); // TO DO :- Replace this code with logger service.
        }
      );
  }

  public back() {
    this.surveyService.surveyData = [];
    this.surveyItems = this.surveyService.surveyData;
    if (this.isSurveyActive) {
      if (this.surveyService.editSurveyPlace) {
        this.surveySetupService.selectSurveySegment(
          this.surveyService.editSurveyPlace
        );
        this.NavigateToCostCenterPath();
      } else {
        this.isSurveyActive = false;
        this.surveySetupService.selectTypeOfSurvey('');
        this.surveySetupService.selectSurveysByCategory([]);
        this.setActiveWizardItemTypeSurvey();
      }
      this.surveyService.editSurveyPlace = '';
    } else {
      this.surveyService.editSurveyPlace = '';
      this.NavigateToCostCenterPath();
    }
  }

  private NavigateToCostCenterPath() {
    this.router.navigate([this.searchCostCenterPath]);
  }

  public showCostCenterSearch(showSearchCostCenter) {
    this.showSearchCostCenter = showSearchCostCenter;
  }

  private prepareListOfSurveys(surveysByCategory) {
    const listOfSurvey = [];
    for (let i = 0; i < surveysByCategory.length; i++) {
      const survey = {
        surveyId: surveysByCategory[i].Header.SurveyId,
        originalSurveyId: surveysByCategory[i].Header.OriginalSurveyID,
        description: surveysByCategory[i].Header.Description
      };
      listOfSurvey.push(survey);
    }
    return listOfSurvey;
  }

  onSelectSurveyType(typeOfSurvey) {
    this.surveySetupService.selectTypeOfSurvey(typeOfSurvey);
  }

  onSelectSurvey(survey) {
    if (this.typeOfSurvey === this.surveyType.ACUITY) {
      this.launchACSurvey(survey);
      return;
    }
    this.surveyService.originalSurveyId = survey.originalSurveyId;
    this.surveyService.surveyId = survey.surveyId;
    this.surveyService.surveyDescription = survey.description;
    this.isSurveyActive = true;
    this.setActiveWizardItemTypeQuestion();
  }

  setActiveWizardItemTypeSurvey() {
    this.activeWizardItemType = WIZARD_ITEM_TYPES.SETUP_SURVEY;
  }

  setActiveWizardItemTypeQuestion() {
    this.activeWizardItemType = WIZARD_ITEM_TYPES.ADD_QUESTION;
  }

  // set predefined, tasting and custom master surveyss
  setMasterSurveysByCategory(surveys) {
    this.predefinedMasterSurveys = [];
    this.tastingMasterSurveys = [];
    this.customMasterSurveys = [];
    this.acuityConnectMasterSurveys = [];
    this.surveys.map(survey => {
      if (survey.Header.Category === SURVEY_TYPE.PREDEFINED) {
        this.predefinedMasterSurveys.push(survey);
      } else if (survey.Header.Category === SURVEY_TYPE.TASTING) {
        this.tastingMasterSurveys.push(survey);
      } else if (survey.Header.Category === SURVEY_TYPE.CUSTOM) {
        this.customMasterSurveys.push(survey);
      } else if (survey.Header.Category === SURVEY_TYPE.ACUITY) {
        this.acuityConnectMasterSurveys.push(survey);
      }
    });

    this.surveyService.setPredefinedMasterSurveys(this.predefinedMasterSurveys);
    this.surveyService.setTastingMasterSurveys(this.tastingMasterSurveys);
  }

  // find master survey from categoryType
  getMasterSurveyByCategory(categories, surveys, categoryType) {
    const surveyByCategory = categories.find(x => x.Category === categoryType);
    if (surveyByCategory) {
      const masterSurveysByCatogry = surveys.filter(
        value =>
          -1 !== surveyByCategory.Surveys.indexOf(value.Header.SurveyId) &&
          value.Header.SurveyId === value.Header.OriginalSurveyID
      );
      return masterSurveysByCatogry;
    }
    return [];
  }

  getSurveyById(surveyId) {
    this.surveyService.getSurveyById(surveyId).then((response: any) => {
      if (response && response.rows && response.rows.length > 0) {
        const survey = response.rows.item(0);
        const surveyContent = JSON.parse(survey.survey_content);
        const selectdSurvey = {
          originalSurveyId: surveyContent.OriginalSurveyID,
          surveyId: surveyContent.SurveyId,
          description: surveyContent.Description
        };
        this.surveySetupService.selectTypeOfSurvey(survey.survey_category);
        this.onSelectSurvey(selectdSurvey);
      }
    });
  }

  launchACSurvey(survey) {
    this.router.navigate([this.userPagePath, survey.surveyId]);
  }
}
