import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CoreModule } from '../../core';
import { SetupSurveyPage } from './setup-survey.page';
import { LocationOrSetupSurveyModule } from '../components/location-or-setup-survey/location-or-setup-survey.module';
import { CurrentLocationModule } from '../components/current-location/current-location.module';
import { SurveySegmentModule } from '../components/survey-segment/survey-segment.module';
import { LaunchAskBtnModule } from '../components/launch-ask-btn/launch-ask-btn.module';
import { SelectSurveyTypeModule } from '../select-survey-type/select-survey-type.module';
import { CustomSurveyModule } from '../custom-survey/custom-survey.module';

const routes: Routes = [
  {
    path: '',
    component: SetupSurveyPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoreModule,
    RouterModule.forChild(routes),
    LocationOrSetupSurveyModule,
    CurrentLocationModule,
    SurveySegmentModule,
    LaunchAskBtnModule,
    SelectSurveyTypeModule,
    CustomSurveyModule
  ],
  declarations: [SetupSurveyPage]
})
export class SetupSurveyPageModule {}
