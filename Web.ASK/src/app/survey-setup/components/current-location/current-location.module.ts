import { NgModule } from '@angular/core';
import { CurrentLocationComponent } from './current-location.component';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [IonicModule, CommonModule, FormsModule],
  declarations: [CurrentLocationComponent],
  exports: [CurrentLocationComponent]
})
export class CurrentLocationModule {}
