import {
  Component,
  EventEmitter,
  Input,
  Output,
  OnInit,
  OnDestroy
} from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { SurveySetupService } from '../../survey-setup.service';
@Component({
  selector: 'current-location',
  templateUrl: './current-location.component.html',
  styleUrls: ['./current-location.component.scss']
})
export class CurrentLocationComponent implements OnInit, OnDestroy {
  public buttonDisplayName = 'CHANGE';
  public isLocationAlreadyPresent = false;
  public wasLocationAlreadyPresent = false;
  public showSearchCostCenter = false;
  public currentCostCenter: Location;
  locationSubscription: Subscription;
  showSearchCostCenterSubscription: Subscription;

  @Input() showChangeButton: boolean;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onCostCenterChange: EventEmitter<any> = new EventEmitter<any>();
  ngOnInit() {
    this.getDbLocation();
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.unsubscribeFromServiceVariables();
  }

  constructor(private surveySetupService: SurveySetupService) {
    this.subscribeToServiceVariables(surveySetupService);
  }

  private unsubscribeFromServiceVariables() {
    if (this.locationSubscription) {
      this.locationSubscription.unsubscribe();
    }
    if (this.showSearchCostCenterSubscription) {
      this.showSearchCostCenterSubscription.unsubscribe();
    }
  }

  private subscribeToServiceVariables(surveySetupService: SurveySetupService) {
    this.locationSubscription = surveySetupService.isLocationSelected$.subscribe(
      isLocationAlreadyPresent => {
        this.isLocationAlreadyPresent = isLocationAlreadyPresent;
        if (this.isLocationAlreadyPresent !== this.wasLocationAlreadyPresent) {
          this.getDbLocation();
        }
        this.wasLocationAlreadyPresent = this.isLocationAlreadyPresent;
      }
    );
    this.showSearchCostCenterSubscription = surveySetupService.showSearchCostCenter$.subscribe(
      showSearchCostCenter => {
        this.showSearchCostCenter = showSearchCostCenter;
      }
    );
  }

  public getDbLocation() {
    this.surveySetupService.getDbLocations().then((data: Location[]) => {
      if (data && data.length > 0) {
        this.currentCostCenter = data[0];
        localStorage.setItem(
          'selectedLocation',
          JSON.stringify(this.currentCostCenter)
        );
        this.isLocationAlreadyPresent = true;
        if (this.isLocationAlreadyPresent !== this.wasLocationAlreadyPresent) {
          this.surveySetupService.selectLocation(this.isLocationAlreadyPresent);
        }
      } else {
        this.currentCostCenter = null;
        this.isLocationAlreadyPresent = false;
        if (this.isLocationAlreadyPresent !== this.wasLocationAlreadyPresent) {
          this.surveySetupService.selectLocation(this.isLocationAlreadyPresent);
        }
      }
      this.surveySetupService.selectCurrentCostCenter(this.currentCostCenter);
      this.wasLocationAlreadyPresent = this.isLocationAlreadyPresent;
    });
  }

  public changeButtonText() {
    this.showSearchCostCenter = !this.showSearchCostCenter;
    this.buttonDisplayName = this.showSearchCostCenter ? 'CONTINUE' : 'CHANGE';
    this.onCostCenterChange.emit(this.showSearchCostCenter);
    this.surveySetupService.displaySearchCostCenter(this.showSearchCostCenter);
  }
}
