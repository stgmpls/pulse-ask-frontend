import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
@Component({
    selector: 'app-outdated-template-modal',
    templateUrl: './outdated-template-modal.component.html',
    styleUrls: ['./outdated-template-modal.component.scss']
})
export class OutdatedTemplateModalComponent {
    constructor(private modalController: ModalController) {
    }
    async closeModal() {
        await this.modalController.dismiss();
    }
}
