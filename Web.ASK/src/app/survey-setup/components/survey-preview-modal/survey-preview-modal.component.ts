import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-survey-preview-modal',
  templateUrl: './survey-preview-modal.component.html',
  styleUrls: ['./survey-preview-modal.component.scss']
})
export class SurveyPreviewModalComponent implements OnInit {
  surveyDetails: any;
  surveyItems: any = [];
  followupItems: any = [];
  private activeItem;
  constructor(private modalController: ModalController) {}
  ngOnInit() {
    let visibleItems = this.getVisibleSurveyItems();
    this.setSurveyItems(visibleItems);
    this.setFollowupItems(visibleItems);
  }
  getFollowupItems(resultOptionId, order) {
    let result = this.followupItems.filter(
      x =>
        x.ParentTriggerResultOptionID === resultOptionId &&
        (x.Order === order + 1 || x.Order === order + 2)
    );
    return result;
  }
  private setFollowupItems(visibleItems: any[]) {
    this.followupItems = visibleItems.filter(
      x => x.ParentTriggerResultOptionID !== null
    );
  }

  private setSurveyItems(visibleItems: any[]) {
    this.surveyItems = visibleItems.filter(
      x => x.ParentTriggerResultOptionID === null
    );
  }

  private getVisibleSurveyItems() {
    let obj = this.surveyDetails;
    let result = Object.keys(obj).map(function(key) {
      return obj[key];
    });

    let visibleItems = [];
    result.forEach(resultItem => {
      resultItem.SurveyItems.forEach(value => {
        value.expanded = false;
        value.ResultOptions.forEach(option => {
          option.expanded = false;
        });
        if (
          value.ItemTypeCode !== 'ML' &&
          value.Text.toUpperCase() !== 'THANK YOU'
        ) {
          visibleItems.push(value);
        }
      });
    });

    return visibleItems;
  }

  async closeModal() {
    await this.modalController.dismiss();
  }

  expandItem(item) {
    if (!item.expanded) {
      this.activeItem = item;
    }
    this.expand(item);
  }

  expand(item) {
    this.surveyItems.forEach(listItem => {
      if (item === listItem) {
        listItem.expanded = !listItem.expanded;
      } else {
        listItem.expanded = false;
        this.collapseResultOptions(listItem);
      }
      return listItem;
    });
  }

  expandResultOption(resultOption, surveyItem) {
    let resultOptionsForCurrentSurveyItem = surveyItem.ResultOptions;
    this.expandResultOptions(resultOption, resultOptionsForCurrentSurveyItem);
    this.expand(surveyItem);
  }

  expandResultOptions(resultOption, resultOptionsForCurrentSurveyItem) {
    resultOptionsForCurrentSurveyItem.forEach(option => {
      if (resultOption === option) {
        option.expanded = !option.expanded;
      } else {
        option.expanded = false;
      }
      return option;
    });
  }

  collapseResultOptions(surveyItem) {
    surveyItem.ResultOptions.forEach(option => {
      option.expanded = false;
      return option;
    });
  }
}
