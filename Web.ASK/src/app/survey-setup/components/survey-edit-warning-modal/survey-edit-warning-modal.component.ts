import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-survey-edit-warning-modal',
  templateUrl: './survey-edit-warning-modal.component.html',
  styleUrls: ['./survey-edit-warning-modal.component.scss']
})
export class SurveyEditWarningModalComponent {
  constructor(private modalController: ModalController) {}
  continue() {
    this.dismiss(true);
  }

  stop() {
    this.dismiss(false);
  }

  async dismiss(value) {
    await this.modalController.dismiss(value);
  }
}
