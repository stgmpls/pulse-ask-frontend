import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import {
  HelperService,
  NetworkService,
  SurveyService
} from '../../../core/services';
import { Location } from '../../../core/entities';
import { Device } from '@ionic-native/device/ngx';
import { Platform, ModalController } from '@ionic/angular';
import { SurveySetupService } from '../../survey-setup.service';
import { NavController } from '@ionic/angular';
import { Router, NavigationEnd } from '@angular/router';
import { SURVEY_TYPE } from '../../constant/constant';
import { Observable } from 'rxjs/Observable';
import { SurveyVersionModalComponent } from '../../components/survey-version-modal/survey-version-modal.component';
import { OutdatedTemplateModalComponent } from '../../components/outdated-template-modal/outdated-template-modal.component';
import { SurveyEditWarningModalComponent } from '../../components/survey-edit-warning-modal/survey-edit-warning-modal.component';
import { SurveyPreviewModalComponent } from '../../components/survey-preview-modal/survey-preview-modal.component';
@Component({
  selector: 'survey-segment',
  templateUrl: './survey-segment.component.html',
  styleUrls: ['./survey-segment.component.scss']
})
export class SurveySegmentComponent implements OnInit, OnDestroy {
  roleId: number;
  sectorId: string;
  username: string;
  public deviceTypeCode = 'T';
  locationSubscription: Subscription;
  showSearchCostCenterSubscription: Subscription;
  surveySegmentSubscription: Subscription;
  showSurveyListSubscription: Subscription;
  configSurveySubscription: Subscription;
  public showSearchCostCenter = false;
  public isLocationAlreadyPresent = false;
  public surveySegment = '';
  public showSurveyList = false;
  public currentCostCenter: Location;
  public configSurvey: any = [];
  public costCenterId: number;
  public themeName = '';
  public isDesktopBrowser = false;
  countLimit = 5;
  public listOfSurveyItems: any[] = [];
  private readonly userPagePath = '/user';
  private readonly setupSurveyPagePath = '/home/setupsurvey';
  private readonly searchCostCenterPath = '/home/searchcostcenter';
  private readonly newSegment = 'new';
  private readonly locationSegment = 'location';
  private readonly deviceSegment = 'device';
  private surveyByCategory = '';
  public SURVEY_TYPE = SURVEY_TYPE;
  private isCurrentSurveyOutdated = false;
  private isTemplateOutdated = false;
  ngOnInit() {}

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.unsubscribeFromServiceVariables();
  }

  constructor(
    private surveySetupService: SurveySetupService,
    private networkMgr: NetworkService,
    private helperService: HelperService,
    private device: Device,
    private platform: Platform,
    private surveyService: SurveyService,
    private router: Router,
    private modalController: ModalController
  ) {
    if (this.platform.is('desktop')) {
      this.isDesktopBrowser = true;
    } else {
      this.isDesktopBrowser = false;
    }
    if (this.surveySegment === '') {
      this.setSurveySegment();
    } else {
      this.processSurveySegmentSelection();
    }
    this.fetchDataFromLocalStorage();
    this.subscribeToServiceVariables();
    this.getDbLocation();
  }

  private unsubscribeFromServiceVariables() {
    if (this.locationSubscription) {
      this.locationSubscription.unsubscribe();
    }
    if (this.showSearchCostCenterSubscription) {
      this.showSearchCostCenterSubscription.unsubscribe();
    }
    if (this.surveySegmentSubscription) {
      this.surveySegmentSubscription.unsubscribe();
    }
    if (this.showSurveyListSubscription) {
      this.showSurveyListSubscription.unsubscribe();
    }
    if (this.configSurveySubscription) {
      this.configSurveySubscription.unsubscribe();
    }
  }

  private setSurveySegment() {
    this.surveySetupService.selectTypeOfSurvey('');
    if (this.router.url.indexOf(this.setupSurveyPagePath) > -1) {
      this.surveySegment = this.newSegment;
      this.surveySetupService.selectSurveySegment(this.surveySegment);
    } else if (this.router.url.indexOf(this.searchCostCenterPath) > -1) {
      this.surveySegment = this.locationSegment;
      if (this.surveySetupService.selectedSegment === this.deviceSegment) {
        this.surveySegment = this.deviceSegment;
      }

      this.surveySetupService.selectSurveySegment(this.surveySegment);
    }
    this.processSurveySegmentSelection();
  }

  private fetchDataFromLocalStorage() {
    this.roleId = Number(localStorage.getItem('roleId'));
    this.sectorId = localStorage.getItem('sectorId');
    this.username = localStorage.getItem('username');
  }

  private subscribeToServiceVariables() {
    this.locationSubscription = this.surveySetupService.isLocationSelected$.subscribe(
      isLocationAlreadyPresent => {
        this.isLocationAlreadyPresent = isLocationAlreadyPresent;
      }
    );
    this.showSearchCostCenterSubscription = this.surveySetupService.showSearchCostCenter$.subscribe(
      showSearchCostCenter => {
        this.showSearchCostCenter = showSearchCostCenter;
      }
    );
    this.surveySegmentSubscription = this.surveySetupService.surveySegment$.subscribe(
      surveySegment => {
        this.surveySegment = surveySegment;
      }
    );
    this.showSurveyListSubscription = this.surveySetupService.showSurveyList$.subscribe(
      showSurveyList => {
        this.showSurveyList = showSurveyList;
      }
    );
    this.configSurveySubscription = this.surveySetupService.configSurvey$.subscribe(
      configSurvey => {
        this.configSurvey = configSurvey;
      }
    );
  }

  public getDbLocation() {
    this.surveySetupService.getDbLocations().then((data: Location[]) => {
      if (data && data.length > 0) {
        this.currentCostCenter = data[0];
        this.costCenterId = this.currentCostCenter.LocationId;
        this.themeName = 'theme-' + this.currentCostCenter.SectorAdamId;
        this.isLocationAlreadyPresent = true;
        this.processSurveySegmentSelection();
      } else {
        this.currentCostCenter = null;
        this.isLocationAlreadyPresent = false;
      }
    });
  }

  public selectedSegment() {
    if (this.surveySegment !== this.newSegment) {
      this.surveySetupService.selectedSegment = this.surveySegment;
    }
    this.surveySetupService.selectSurveySegment(this.surveySegment);
    this.processSurveySegmentSelection();
  }

  private processSurveySegmentSelection() {
    if (this.surveySegment === this.newSegment) {
      if (!this.surveyService.isEditMode) {
        this.surveySetupService.selectTypeOfSurvey('');
        this.surveySetupService.selectSurveysByCategory([]);
        this.surveyService.surveyData = [];
      }
      this.showSurveyList = false;
      this.surveySetupService.selectSurveyList(this.showSurveyList);
      this.NavigateToSurveyPagePath();
    } else if (this.surveySegment === this.locationSegment) {
      this.showSurveyList = true;
      this.surveySetupService.selectSurveyList(this.showSurveyList);

      if (this.router.url.indexOf(this.setupSurveyPagePath) > -1) {
        this.NavigateToCostCenterPath();
      } else {
        this.getLocationSurveys();
      }
    } else if (this.surveySegment === this.deviceSegment) {
      this.showSurveyList = true;
      this.surveySetupService.selectSurveyList(this.showSurveyList);

      if (this.router.url.indexOf(this.setupSurveyPagePath) > -1) {
        this.NavigateToCostCenterPath();
      } else {
        this.getDeviceSurveys();
      }
    }
  }

  private NavigateToSurveyPagePath() {
    if (this.router.url.indexOf(this.searchCostCenterPath) > -1) {
      this.router.navigate([this.setupSurveyPagePath]);
    }
  }

  private NavigateToCostCenterPath() {
    if (this.router.url.indexOf(this.setupSurveyPagePath) > -1) {
      this.router.navigate([this.searchCostCenterPath]);
    }
  }

  public getLocationSurveys() {
    if (this.networkMgr.isAvailiable) {
      if (this.costCenterId) {
        this.surveySetupService
          .getLocationSurveys(
            this.costCenterId,
            this.countLimit,
            this.roleId,
            this.deviceTypeCode
          )
          .subscribe((data: any) => {
            if (data) {
              this.configSurvey = data.Data;
              this.processSurveys();
            } else {
              this.configSurvey = [];
            }
            this.surveySetupService.selectConfigSurvey(this.configSurvey);
          });
      }
    } else {
      this.helperService.showAlert(
        'No internet connection available. Please check your settings.'
      );
    }
  }

  public getDeviceSurveys() {
    if (this.networkMgr.isAvailiable) {
      this.surveySetupService
        .getDeviceSurveys(this.device.uuid, this.countLimit, this.roleId)
        .subscribe((data: any) => {
          if (data) {
            this.configSurvey = data.Data;
            this.processSurveys();
          } else {
            this.configSurvey = [];
          }
          this.surveySetupService.selectConfigSurvey(this.configSurvey);
        });
    } else {
      this.helperService.showAlert(
        'No internet connection available. Please check your settings.'
      );
    }
  }

  /* check if surveys are in SQLite dtabase already, if not add them to database. */
  private processSurveys() {
    this.fetchDataFromLocalStorage();
    this.configSurvey.forEach(element => {
      this.surveyService
        .getSurveyByIdAndUsername(element.Header.SurveyId, this.username)
        .then((response: any) => {
          const surveyDetailArray = this.helperService.convertObjectCollectionToArray(
            element.Details
          );
          if (!response || !response.rows || response.rows.length === 0) {
            this.surveyService.insertSurveys(
              new Array(element),
              0,
              this.username,
              this.sectorId,
              this.roleId
            );
          } else {
            this.surveyService.saveSurvey(
              this.username,
              this.sectorId,
              this.roleId,
              element.Header.SurveyId,
              JSON.stringify(element.Header),
              element.Category
            );
            this.surveyService.updateSurveyDetails(
              this.username,
              this.sectorId,
              this.roleId,
              element.Header.SurveyId,
              surveyDetailArray
            );
          }
        });
    });
  }

  public getItemsForSurvey(survey) {
    return this.surveyService
      .getDisplayGroupData(
        this.username,
        this.sectorId,
        this.roleId,
        survey.Header.SurveyId,
        undefined,
        undefined,
        0,
        0,
        null,
        false,
        false,
        true
      )
      .then((response: any) => {
        if (response && response.survey_item) {
          const surveyItems = JSON.parse(response.survey_item);
          for (let i = 0; i < surveyItems.length; i++) {
            survey.Details[0].SurveyItems.forEach(item => {
              if (item.SurveyItemID === surveyItems[i].SurveyItemID) {
                surveyItems[i] = item;
              }
            });
          }
          return surveyItems;
        }
      });
  }

  public createSurveyItems(mergedSurveyItems) {
    this.listOfSurveyItems = [];
    for (let i = 0; i < mergedSurveyItems.length; i++) {
      let resultText = '';
      if (mergedSurveyItems[i].ItemTypeCode === 'ML') {
        resultText = localStorage.getItem('selectedLocationId');
      } else if (
        mergedSurveyItems[i].ItemTypeCode !== 'ML' &&
        mergedSurveyItems[i].ScaffoldIsVisible === 'false'
      ) {
        resultText = mergedSurveyItems[i].Text;
      } else {
        resultText =
          mergedSurveyItems[i].Result &&
          mergedSurveyItems[i].Result.ResultText !== ''
            ? mergedSurveyItems[i].Result.ResultText
            : mergedSurveyItems[i].Text;
      }

      // for saving data in sql server
      const surveyToBeCopied = {
        SurveyItemId: mergedSurveyItems[i].SurveyItemID,
        ResultOptionIds: null,
        ResultOptionText: resultText,
        ItemId: mergedSurveyItems[i].ItemID,
        ResultOptionTypeId:
          mergedSurveyItems[i].ResultOptionTypeID === 0
            ? null
            : mergedSurveyItems[i].ResultOptionTypeID,
        SurveyID: this.surveyService.surveyId,
        OriginalSurveyID: this.surveyService.originalSurveyId
      };
      this.listOfSurveyItems.push(surveyToBeCopied);
    }
  }

  public insertSurveys(surveyList, surveyCount) {
    this.surveyService
      .insertSurveys(
        surveyList,
        surveyCount,
        this.username,
        this.sectorId,
        this.roleId
      )
      .then(
        () => {
          if (surveyCount + 1 === surveyList.length) {
            this.router.navigate([
              this.userPagePath,
              surveyList[surveyCount].Header.SurveyId
            ]);
          }
        },
        error => {
          console.log(error.message); // TO DO :- Replace this code with logger service.
        }
      );
  }

  public editSurvey(survey) {
    this.getLatestSurveyId(
      survey.Header.SurveyId,
      this.costCenterId.toString()
    ).subscribe((latestSurveyId: number) => {
      if (
        !this.isTemplateOutdated &&
        this.isCurrentSurveyOutdated &&
        survey.Header.SurveyId !== latestSurveyId
      ) {
        // if template is not outdated and user clicked 'Yes' button on survey version popup
        this.surveyService
          .getAllSurveys(
            this.roleId,
            this.sectorId,
            this.deviceTypeCode,
            false,
            latestSurveyId
          )
          .subscribe((surveyResponse: any) => {
            if (surveyResponse.Data && surveyResponse.Data.length > 0) {
              this.redirectToSetupSurveyPage(surveyResponse.Data[0]);
            }
          });
      } else if (!this.isTemplateOutdated && !this.isCurrentSurveyOutdated) {
        // if template is not outdated and survey version popup was not displayed
        this.redirectToSetupSurveyPage(survey);
      }
    });
  }

  getSurveyByCategory() {
    this.surveyService.getUserAdHocForms(this.roleId).subscribe(response => {});
  }

  launchSurveyWithoutCopy(survey) {
    this.getLatestSurveyId(
      survey.Header.SurveyId,
      this.costCenterId.toString()
    ).subscribe((latestSurveyId: number) => {
      if (
        !this.isTemplateOutdated &&
        this.isCurrentSurveyOutdated &&
        survey.Header.SurveyId !== latestSurveyId
      ) {
        // if template is not outdated and user clicked 'Yes' button on survey version popup
        this.redirectToUserResponsePage(latestSurveyId);
      } else if (!this.isTemplateOutdated && !this.isCurrentSurveyOutdated) {
        // if template is not outdated and survey version popup was not displayed
        this.redirectToUserResponsePage(survey.Header.SurveyId);
      }
    });
  }

  redirectToUserResponsePage(surveyId) {
    this.router.navigate([this.userPagePath, surveyId]);
  }

  async redirectToSetupSurveyPage(survey) {
    this.showSurveyEditWarningModal().then(result => {
      if (result) {
        this.surveyService.originalSurveyId = survey.Header.OriginalSurveyID;
        this.surveyService.surveyDescription = survey.Header.Description;
        this.surveyService.surveyId = survey.Header.SurveyId;
        this.surveyService.isEditMode = true;
        this.surveyService.editSurveyPlace = this.surveySegment;
        this.router.navigate([
          this.setupSurveyPagePath,
          survey.Header.SurveyId
        ]);
      }
    });
  }

  getLatestSurveyId(surveyId: number, locationId: string) {
    this.isCurrentSurveyOutdated = false;
    return Observable.create((observer: any) => {
      let latestSurveyId = 0;
      this.surveyService
        .getLatestVersionSurveyId(surveyId, locationId, false)
        .subscribe(
          response => {
            latestSurveyId = response.Data.LatestSurveyId;
            if (response.Data.IsTemplateUpdatedAfterSurveyCreation) {
              this.isTemplateOutdated = true;
              this.showOutdatedTemplateModal(latestSurveyId).then(
                surveyIdFromModal => {
                  observer.next(surveyIdFromModal);
                  observer.complete();
                }
              );
            } else {
              this.isTemplateOutdated = false;
              if (latestSurveyId !== surveyId) {
                this.showSurveyVersionModal(latestSurveyId, surveyId).then(
                  surveyIdFromModal => {
                    observer.next(surveyIdFromModal);
                    observer.complete();
                  }
                );
              } else {
                observer.next(surveyId);
                observer.complete();
              }
            }
          },
          error => {
            console.log(error.message);
            observer.next(surveyId);
            observer.complete();
          }
        );
    });
  }

  async showSurveyVersionModal(latestSurveyId, surveyId) {
    const modal = await this.modalController.create({
      component: SurveyVersionModalComponent,
      cssClass: 'survey-version-modal',
      backdropDismiss: false
    });

    modal.present();
    this.isCurrentSurveyOutdated = true;
    return await modal.onDidDismiss().then(valueFromModal => {
      if (valueFromModal !== null) {
        return valueFromModal.data ? latestSurveyId : surveyId;
      }
    });
  }

  async showOutdatedTemplateModal(surveyId) {
    const modal = await this.modalController.create({
      component: OutdatedTemplateModalComponent,
      cssClass: 'outdated-template-modal',
      backdropDismiss: false
    });

    modal.present();
    return await modal.onDidDismiss().then(() => {
      return surveyId;
    });
  }

  async showSurveyEditWarningModal() {
    const modal = await this.modalController.create({
      component: SurveyEditWarningModalComponent,
      cssClass: 'survey-edit-warning-modal',
      backdropDismiss: false
    });

    modal.present();
    return await modal.onDidDismiss().then(valueFromModal => {
      if (valueFromModal !== null) {
        return valueFromModal.data;
      } else {
        return false;
      }
    });
  }

  public previewSurvey(survey) {
    this.showSurveyPreview(survey.Details);
  }

  async showSurveyPreview(surveyDetails) {
    const modal = await this.modalController.create({
      component: SurveyPreviewModalComponent,
      cssClass: 'survey-preview-modal',
      backdropDismiss: false,
      componentProps: {
        surveyDetails: surveyDetails
      }
    });
    modal.present();
    return await modal.onDidDismiss();
  }
}
