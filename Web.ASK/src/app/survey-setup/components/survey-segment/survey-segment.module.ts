import { NgModule } from '@angular/core';
import { SurveySegmentComponent } from './survey-segment.component';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { SurveyVersionModalComponent } from '../../components/survey-version-modal/survey-version-modal.component';
import { OutdatedTemplateModalComponent } from '../../components/outdated-template-modal/outdated-template-modal.component';
import { SurveyEditWarningModalComponent } from '../../components/survey-edit-warning-modal/survey-edit-warning-modal.component';
import { SurveyPreviewModalComponent } from '../../components/survey-preview-modal/survey-preview-modal.component';
import { AccordionModule } from '../../../core/components';

@NgModule({
  imports: [IonicModule, CommonModule, FormsModule, AccordionModule],
  declarations: [
    SurveySegmentComponent,
    SurveyVersionModalComponent,
    OutdatedTemplateModalComponent,
    SurveyEditWarningModalComponent,
    SurveyPreviewModalComponent
  ],
  exports: [
    SurveySegmentComponent,
    SurveyVersionModalComponent,
    OutdatedTemplateModalComponent,
    SurveyEditWarningModalComponent,
    SurveyPreviewModalComponent
  ],
  entryComponents: [
    SurveyVersionModalComponent,
    OutdatedTemplateModalComponent,
    SurveyEditWarningModalComponent,
    SurveyPreviewModalComponent
  ]
})
export class SurveySegmentModule {}
