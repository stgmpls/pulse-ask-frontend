import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  EventEmitter,
  Output
} from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { SurveyService } from '../../../core/services';
import { SurveySetupService } from '../../survey-setup.service';
import { Device } from '@ionic-native/device/ngx';
import { SURVEY_TYPE } from '../../constant/constant';
import { OrderByPipe } from '../../../core/pipes/order-by.pipe';

@Component({
  selector: 'app-launch-ask-btn',
  templateUrl: './launch-ask-btn.component.html',
  styleUrls: ['./launch-ask-btn.component.scss']
})
export class LaunchAskBtnComponent implements OnInit, OnDestroy {
  surveySegmentSubscription: Subscription;
  surveysByCategorySubscription: Subscription;
  public surveysByCategory: any = [];
  public surveySegment: string;
  roleId: number;
  sectorId: string;
  username: string;
  public loopingIndex = 0;
  public deviceTypeCode = 'T';
  public surveyItems: any[];
  public listOfSurveyItems: any[] = [];
  private readonly userPagePath = '/user';

  @Input() surveyType = '';
  @Output() requireSurveyItem: EventEmitter<any> = new EventEmitter();

  constructor(
    private surveyService: SurveyService,
    private router: Router,
    private surveySetupService: SurveySetupService,
    private alertCtrl: AlertController,
    private device: Device,
    private orderByPipe: OrderByPipe
  ) {
    this.fetchDataFromLocalStorage();
    this.subscribeToServiceVariables();
  }

  private fetchDataFromLocalStorage() {
    this.roleId = Number(localStorage.getItem('roleId'));
    this.sectorId = localStorage.getItem('sectorId');
    this.username = localStorage.getItem('username');
  }

  private subscribeToServiceVariables() {
    this.surveySegmentSubscription = this.surveySetupService.surveySegment$.subscribe(
      surveySegment => {
        this.surveySegment = surveySegment;
      }
    );

    this.surveysByCategorySubscription = this.surveySetupService.surveysByCategory$.subscribe(
      surveysByCategory => {
        this.surveysByCategory = surveysByCategory;
      }
    );
  }

  ngOnInit() {
    console.log('surveyTpe', this.surveyType);
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.unsubscribeFromServiceVariables();
  }

  private unsubscribeFromServiceVariables() {
    if (this.surveySegmentSubscription) {
      this.surveySegmentSubscription.unsubscribe();
    }

    if (this.surveysByCategorySubscription) {
      this.surveysByCategorySubscription.unsubscribe();
    }
  }

  public launchAsk() {
    this.surveyItems = this.surveyService.surveyData;
    this.getItemsForSurvey(this.surveyService.surveyId).then(
      (mergedSurveyItems: any) => {
        if (mergedSurveyItems) {
          let filteredItems;
          if (
            this.surveyType === SURVEY_TYPE.PREDEFINED ||
            this.surveyType === SURVEY_TYPE.TASTING ||
            this.surveyType === ''
          ) {
            filteredItems = this.getInvalidSurveyItem(mergedSurveyItems);
          } else if (this.surveyType === SURVEY_TYPE.CUSTOM) {
            mergedSurveyItems = this.orderByPipe.transform(
              mergedSurveyItems,
              'SurveyItemID'
            );
            filteredItems = this.getInvalidCustomSurveyItem(mergedSurveyItems);
          }

          if (filteredItems.length) {
            // Display error message.
            this.showLauchAskError();
          } else {
            // Submit form data and launch ASK.
            this.createSurveyItem(mergedSurveyItems);
            let isSurveyModified = false;
            if (this.surveyService.isEditMode) {
              for (let i = 0; i < this.surveyItems.length; i++) {
                // check for changes in survey item text
                if (
                  this.surveyItems[i].Text !==
                  this.listOfSurveyItems[i].ResultOptionText
                ) {
                  isSurveyModified = true;
                  break;
                }
                // check for changes in survey item result options
                if (
                  this.surveyItems[i].ResultOptions &&
                  this.surveyItems[i].ResultOptions.length > 0 &&
                  this.surveyItems[i].ResultOptions !== '[]'
                ) {
                  const resultOptionIds = JSON.parse(
                    this.surveyItems[i].ResultOptions
                  ).map(item => {
                    return item.ResultOptionId;
                  });
                  if (
                    this.listOfSurveyItems[i].ResultOptionIds !== '' &&
                    resultOptionIds.join(',') !==
                    this.listOfSurveyItems[i].ResultOptionIds
                  ) {
                    isSurveyModified = true;
                    break;
                  }
                }
                // check for followup options
                if (
                  this.surveyItems[i].Result &&
                  this.surveyItems[i].Result.FollowupOptions &&
                  this.surveyItems[i].Result.FollowupOptions.length > 0 &&
                  this.surveyItems[i].Result.FollowupOptions !== ''
                ) {
                  const followupOptions = JSON.parse(
                    this.surveyItems[i].ResultOptions
                  ).map(item => {
                    return item.Text;
                  });
                  if (
                    this.listOfSurveyItems[i].FollowupOptions !== '' &&
                    followupOptions.join(',') !==
                    this.listOfSurveyItems[i].FollowupOptions.join(',')
                  ) {
                    isSurveyModified = true;
                    break;
                  }
                }
              }
            }
            let isSurveyToBeLaunched = false;
            if (!this.surveyService.isEditMode) {
              // set 'isSurveyToBeLaunched' if survey is being created from template
              isSurveyToBeLaunched = true;
            } else if (isSurveyModified) {
              // set 'isSurveyToBeLaunched' if existing survey has been edited
              isSurveyToBeLaunched = true;
            } else {
              // reset 'isSurveyToBeLaunched' in all other cases
              isSurveyToBeLaunched = false;
            }
            if (!isSurveyToBeLaunched) {
              // if 'isSurveyToBeLaunched' is not set, get user responses against existing survey
              this.router.navigate([
                this.userPagePath,
                this.surveyService.surveyId
              ]);
            } else {
              let surveyIdToBeUsed = this.surveyService.originalSurveyId;
              if (this.surveyService.isEditMode) {
                surveyIdToBeUsed = this.surveyService.surveyId;
              }
              // if 'isSurveyToBeLaunched' is set, launch new survey
              if (
                this.surveyType === SURVEY_TYPE.PREDEFINED ||
                this.surveyType === SURVEY_TYPE.TASTING ||
                this.surveyType === ''
              ) {
                this.surveyService
                  .launchAsk(
                    this.listOfSurveyItems,
                    surveyIdToBeUsed,
                    this.device.uuid,
                    this.roleId
                  )
                  .subscribe((resp: any) => {
                    this.handleLaunchAskResponse(resp);
                  });
              } else if (this.surveyType === SURVEY_TYPE.CUSTOM) {
                this.surveyService
                  .launchAskCustomSurvey(
                    this.listOfSurveyItems,
                    surveyIdToBeUsed,
                    this.device.uuid,
                    this.roleId
                  )
                  .subscribe((resp: any) => {
                    this.handleLaunchAskResponse(resp);
                  });
              }
            }
          }
        } else {
          // Display error message.
          this.showLauchAskError();
        }
      }
    );
  }

  handleLaunchAskResponse(resp) {
    const newSurveyId = resp.Data;
    this.surveyService
      .getAllSurveys(
        this.roleId,
        this.sectorId,
        this.deviceTypeCode,
        false,
        newSurveyId
      )
      .subscribe((surveyResponse: any) => {
        if (surveyResponse.Data && surveyResponse.Data.length > 0) {
          const newSurvey = surveyResponse.Data;
          this.insertSurveys(newSurvey, 0);
        }
      });
  }

  public createSurveyItem(mergedSurveyItems) {
    this.listOfSurveyItems = [];
    for (let i = 0; i < mergedSurveyItems.length; i++) {
      let resultText = '';
      let FollowupOptions = [];
      // add FollowupOptions field for custom survey if ResultText is empty do no add survey item to surveyToBeCopied
      if (
        this.surveyType === SURVEY_TYPE.CUSTOM &&
        JSON.parse(mergedSurveyItems[i].ParentTriggerResultOptionID) !== null &&
        mergedSurveyItems[i].ControlTypeCode === 'RB'
      ) {
        if (
          mergedSurveyItems[i].Result.ResultText.trim() === '' &&
          mergedSurveyItems[i].Result.FollowupOptions.length === 0
        ) {
          continue;
        } else {
          FollowupOptions = mergedSurveyItems[i].Result.FollowupOptions.map(
            item => item.Text
          );
        }
      }
      if (mergedSurveyItems[i].ItemTypeCode === 'ML') {
        resultText = localStorage.getItem('selectedLocationId');
      } else if (
        mergedSurveyItems[i].ItemTypeCode !== 'ML' &&
        mergedSurveyItems[i].ScaffoldIsVisible === 'false'
      ) {
        resultText = mergedSurveyItems[i].Text;
      } else {
        resultText =
          mergedSurveyItems[i].Result.ResultText !== ''
            ? mergedSurveyItems[i].Result.ResultText
            : mergedSurveyItems[i].Text;
      }
      let resultOptionIds = '';
      if (
        mergedSurveyItems[i].Result &&
        mergedSurveyItems[i].Result.ResultOptionId !== null &&
        mergedSurveyItems[i].Result.ResultOptionId !== 0 &&
        mergedSurveyItems[i].Result.ResultOptionId.length
      ) {
        resultOptionIds = mergedSurveyItems[i].Result.ResultOptionId.join(',');
      }
      let surveyToBeCopied;
      // for saving data in sql server
      surveyToBeCopied = {
        SurveyItemId: mergedSurveyItems[i].SurveyItemID,
        ResultOptionIds: resultOptionIds,
        ResultOptionText: resultText,
        ItemId: mergedSurveyItems[i].ItemID,
        ResultOptionTypeId:
          mergedSurveyItems[i].ResultOptionTypeID === 0
            ? null
            : mergedSurveyItems[i].ResultOptionTypeID,
        SurveyID: this.surveyService.surveyId,
        OriginalSurveyID: this.surveyService.originalSurveyId
      };

      if (FollowupOptions && FollowupOptions.length > 0) {
        surveyToBeCopied.FollowupOptions = FollowupOptions;
      }

      this.listOfSurveyItems.push(surveyToBeCopied);
    }
  }

  public insertSurveys(surveyList, surveyCount) {
    this.surveyService
      .insertSurveys(
        surveyList,
        surveyCount,
        this.username,
        this.sectorId,
        this.roleId
      )
      .then(
        result => {
          if (surveyCount + 1 === surveyList.length) {
            this.router.navigate([
              this.userPagePath,
              surveyList[surveyCount].Header.SurveyId
            ]);
          }
        },
        error => {
          console.log(error.message); // TO DO :- Replace this code with logger service.
        }
      );
  }

  public getItemsForSurvey(surveyId) {
    return this.surveyService
      .getDisplayGroupData(
        this.username,
        this.sectorId,
        this.roleId,
        surveyId,
        undefined,
        undefined,
        0,
        0,
        null,
        false,
        false,
        true
      )
      .then((response: any) => {
        if (response && response.survey_item) {
          const surveyItems = JSON.parse(response.survey_item);
          for (let i = 0; i < surveyItems.length; i++) {
            this.surveyItems.forEach(item => {
              if (item.SurveyItemID === surveyItems[i].SurveyItemID) {
                surveyItems[i] = item;
              }
            });
          }
          return surveyItems;
        }
      });
  }
  async showLauchAskError() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: '',
      message: 'Please Enter Values Correctly.',
      buttons: ['OK']
    });

    await alert.present();
  }

  // validate predefined and tasting survey items
  getInvalidSurveyItem(mergedSurveyItems) {
    const filteredItems = mergedSurveyItems.filter(surveyItem => {
      if (JSON.parse(surveyItem.ScaffoldIsVisible)) {
        if (
          !surveyItem.Result ||
          (surveyItem.Result &&
            (surveyItem.Result.ResultText.trim() === '' &&
              (surveyItem.Result.ResultOptionId === null ||
                surveyItem.Result.ResultOptionId.length === 0)))
        ) {
          return true;
        }
      }
    });
    if (filteredItems) {
      this.requireSurveyItem.emit(filteredItems.map(item => item.SurveyItemID));
    }
    return filteredItems;
  }

  // validate custom survey items
  getInvalidCustomSurveyItem(mergedSurveyItems) {
    const filteredItems = mergedSurveyItems.filter(surveyItem => {
      if (JSON.parse(surveyItem.ScaffoldIsVisible)) {
        if (
          JSON.parse(surveyItem.ParentTriggerResultOptionID) !== null &&
          surveyItem.ControlTypeCode === 'RB'
        ) {
          return this.validateFollowUpSurveyItem(surveyItem);
        }
        if (
          surveyItem.Result.ResultText.trim() === '' &&
          (surveyItem.Result.ResultOptionId === null ||
            surveyItem.Result.ResultOptionId.length === 0)
        ) {
          return true;
        }
      }
    });
    if (filteredItems) {
      this.requireSurveyItem.emit(filteredItems.map(item => item.SurveyItemID));
    }
    return filteredItems;
  }

  // validate followUp survey Item
  validateFollowUpSurveyItem(surveyItem) {
    if (JSON.parse(surveyItem.IsRequired)) {
      if (surveyItem.Result.ResultText.trim() === '') {
        return true;
      } else if (
        surveyItem.Result.FollowupOptions.length === 0 ||
        surveyItem.Result.FollowupOptions.find(option => option.Text === '')
      ) {
        return true;
      }
    } else if (
      surveyItem.Result.ResultText.trim() !== '' &&
      (surveyItem.Result.FollowupOptions.length === 0 ||
        surveyItem.Result.FollowupOptions.find(
          option => option.Text.trim() === ''
        ))
    ) {
      return true;
    }
    return false;
  }

  getCustomSurveyFollowUpOptions(mergedSurveyItems) {
    const surveyFollowupOptions = [];
    mergedSurveyItems.map((surveyItem, index) => {
      if (
        (surveyItem.ParentTriggerResultOptionID !== null ||
          surveyItem.ParentTriggerResultOptionID !== 'null') &&
        surveyItem.ControlTypeCode === 'RB'
      ) {
        const options = [];
        surveyItem.Result.ResultOptions.map(resultOption => {
          options.push(resultOption.Text);
        });
        surveyFollowupOptions.push({
          SurveyItemId: surveyItem.SurveyItemID,
          FollowupOption: options
        });

        this.listOfSurveyItems[index].FollowupOptions = options;
      }
    });
    return surveyFollowupOptions;
  }
}
