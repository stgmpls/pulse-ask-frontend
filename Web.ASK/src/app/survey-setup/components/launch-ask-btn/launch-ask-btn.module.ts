import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { LaunchAskBtnComponent } from './launch-ask-btn.component';

@NgModule({
    imports: [IonicModule],
    declarations: [LaunchAskBtnComponent],
    exports: [LaunchAskBtnComponent]
})
export class LaunchAskBtnModule { }
