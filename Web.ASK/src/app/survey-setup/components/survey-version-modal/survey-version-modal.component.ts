import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
@Component({
    selector: 'app-survey-version-modal',
    templateUrl: './survey-version-modal.component.html',
    styleUrls: ['./survey-version-modal.component.scss']
})
export class SurveyVersionModalComponent {
    constructor(private modalController: ModalController) {
    }

    continue() {
        this.dismiss(true);
    }

    stop() {
        this.dismiss(false);
    }

    async dismiss(value) {
        await this.modalController.dismiss(value);
    }
}
