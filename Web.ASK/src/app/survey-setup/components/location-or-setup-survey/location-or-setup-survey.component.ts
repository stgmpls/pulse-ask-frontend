import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'location-or-setup-survey',
  templateUrl: './location-or-setup-survey.component.html',
  styleUrls: ['./location-or-setup-survey.component.scss']
})
export class LocationOrSetupSurveyComponent implements OnInit {
  @Input() wizardInfo = [];
  @Input() activeItemType = '';
  ngOnInit() {}
}
