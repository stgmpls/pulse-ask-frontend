import { NgModule } from '@angular/core';
import { LocationOrSetupSurveyComponent } from './location-or-setup-survey.component';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [IonicModule, CommonModule, FormsModule],
  declarations: [LocationOrSetupSurveyComponent],
  exports: [LocationOrSetupSurveyComponent]
})
export class LocationOrSetupSurveyModule {}
