import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { SurveySetupPage } from './survey-setup.page';
import { SurveySetupModule } from './survey-setup-routing.module';
import { CoreModule } from '../core';
import { SelectSurveyTypeModule } from './select-survey-type/select-survey-type.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SurveySetupModule,
    CoreModule,
    SelectSurveyTypeModule
  ],
  declarations: [SurveySetupPage]
})
export class SurveySetupPageModule {}
