import { Injectable } from '@angular/core';
import {
  AuthenticationService,
  APIMethod,
  DataService,
  DbService,
  SyncService,
  NetworkService
} from '../core/services';
import { Subject } from 'rxjs/Subject';
import { EMPTY } from 'rxjs';
import { APIRequest } from '../core/entities';

@Injectable({
  providedIn: 'root'
})
export class SurveySetupService {
  private readonly locationTableName = 'location';
  // Observable source
  private isLocationSelected = new Subject<boolean>();
  private showSearchCostCenter = new Subject<boolean>();
  private typeOfSurvey = new Subject<string>();
  private surveySegment = new Subject<string>();
  private surveysByCategory = new Subject<any>();
  private currentCostCenter = new Subject<any>();
  private showSurveyList = new Subject<boolean>();
  private configSurvey = new Subject<any>();
  selectedSegment = '';

  // Observable stream
  isLocationSelected$ = this.isLocationSelected.asObservable();
  showSearchCostCenter$ = this.showSearchCostCenter.asObservable();
  typeOfSurvey$ = this.typeOfSurvey.asObservable();
  surveySegment$ = this.surveySegment.asObservable();
  surveysByCategory$ = this.surveysByCategory.asObservable();
  currentCostCenter$ = this.currentCostCenter.asObservable();
  showSurveyList$ = this.showSurveyList.asObservable();
  configSurvey$ = this.configSurvey.asObservable();

  // Service message commands
  selectLocation(isLocationSelected: boolean) {
    this.isLocationSelected.next(isLocationSelected);
  }
  displaySearchCostCenter(showSearchCostCenter: boolean) {
    this.showSearchCostCenter.next(showSearchCostCenter);
  }
  selectTypeOfSurvey(typeOfSurvey: string) {
    this.typeOfSurvey.next(typeOfSurvey);
  }
  selectSurveySegment(surveySegment: string) {
    this.surveySegment.next(surveySegment);
  }
  selectSurveysByCategory(surveysByCategory: any) {
    this.surveysByCategory.next(surveysByCategory);
  }
  selectCurrentCostCenter(currentCostCenter: any) {
    this.currentCostCenter.next(currentCostCenter);
  }
  selectSurveyList(showSurveyList: boolean) {
    this.showSurveyList.next(showSurveyList);
  }

  selectConfigSurvey(configSurvey: any) {
    this.configSurvey.next(configSurvey);
  }

  constructor(
    private dataService: DataService,
    private db: DbService,
    private authenticationService: AuthenticationService,
    private syncService: SyncService,
    private networkService: NetworkService
  ) {}

  public getDbLocations() {
    return this.findAllLocations();
  }

  public getLocationSurveys(locationId, countLimit, roleId, deviceTypeCode) {
    const params = `api/Survey/surveys/locations/${locationId}/${countLimit}?roleId=${roleId}&deviceTypeCode=${deviceTypeCode}`;
    const request: APIRequest = new APIRequest(params, APIMethod.GET);
    return this.dataService.execute(request);
  }

  public getDeviceSurveys(deviceId, countLimit, roleId) {
    const params = `api/Survey/surveys/devices/${deviceId}/${countLimit}?roleId=${roleId}`;
    const request: APIRequest = new APIRequest(params, APIMethod.GET);
    return this.dataService.execute(request);
  }

  private findAllLocations() {
    return this.db
      .executeSql('SELECT * FROM ' + this.locationTableName, [])
      .then((data: any) => {
        if (data) {
          return this.getRowsData(data.rows);
        } else {
          return data;
        }
      });
  }

  private getRowsData(rows) {
    const data = [];
    for (let i = 0; i < rows.length; i++) {
      data.push(rows.item(i));
    }
    return data;
  }

  public logout() {
    return this.authenticationService.logout();
  }

  public sync(username, sectorId, roleId) {
    let isNetworkConnected = false;
    if (this.networkService.isAvailiable) {
      isNetworkConnected = true;
    }
    return this.syncService.submitFormDetails(
      username,
      sectorId,
      roleId,
      isNetworkConnected
    );
  }

  public getResultOptionsForCustomSurvey() {
    const params = 'api/User/GetResultOptionsForCustomSurvey/';
    const request: APIRequest = new APIRequest(params, APIMethod.GET);
    return this.dataService.execute(request);
  }
}
