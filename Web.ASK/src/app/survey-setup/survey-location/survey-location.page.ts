import { Component, OnInit, OnDestroy } from '@angular/core';
import { HelperService, NetworkService } from '../../core/services';

import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { STATES_ARRAY } from '../../core/constant/constants';
import { Subscription } from 'rxjs/Subscription';
import { SurveySetupService } from '../survey-setup.service';
import { WIZARD_CONFIG, WIZARD_ITEM_TYPES } from '../constant/constant';

@Component({
  selector: 'app-location',
  templateUrl: './survey-location.page.html',
  styleUrls: ['./survey-location.page.scss']
})
export class SurveyLocationPage implements OnInit, OnDestroy {
  locationSubscription: Subscription;
  surveySegmentSubscription: Subscription;
  showSearchCostCenterSubscription: Subscription;
  surveyTypeSubscription: Subscription;
  currentCostCenterSubscription: Subscription;
  locations: any;
  zip_code = '';
  name = '';
  state = '';
  city = '';
  number = '';
  response: any;
  dbLocations: any;
  states = [];
  selectedState: any;
  public costCenter: Location;
  public showChangeButton = true;
  public showSearchCostCenter = false;
  public isLocationAlreadyPresent = false;
  public surveySegment = 'location';
  public typeOfSurvey: string;
  public currentCostCenter: any;
  private readonly locationSegment = 'location';
  private readonly setupSurveyPagePath = '/home/setupsurvey';
  public wizardInfo = WIZARD_CONFIG;
  public wizardItemTypes = WIZARD_ITEM_TYPES;

  constructor(
    private helperService: HelperService,
    private networkService: NetworkService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private surveySetupService: SurveySetupService
  ) {
    this.states = STATES_ARRAY;
    this.subscribeToServiceVariables(surveySetupService);
  }

  private subscribeToServiceVariables(surveySetupService: SurveySetupService) {
    this.surveySegmentSubscription = surveySetupService.surveySegment$.subscribe(
      surveySegment => {
        this.surveySegment = surveySegment;
      }
    );
    this.locationSubscription = surveySetupService.isLocationSelected$.subscribe(
      isLocationAlreadyPresent => {
        this.isLocationAlreadyPresent = isLocationAlreadyPresent;
      }
    );
    this.showSearchCostCenterSubscription = surveySetupService.showSearchCostCenter$.subscribe(
      showSearchCostCenter => {
        this.showSearchCostCenter = showSearchCostCenter;
      }
    );
    this.surveyTypeSubscription = surveySetupService.typeOfSurvey$.subscribe(
      typeOfSurvey => {
        this.typeOfSurvey = typeOfSurvey;
      }
    );
    this.currentCostCenterSubscription = surveySetupService.currentCostCenter$.subscribe(
      currentCostCenter => {
        this.currentCostCenter = currentCostCenter;
      }
    );
  }
  ngOnInit() {}

  public showCostCenterSearch(showSearchCostCenter) {
    this.showSearchCostCenter = showSearchCostCenter;
  }

  public setLocation(isLocationAlreadyPresent) {
    this.isLocationAlreadyPresent = isLocationAlreadyPresent;
    this.surveySetupService.selectLocation(this.isLocationAlreadyPresent);
    if (this.setupSurveyPagePath) {
      this.router.navigate([this.setupSurveyPagePath]);
    }
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.unsubscribeFromServiceVariables();
  }
  private unsubscribeFromServiceVariables() {
    if (this.surveySegmentSubscription) {
      this.surveySegmentSubscription.unsubscribe();
    }
    if (this.locationSubscription) {
      this.locationSubscription.unsubscribe();
    }
    if (this.showSearchCostCenterSubscription) {
      this.showSearchCostCenterSubscription.unsubscribe();
    }
    if (this.surveyTypeSubscription) {
      this.surveyTypeSubscription.unsubscribe();
    }
    if (this.currentCostCenterSubscription) {
      this.currentCostCenterSubscription.unsubscribe();
    }
  }
}
