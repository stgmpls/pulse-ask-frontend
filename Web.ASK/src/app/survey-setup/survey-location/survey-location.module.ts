import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SurveyLocationPage } from './survey-location.page';
import { CoreModule } from '../../core';
import { LocationOrSetupSurveyModule } from '../components/location-or-setup-survey/location-or-setup-survey.module';
import { CurrentLocationModule } from '../components/current-location/current-location.module';
import { SurveySegmentModule } from '../components/survey-segment/survey-segment.module';

const routes: Routes = [
  {
    path: '',
    component: SurveyLocationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoreModule,
    RouterModule.forChild(routes),
    LocationOrSetupSurveyModule,
    CurrentLocationModule,
    SurveySegmentModule
  ],
  declarations: [SurveyLocationPage]
})
export class SurveyLocationPageModule {}
