import { NgModule } from '@angular/core';
import { CustomSurveyComponent } from './custom-survey.component';
import { LeftNavigationComponent } from './components/lef-navigation/left-navigation.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { CustomSurveyTemplateComponent } from './components/custom-survey-template/custom-survey-template.component';
import { CoreModule } from '../../core';
import { LaunchAskBtnModule } from '../components/launch-ask-btn/launch-ask-btn.module';

@NgModule({
  imports: [IonicModule, CommonModule, CoreModule, LaunchAskBtnModule],
  declarations: [
    CustomSurveyComponent,
    LeftNavigationComponent,
    CustomSurveyTemplateComponent
  ],
  exports: [
    CustomSurveyComponent,
    LeftNavigationComponent,
    CustomSurveyTemplateComponent
  ]
})
export class CustomSurveyModule {}
