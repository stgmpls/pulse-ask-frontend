import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { SurveyService } from '../../core/services';
import { SurveySetupService } from '../survey-setup.service';
import { OrderByPipe } from '../../core/pipes/order-by.pipe';
import { SURVEY_TYPE } from '../constant/constant';

@Component({
  selector: 'app-custom-survey',
  templateUrl: './custom-survey.component.html',
  styleUrls: ['./custom-survey.component.scss']
})
export class CustomSurveyComponent implements OnInit {
  roleId: number;
  sectorId: string;
  username: string;
  surveyItems: any = [];
  surveyItemsCopy: any = [];
  resultOptionsForCustomSurvey = [];
  activeItemID = null;
  activeSurveyItem;
  currentActiveIndex = -1;
  isItemActive = true;
  smileyList = [];
  activeSmileyText = '';
  SURVEY_TYPE = SURVEY_TYPE;
  lastSurveyItemActiveIndex = -1;
  lastSurveyItemIndex: number;

  constructor(
    private surveyService: SurveyService,
    private surveySetupService: SurveySetupService,
    private orderByPipe: OrderByPipe
  ) {}

  ngOnInit() {
    this.fetchDataFromLocalStorage();
    this.setSurveyItems();
    this.setResultOptionsForCustomSurvey();
  }

  private fetchDataFromLocalStorage() {
    this.roleId = Number(localStorage.getItem('roleId'));
    this.sectorId = localStorage.getItem('sectorId');
    this.username = localStorage.getItem('username');
  }

  setSurveyItems() {
    this.getDisplayGroupData(
      this.username,
      this.sectorId,
      this.roleId,
      this.surveyService.surveyId,
      undefined,
      undefined,
      0,
      0,
      null,
      false,
      false,
      true
    );
  }

  public getDisplayGroupData(
    username,
    sectorId,
    roleId,
    surveyId,
    contextGroup,
    displayGroup,
    loopingIndex,
    uniqueId,
    parentId,
    canBacktrackBranches,
    inChild,
    isAdmin
  ) {
    this.surveyService
      .getDisplayGroupData(
        username,
        sectorId,
        roleId,
        surveyId,
        contextGroup,
        displayGroup,
        loopingIndex,
        uniqueId,
        parentId,
        canBacktrackBranches,
        inChild,
        isAdmin
      )
      .then((response: any) => {
        if (response && response.survey_item) {
          const surveyItems = this.orderByPipe.transform(
            JSON.parse(response.survey_item),
            'SurveyItemID'
          );
          this.surveyService.surveyData = surveyItems;
          this.surveyItemsCopy = JSON.parse(JSON.stringify(surveyItems));
          surveyItems.forEach(surveyItem => {
            this.initilizeSurveyItem(surveyItem);
          });
          this.surveyItems = surveyItems;
          this.setStartIndexOfSurveyItems();
          this.setlastIndexOfSurveyItem();
          if (this.surveyItems.length > 0) {
            this.renderCurrentSurveyItem(this.currentActiveIndex);
          }
        }
      });
  }

  // if surveyItem is parent then set activeItemID as SurveyItemID,isItemActive to true
  // if surveyItem is  followUp then set isItemActive to false
  setActiveSurveyItemState(index) {
    if (
      this.surveyItems[index].ParentTriggerResultOptionID == null ||
      this.surveyItems[index].ParentTriggerResultOptionID === 'null'
    ) {
      this.activeItemID = this.surveyItems[index].SurveyItemID;
      this.isItemActive = true;
    } else {
      this.isItemActive = false;
      const resultOptionOfParentSurveyItem = JSON.parse(
        this.surveyItems.find(item => item.SurveyItemID === this.activeItemID)
          .ResultOptions
      );

      const resultOptionItem = resultOptionOfParentSurveyItem.find(
        item =>
          item.ResultOptionId ===
          this.surveyItems[index].ParentTriggerResultOptionID
      );

      if (resultOptionItem) {
        this.activeSmileyText = resultOptionItem.Text;
      }
    }
  }

  renderCurrentSurveyItem(index) {
    this.setCurrentActiveSurveyItem(this.currentActiveIndex);
    this.setActiveSurveyItemState(this.currentActiveIndex);

    if (
      this.activeSurveyItem &&
      this.activeSurveyItem.ControlTypeCode === 'SM'
    ) {
      this.setSmileyList(this.activeSurveyItem.ResultOptionTypeID);
    }
  }

  // set  activeSurveyItem and if ControlTypeCode is 'SM' then set smiley list

  setCurrentActiveSurveyItem(index) {
    if (this.surveyItems.length > 0 && index <= this.surveyItems.length) {
      //  this.surveyItems[index].ResultOptions = JSON.parse(this.surveyItems[index].ResultOptions)
      this.activeSurveyItem = this.surveyItems[index];
    }
  }

  onSaveAndNext() {
    if (this.currentActiveIndex !== this.surveyItems.length - 1) {
      this.activeSurveyItem = null;
      if (
        this.currentActiveIndex + 1 <= this.surveyItems.length - 1 &&
        JSON.parse(
          this.surveyItems[this.currentActiveIndex + 1].ScaffoldIsVisible
        )
      ) {
        this.lastSurveyItemActiveIndex = this.currentActiveIndex;
        this.currentActiveIndex++;
        this.renderCurrentSurveyItem(this.currentActiveIndex);
      }
    }
  }

  // on click of item on left navigation, check type of item that is it ParentSurveyItem or followUpSurveyItem.
  // if parentSurveyItem then set  elementPos to poostion of surevyItem by SurveyItemID.

  onItemChange(SurveyItemID) {
    this.activeSurveyItem = null;
    let elementPos = -1;
    elementPos = this.getPositionOfSurveyItem(SurveyItemID);
    if (elementPos !== -1) {
      this.lastSurveyItemActiveIndex = this.currentActiveIndex;
      this.currentActiveIndex = elementPos;
      this.renderCurrentSurveyItem(this.currentActiveIndex);
    }
  }
  // return position of surveyItem comapring surveyItemID
  getPositionOfSurveyItem(SurveyItemID) {
    const elementPos = this.surveyItems
      .map(item => item.SurveyItemID)
      .indexOf(SurveyItemID);

    return elementPos;
  }

  setResultOptionsForCustomSurvey() {
    this.surveySetupService.getResultOptionsForCustomSurvey().subscribe(
      res => {
        if (res.Data && res.Data.length > 0) {
          this.resultOptionsForCustomSurvey = res.Data;
        }
      },
      err => {
        console.error(err);
      }
    );
  }

  onSmileyOptionChange(resultOptionTypeId) {
    this.setSmileyList(resultOptionTypeId);
    this.clearFollowUpQuestion();
  }

  setSmileyList(resultOptionTypeId) {
    const smileyList = this.resultOptionsForCustomSurvey.find(
      resultOptionType =>
        resultOptionTypeId === resultOptionType.ResultOptionTypeId
    );
    if (smileyList) {
      this.smileyList = smileyList;
    }
  }

  onClear() {
    this.activeSurveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      this.activeSurveyItem.SurveyItemID
    );

    if (this.activeSurveyItem.ControlTypeCode === 'SM') {
      this.activeSurveyItem.ResultOptionTypeID = this.surveyItemsCopy[
        this.currentActiveIndex
      ].ResultOptionTypeID;
      this.clearFollowUpQuestion();
    }
    if (this.activeSurveyItem.ControlTypeCode === 'RB') {
      this.activeSurveyItem.Result.FollowupOptions = JSON.parse(
        this.surveyItemsCopy[this.currentActiveIndex].ResultOptions
      ).map(option => option.Text);
    }
  }

  clearFollowUpQuestion() {
    for (
      let i = this.currentActiveIndex + 1;
      i < this.surveyItems.length;
      i++
    ) {
      if (
        this.surveyItems[i].ParentTriggerResultOptionID === null ||
        this.surveyItems[i].ParentTriggerResultOptionID === 'null'
      ) {
        break;
      }
      this.surveyItems[
        i
      ].Result = this.surveyService.createSurveyItemResultInstance(
        this.surveyItems[i].SurveyItemID
      );
      this.surveyItems[i].Result.FollowupOptions = [];
    }
  }

  setStartIndexOfSurveyItems() {
    for (let i = 0; i < this.surveyItems.length; i++) {
      if (JSON.parse(this.surveyItems[i].ScaffoldIsVisible)) {
        this.currentActiveIndex = i;
        break;
      }
    }
  }

  setlastIndexOfSurveyItem() {
    for (let i = 0; i < this.surveyItems.length; i++) {
      if (JSON.parse(this.surveyItems[i].ScaffoldIsVisible)) {
        this.lastSurveyItemIndex = i;
      }
    }
  }

  initilizeSurveyItem(surveyItem) {
    surveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      surveyItem.SurveyItemID
    );
    if (surveyItem.ControlTypeCode === 'RB') {
      surveyItem.Result.FollowupOptions = JSON.parse(
        surveyItem.ResultOptions
      ).map(item => {
        return { Text: item.Text };
      });
    }
    if (this.surveyService.isEditMode) {
      surveyItem.Result.ResultText = surveyItem.Text;
    }
  }
}
