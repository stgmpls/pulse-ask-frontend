import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { LaunchAskBtnComponent } from '../../../components/launch-ask-btn/launch-ask-btn.component';

@Component({
  selector: 'app-left-navigation',
  styleUrls: ['./left-navigation.component.scss'],
  templateUrl: './left-navigation.component.html'
})
export class LeftNavigationComponent implements OnChanges {
  @Input() activeItemID: number;
  @Input() isItemActive = true;
  @Input() surveyItems = [];
  @Input() smileyList = [];
  @Input() activeSmileyText = '';
  @Output() itemChange: EventEmitter<any> = new EventEmitter();
  @Input() surveyType = '';
  @Input() requiredSurveyItemIdArray = [];
  @Input() lastSurveyItemActiveIndex;
  @Input() currentActiveIndex;
  @ViewChild('lAskBtnComp') launchAskBtnComponent: LaunchAskBtnComponent;
  expandSurveyItemIdObj;

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes &&
      changes.lastSurveyItemActiveIndex &&
      this.lastSurveyItemActiveIndex !== -1
    ) {
      this.launchAskBtnComponent.getInvalidCustomSurveyItem(
        new Array(this.surveyItems[this.lastSurveyItemActiveIndex])
      );
    }
  }

  itemClicked(SurveyItemID, index) {
    if (this.currentActiveIndex !== index) {
      this.itemChange.emit(SurveyItemID);
    }
  }

  onSelectSmiley(selectedItem) {
    this.itemClicked(selectedItem.SurveyItemID, selectedItem.index);
  }

  onValidation(requiredSurveyItemIdArray) {
    if (this.requiredSurveyItemIdArray.length === 0) {
      this.requiredSurveyItemIdArray = requiredSurveyItemIdArray;
      return;
    } else if (requiredSurveyItemIdArray.length === 0) {
      const index = this.requiredSurveyItemIdArray.indexOf(
        this.surveyItems[this.lastSurveyItemActiveIndex].SurveyItemID
      );
      if (index !== -1) {
        this.requiredSurveyItemIdArray.splice(index, 1);
      }
    }

    this.requiredSurveyItemIdArray = this.arrayUnique([
      ...this.requiredSurveyItemIdArray,
      ...requiredSurveyItemIdArray
    ]);
  }

  arrayUnique(array) {
    const a = array.concat();
    for (let i = 0; i < a.length; ++i) {
      for (let j = i + 1; j < a.length; ++j) {
        if (a[i] === a[j]) {
          a.splice(j--, 1);
        }
      }
    }

    return a;
  }

  inRequiredSurveyItemIdArray(surveyItemID) {
    let surveyItemIndex;
    let isFollowupInvalid = false;
    const resultOptions = JSON.parse(
      this.surveyItems.find((item, index) => {
        if (item.SurveyItemID === surveyItemID) {
          surveyItemIndex = index;
          return true;
        }
        return false;
      }).ResultOptions
    );

    resultOptions.map(resultOption => {
      for (let i = surveyItemIndex + 1; i < this.surveyItems.length; i++) {
        if (
          JSON.parse(this.surveyItems[i].ParentTriggerResultOptionID) === null
        ) {
          break;
        }
        if (
          JSON.parse(this.surveyItems[i].ParentTriggerResultOptionID) ===
            resultOption.ResultOptionId &&
          this.requiredSurveyItemIdArray.indexOf(
            this.surveyItems[i].SurveyItemID
          ) !== -1
        ) {
          isFollowupInvalid = true;
        }
      }
    });
    return (
      this.requiredSurveyItemIdArray.indexOf(surveyItemID) !== -1 ||
      isFollowupInvalid
    );
  }
}
