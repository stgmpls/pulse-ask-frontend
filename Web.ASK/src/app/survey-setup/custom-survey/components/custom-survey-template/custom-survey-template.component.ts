import {
  Component,
  Input,
  OnInit,
  OnChanges,
  EventEmitter,
  Output
} from '@angular/core';

@Component({
  selector: 'app-custom-survey-template',
  templateUrl: './custom-survey-template.component.html',
  styleUrls: ['./custom-survey-template.component.scss']
})
export class CustomSurveyTemplateComponent {
  @Input() activeSurveyItem;
  @Input() resultOptionsForCustomSurvey;
  @Input() currentActiveIndex;
  @Input() lastSurveyItemIndex;
  @Output() saveNext: EventEmitter<any> = new EventEmitter();
  @Output() smileyOptionChange: EventEmitter<any> = new EventEmitter();
  @Output() clear: EventEmitter<any> = new EventEmitter();
  isValid = true;
  isControlValid = true;

  // emit event on click of save and next button
  onSaveAndNext() {
    if (this.isValidSurveyItem()) {
      this.saveNext.emit();
    }
  }

  // catch event from smiley dropdown and emit event
  onSmileyOptionChange(resultOptionTypeId) {
    this.smileyOptionChange.emit(resultOptionTypeId);
  }

  onClear() {
    this.clear.emit();
  }

  isValidSurveyItem() {
    if (
      JSON.parse(this.activeSurveyItem.IsRequired) &&
      (this.activeSurveyItem.Result.ResultText.trim() === '' &&
        (this.activeSurveyItem.Result.ResultOptionId === null ||
          this.activeSurveyItem.Result.ResultOptionId.length === 0))
    ) {
      this.isValid = false;
      return false;
    }
    this.isValid = true;
    if (
      JSON.parse(this.activeSurveyItem.ParentTriggerResultOptionID) !== null &&
      this.activeSurveyItem.ControlTypeCode === 'RB' &&
      this.activeSurveyItem.Result.ResultText.trim() !== ''
    ) {
      if (
        this.activeSurveyItem.Result.FollowupOptions.length === 0 ||
        this.activeSurveyItem.Result.FollowupOptions.find(
          option => option.Text === ''
        )
      ) {
        this.isControlValid = false;
        return false;
      }
    }
    this.isControlValid = true;
    return true;
  }
}
