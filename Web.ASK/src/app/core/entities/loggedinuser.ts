import { Userdata } from './userdata';
export class Loggedinuser {
    id: number;

    user_id: number;

    name: string;

    user_name: string;

    user_data?: Userdata;

    last_view_state: any;
}
