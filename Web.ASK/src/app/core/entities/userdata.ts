export class Userdata {
  username: string;
  name: string;
  apiToken: string;
  refreshToken: string;
  tokenExpiryTime: number;
  refreshTokenExpiryTime: number;
  isADUser: string;
  isLocationSearchAllowed: boolean;
  loginTime: number;
}
