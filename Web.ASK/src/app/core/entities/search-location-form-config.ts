export interface FormConfig {
  costCenterNumber: FormFieldConfig;
  costCenterName: FormFieldConfig;
  zipCode: FormFieldConfig;
  state: FormFieldConfig;
  city: FormFieldConfig;
  searchBtn: { size: string; placeholder: string };
}
export interface FormFieldConfig {
  size: string;
  placeholder: string;
  defaultValue: string;
}
