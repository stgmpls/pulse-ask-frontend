export class Location {
  Id: number;

  LocationId: number;

  LocationDataSource: string;

  SectorAdamId: string;

  LocationDescription: string;
}
