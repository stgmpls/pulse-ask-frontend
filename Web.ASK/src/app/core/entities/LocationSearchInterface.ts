export interface ILocationSearch {
    zip_code: string;
    name: string;
    state: string;
    city: string;
    number: string;
}
