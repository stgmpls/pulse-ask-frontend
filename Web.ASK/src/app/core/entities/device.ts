export class Device {
    RegistrationId: string;
    Model: string;
    Platform: string;
    Manufacturer: string;
    Uuid: string;
    OsVersion: string;
}
