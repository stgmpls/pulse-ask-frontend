import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  database: SQLiteObject;
  window: any = window;
  private databaseReady: BehaviorSubject<boolean>;
  constructor(private sqlite: SQLite, private platform: Platform) {
    this.platform.ready().then(() => {
      this.createDatabase();
    });
  }

  createDatabase() {
    if (!this.platform.is('cordova') || this.platform.is('desktop')) {
      // app is running on device
      this.database = this.window.openDatabase(
        'compass.db',
        '1.0',
        'compass database',
        2 * 1024 * 1024
      );
      if (this.database) {
        this.createTables();
      }
    } else {
      if (this.platform.is('android')) {
        this.sqlite
          .create({
            name: 'compass.db',
            location: 'default'
          })
          .then((db: SQLiteObject) => {
            this.database = db;
            this.createTables();
          });
      } else {
        this.sqlite
          .create({
            name: 'compass.db',
            iosDatabaseLocation: 'Documents'
          })
          .then((db: SQLiteObject) => {
            this.database = db;
            this.createTables();
          });
      }
    }
  }

  createTables() {
    Promise.all([
      this.execute(
        this.database,
        `CREATE TABLE IF NOT EXISTS user(id integer primary key,userId integer, name text,
           username text, user_data blob)`
      ).then(_data => {}),
      this.execute(
        this.database,
        `CREATE TABLE IF NOT EXISTS location(Id integer primary key, DataSourceCode text, LocationId integer,
           SectorAdamId text, LocationDescription text)`
      ).then(_data => {}),
      this.execute(
        this.database,
        `CREATE TABLE IF NOT EXISTS user_surveys(survey_unique_id integer primary key,
           username text, sector_id text, role_id integer, survey_id integer, survey_content blob, survey_category text)`
      ).then(_data => {}),
      this.execute(
        this.database,
        `CREATE UNIQUE INDEX IF NOT EXISTS username_survey_id on user_surveys(username, sector_id,
           role_id, survey_id)`
      ).then(_data => {}),
      this.execute(
        this.database,
        `CREATE TABLE IF NOT EXISTS user_surveys_details(survey_unique_id integer,
           unique_id integer, parent_id integer, context_group_id integer, context_group_title text,
            display_group_id integer, is_looping text, isLast text, no_data_rule_code text,
             no_data_message text, isFirstChild text)`
      ).then(_data => {}),
      this.execute(
        this.database,
        `CREATE TABLE IF NOT EXISTS user_survey_items(survey_unique_id integer,
           survey_detail_unique_id integer, survey_item_id integer, item_text text, item_id integer,
            item_type_code text, control_type_code text, parent_trigger_result_option_id integer,
             action_type integer, is_required integer, collapse_answer integer,
              result_text_max_length integer, limit_date_range integer,
               result_text_max_date_day_offset integer, result_text_min_date_day_offset integer,
                numbering text, help_text text, info_text text, class_name text,
                 result_text_data_type text, result_options blob, scaffold_item_text text,
                  scaffold_control_type_id integer, scaffold_result_option_type_id integer,
                   scaffold_default_value text, scaffold_is_read_only integer,
                    scaffold_is_visible integer, result_option_type_id integer,
                     scaffold_control_type_code text)`
      ).then(_data => {}),
      this.execute(
        this.database,
        `CREATE TABLE IF NOT EXISTS user_survey_item_result(
          survey_item_result_id integer primary key,  username text, sector_id text,
           role_id integer, survey_id integer, survey_item_result blob, status text,
            start_date text, modified_date text, survey_instance_id integer, location_id integer,
             location text, is_deleted integer default 0, is_end text)`
      ).then(_data => {}),
      this.execute(
        this.database,
        `CREATE TABLE IF NOT EXISTS active_survey_details(
          survey_item_result_id integer, survey_unique_id interger, unique_id integer, display_group_id integer, level_parent_id integer)`
      ).then(_data => {})
    ]).then(_data => {});
  }

  execute(db, query, params?: any) {
    return new Promise((resolve, _reject) => {
      if (params === null) {
        params = [];
      }
      if (!this.platform.is('cordova') || this.platform.is('desktop')) {
        // tslint:disable-next-line: no-unused-expression
        db &&
          db.transaction(function(tx) {
            tx.executeSql(
              query,
              params,
              function(_tx, result) {
                resolve(result);
              },
              function(ee, ees) {
                console.log(ee, ees); // TO DO :- Replace this code with logger service.
              }
            );
          });
      } else {
        return resolve(db.executeSql(query, params));
      }
    });
  }

  executeBatch(queries: string[]) {
    if (!this.platform.is('cordova')) {
      const promieses: Array<Promise<any>> = [];
      queries.forEach(query => {
        promieses.push(this.execute(this.database, query, null).then(() => {}));
      });
      return Promise.all(promieses);
    } else {
      return this.database.sqlBatch(queries);
    }
  }

  executeSql(query, params) {
    return this.execute(this.database, query, params);
  }

  getDatabaseState() {
    return this.databaseReady.asObservable();
  }

  /*
  Parameters:
    data.tableName : Table that you want to modify
    data.columnName : Column that is being added
    data.type : Data type of that colunmn
    data.contraint : optional parameter to add constraint on new column
  */
  checkAndCreateNewColumns(data) {
    this.execute(
      this.database,
      `SELECT 1 from sqlite_master WHERE tbl_name = '${data.tableName}' and sql like '%${data.columnName}%'`
    ).then((resp: any) => {
      console.log('checkAndCreateNewColumns', resp); // TO DO :- Replace this code with logger service.
      if (resp && resp.rows.length > 0) {
        console.log(
          'Column exist no need to alter',
          ' Dbprovider ',
          'checkAndCreateNewColumns'
        ); // TO DO :- Replace this code with logger service.
      } else {
        let query;
        if (data.constraint && data.constraint !== '') {
          query =
            'ALTER TABLE ' +
            data.tableName +
            ' ADD ' +
            data.columnName +
            ' ' +
            data.type +
            ' ' +
            data.constraint;
        } else {
          query =
            'ALTER TABLE ' +
            data.tableName +
            ' ADD ' +
            data.columnName +
            ' ' +
            data.type;
        }

        this.execute(this.database, query).then(_response => {
          console.log('Altered the table-', data.tableName); // TO DO :- Replace this code with logger service.
        });
      }
    });
  }
}
