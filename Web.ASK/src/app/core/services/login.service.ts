import { Injectable } from '@angular/core';
import { APIMethod } from './data.service';
import { AuthenticationService } from './authentication.service';
import { DbService } from './db.service';
import { DataService } from './data.service';
import { APIRequest, Loggedinuser } from '../entities';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(
    private authenticationService: AuthenticationService,
    private dataService: DataService,
    private db: DbService
  ) {}

  public authenticateUser(
    userName: string,
    password: string,
    deviceData?: any
  ) {
    return this.authenticationService.login(userName, password, deviceData);
  }

  public testApi() {
    const params: string =
      'api/Survey/AllFeedbackSurveysBySectorId?SectorId=' + undefined;
    const request: APIRequest = new APIRequest(params, APIMethod.GET);
    return this.dataService.execute(request);
  }

  public setUser(user: Loggedinuser) {
    const query =
      'INSERT INTO user (userId, name, username, user_data) VALUES (?,?,?,?)';
    return this.db.executeSql(query, [
      user.user_id,
      user.name,
      user.user_name,
      JSON.stringify(user.user_data)
    ]);
  }

  public getCurrentUserInfo() {
    const params = 'api/user/GetCurrentUserInfo';
    const request: APIRequest = new APIRequest(params, APIMethod.GET);
    return this.dataService.executeAPI(request);
  }
}
