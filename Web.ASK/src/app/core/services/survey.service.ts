import { Injectable } from '@angular/core';
import { APIRequest } from '../entities';
import { Subject } from 'rxjs/Subject';
import { APIMethod } from './data.service';
import { DbService } from './db.service';
import { DataService } from './data.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class SurveyService {
  private readonly locationTableName = 'location';
  public surveyData: any;
  public originalSurveyId: any;
  public surveyId: any;
  public surveyDescription: any;
  public isEditMode = false;
  public editSurveyPlace: any;
  public logoutTime: any;
  public questionResultId = -1;
  constructor(private dataService: DataService, private db: DbService) {}

  // Observable any source
  private predefinedMasterSurveys = new Subject<any>();
  private tastingMasterSurveys = new Subject<any>();

  // Observable stream
  predefinedMasterSurveys$ = this.predefinedMasterSurveys.asObservable();
  tastingMasterSurveys$ = this.tastingMasterSurveys.asObservable();

  // Service message commands
  setPredefinedMasterSurveys(predefinedMasterSurveys: any) {
    this.predefinedMasterSurveys.next(predefinedMasterSurveys);
  }
  setTastingMasterSurveys(tastingMasterSurveys: any) {
    this.tastingMasterSurveys.next(tastingMasterSurveys);
  }

  public getUserAdHocForms(roleId: number) {
    const params: string = 'api/survey/UserAdHocForms/' + roleId;
    const request: APIRequest = new APIRequest(params, APIMethod.GET);
    return this.dataService.executeAPI(request);
  }

  public getAllSurveys(
    roleId: number,
    sectorAdamId: string,
    deviceTypeCode: string,
    isAll: boolean,
    surveyId: number
  ) {
    const params = `api/survey/UserSurveys/${roleId}/${sectorAdamId}/${deviceTypeCode}?isAskTemplate=${isAll}&surveyId=${surveyId}`;
    const request: APIRequest = new APIRequest(params, APIMethod.GET);
    return this.dataService.executeAPI(request);
  }

  /**
         Insert or update survey into the database.
        **/
  public saveSurvey(
    username,
    sectorId,
    roleId,
    surveyId,
    surveyContent,
    surveyCategory
  ) {
    const query = `SELECT * from user_surveys WHERE username=? AND sector_id=? AND role_id=? AND survey_id=?`;
    return this.db
      .executeSql(query, [username, sectorId, roleId, surveyId])
      .then((result: any) => {
        if (result.rows && result.rows.length === 0) {
          const insertQuery = `INSERT INTO user_surveys (username, sector_id, role_id, survey_id, survey_content, survey_category) VALUES
             (?,?,?,?,?,?)`;
          return this.db
            .executeSql(insertQuery, [
              username,
              sectorId,
              roleId,
              surveyId,
              surveyContent,
              surveyCategory
            ])
            .then(
              function(insertResult: any) {
                return insertResult.insertId;
              },
              function(error: any) {
                if (
                  error.code === 6 &&
                  error.message.indexOf('UNIQUE constraint failed') !== -1
                ) {
                  const updateQuery = `UPDATE user_surveys set survey_content=? WHERE sector_id=? AND role_id=?
                     AND survey_id=?`;
                  return this.db
                    .executeSql(updateQuery, [
                      surveyContent,
                      sectorId,
                      roleId,
                      surveyId
                    ])
                    .then(() => {
                      return result.rows.item(0).survey_unique_id;
                    });
                }
              }
            );
        } else {
          const updateQuery = `UPDATE user_surveys set survey_content=? WHERE username=? AND sector_id=? AND role_id=?
             AND survey_id=?`;
          return this.db
            .executeSql(updateQuery, [
              surveyContent,
              username,
              sectorId,
              roleId,
              surveyId
            ])
            .then(() => {
              return result.rows.item(0).survey_unique_id;
            });
        }
      });
  }

  public updateSurveyDetails(
    userName,
    sectorId,
    roleId,
    surveyId,
    surveyDetails
  ) {
    return this.db
      .executeSql(
        'SELECT survey_unique_id FROM user_surveys WHERE survey_id=? AND username=? AND sector_id=? AND role_id=?',
        [surveyId, userName, sectorId, roleId]
      )
      .then((result: any) => {
        const surveyUniqueId = result.rows.item(0).survey_unique_id;
        let index = 0;
        surveyDetails.forEach(element => {
          const surveyDetail = element[index]; // TODO: Hack for now. The ConvertObjectCollection to Array needs to be adjusted.
          index++;
          const updateSurveyDetailQuery =
            'UPDATE user_surveys_details SET context_group_title=?, ' +
            'is_looping=?, ' +
            'no_data_rule_code=?, ' +
            'no_data_message=? ' +
            'WHERE survey_unique_id=? AND context_group_id=? AND display_group_id=?';
          this.db.executeSql(updateSurveyDetailQuery, [
            surveyDetails.ContextGroupTitle,
            surveyDetail.IsLooping,
            surveyDetail.NoDataRuleCode,
            surveyDetail.NoDataMessage,
            surveyUniqueId,
            surveyDetail.ContextGroupId,
            surveyDetail.DisplayGroupId
          ]);
          surveyDetail.SurveyItems.forEach(surveyItem => {
            const updateSurveyItemQuery =
              'UPDATE user_survey_items SET item_text=?, ' +
              'item_type_code=?, ' +
              'control_type_code=?, ' +
              'parent_trigger_result_option_id=?, ' +
              'action_type=?, ' +
              'is_required=?, ' +
              'collapse_answer=?, ' +
              'result_text_max_length=?, ' +
              'limit_date_range=?, ' +
              'result_text_max_date_day_offset=?, ' +
              'result_text_min_date_day_offset=?, ' +
              'class_name=?, ' +
              'result_text_data_type=?, ' +
              'result_options=?, ' +
              'scaffold_item_text=?, ' +
              'scaffold_control_type_id=?, ' +
              'scaffold_result_option_type_id=?, ' +
              'scaffold_default_value=?, ' +
              'scaffold_is_read_only=?, ' +
              'scaffold_is_visible=?, ' +
              'result_option_type_id=?, ' +
              'scaffold_control_type_code=? ' +
              'WHERE survey_unique_id=? AND survey_item_id=?';

            this.db.executeSql(updateSurveyItemQuery, [
              surveyItem.Text,
              surveyItem.ItemTypeCode,
              surveyItem.ControlTypeCode,
              surveyItem.ParentTriggerResultOptionID,
              surveyItem.ActionType,
              surveyItem.IsRequired,
              surveyItem.CollapseAnswer,
              surveyItem.ResultTextMaxLength,
              surveyItem.LimitDateRange,
              surveyItem.ResultTextMaxDateDayOffset,
              surveyItem.ResultTextMinDateDayOffset,
              surveyItem.ClassName,
              surveyItem.ResultTextDataType,
              JSON.stringify(surveyItem.ResultOptions),
              surveyItem.ScaffoldItemText,
              surveyItem.ScaffoldControlTypeID,
              surveyItem.ScaffoldResultOptionTypeID,
              surveyItem.ScaffoldDefaultValue,
              surveyItem.ScaffoldIsReadOnly,
              surveyItem.ScaffoldIsVisible,
              surveyItem.ResultOptionTypeID,
              surveyItem.ScaffoldControlTypeCode,
              surveyUniqueId,
              surveyItem.SurveyItemID
            ]);
          });
        });
      });
  }

  /**
         Insert or update survey content into the database.
        **/

  public saveSurveyContent(
    surveyUniqueId,
    surveyValue,
    surveyItemsArray,
    userSurveysDetails
  ) {
    const surveyItems = [];
    surveyItemsArray.forEach(element => {
      element.items.forEach(item => {
        surveyItems.push({
          detailUniqueId: element.detailUniqueId,
          item: item
        });
      });
    });
    const deleteQuery = `DELETE  from user_surveys_details WHERE user_surveys_details.survey_unique_id = ?`;
    return this.db.executeSql(deleteQuery, [surveyValue]).then(() => {
      const deleteSurveyItemsQuery = `DELETE  from user_survey_items WHERE user_survey_items.survey_unique_id = ?`;
      return this.db
        .executeSql(deleteSurveyItemsQuery, [surveyValue])
        .then(() => {
          const surveyDetailValues = userSurveysDetails.join(',');
          if (surveyDetailValues !== '') {
            const insertQuery =
              `INSERT INTO user_surveys_details(survey_unique_id, unique_id, parent_id, context_group_id,
               context_group_title, display_group_id, is_looping, isLast, isFirstChild,
                no_data_rule_code, no_data_message) VALUES ` +
              surveyDetailValues;
            return this.db.executeSql(insertQuery, []).then(() => {
              this.saveSurveyItems(surveyItems, surveyUniqueId);
            });
          }
        });
    });
  }

  /**
         Insert or update survey items into the database.
        **/
  public saveSurveyItems(jsonArray, surveyId) {
    const queries: string[] = [];
    jsonArray.forEach(element => {
      let resultOptions = JSON.stringify(element.item.ResultOptions);
      // tslint:disable-next-line: quotemark
      element.item.Text = element.item.Text.replace(/'/g, '""');
      element.item.ScaffoldItemText = element.item.ScaffoldItemText.replace(/'/g, '""');
      resultOptions = resultOptions.replace(/'/g, '""');
      const insertQuery = `INSERT INTO user_survey_items(survey_unique_id, survey_detail_unique_id, survey_item_id,
           item_text, item_id, item_type_code, control_type_code, parent_trigger_result_option_id,
            action_type, is_required, collapse_answer, result_text_max_length, limit_date_range,
             result_text_max_date_day_offset, result_text_min_date_day_offset, numbering, help_text,
              info_text, class_name, result_text_data_type, result_options, scaffold_item_text,
               scaffold_control_type_id, scaffold_result_option_type_id, scaffold_default_value,
                scaffold_is_read_only, scaffold_is_visible, result_option_type_id,
                 scaffold_control_type_code) VALUES ('
        ${surveyId}',
         '${element.detailUniqueId}',
         '${element.item.SurveyItemID}',
         '${element.item.Text}',
         '${element.item.ItemID}',
         '${element.item.ItemTypeCode}',
         '${element.item.ControlTypeCode}',
         '${element.item.ParentTriggerResultOptionID}',
         '${element.item.ActionType}',
         '${element.item.IsRequired}',
         '${element.item.CollapseAnswer}',
         '${element.item.ResultTextMaxLength}',
         '${element.item.LimitDateRange}',
         '${element.item.ResultTextMaxDateDayOffset}',
         '${element.item.ResultTextMinDateDayOffset}',
         '${element.item.Numbering}',
         '${element.item.HelpText}',
         '${element.item.InfoText}',
         '${element.item.ClassName}',
         '${element.item.ResultTextDataType}',
         '${resultOptions}',
         '${element.item.ScaffoldItemText}',
         '${element.item.ScaffoldControlTypeID}',
         '${element.item.ScaffoldResultOptionTypeID}',
         '${element.item.ScaffoldDefaultValue}',
         '${element.item.ScaffoldIsReadOnly}',
         '${element.item.ScaffoldIsVisible}',
         '${element.item.ResultOptionTypeID}',
         '${element.item.ScaffoldControlTypeCode}')`;
      queries.push(insertQuery);
    });
    return this.db.executeBatch(queries).then(() => {});
  }

  public getDisplayGroupData(
    username,
    sectorId,
    roleId,
    surveyId,
    contextGroupId,
    displayGroupId,
    loopingIndex,
    uniqueId,
    parentId,
    canBacktrackBranches,
    inChild,
    isAdmin
  ): any {
    const surveyDetailUniqueId = 0;
    let selectItems: string;
    if (!isAdmin) {
      return this.fetchDisplayGroupData(
        username,
        sectorId,
        roleId,
        contextGroupId,
        displayGroupId,
        loopingIndex,
        uniqueId,
        parentId,
        surveyId,
        canBacktrackBranches,
        inChild
      ).then((result: any) => {
        return this.getSurveyItemOfDisplayGroup(
          result,
          username,
          sectorId,
          roleId,
          surveyId,
          isAdmin
        );
      });
    } else {
      selectItems = `SELECT user_survey_items.survey_unique_id, user_survey_items.survey_detail_unique_id,
         survey_item_id, item_text, item_id, item_type_code, control_type_code,
          parent_trigger_result_option_id, action_type, is_required, collapse_answer,
           result_text_max_length, limit_date_range, result_text_max_date_day_offset,
            result_text_min_date_day_offset, numbering, help_text, info_text, class_name,
             result_text_data_type, result_options, scaffold_item_text, scaffold_control_type_id,
              scaffold_result_option_type_id, scaffold_default_value, scaffold_is_read_only,
               scaffold_is_visible, result_option_type_id, scaffold_control_type_code
                FROM user_surveys_details INNER JOIN user_surveys ON user_surveys_details.survey_unique_id
                 = user_surveys.survey_unique_id INNER JOIN user_survey_items ON
                  user_survey_items.survey_unique_id = user_surveys.survey_unique_id AND
                   user_survey_items.survey_detail_unique_id = user_surveys_details.unique_id
                    AND user_surveys.username = ? AND user_surveys.sector_id = ? AND user_surveys.role_id
                     = ? WHERE user_surveys.survey_id = ? ORDER BY user_surveys_details.display_group_id ASC`;
      return this.db
        .executeSql(selectItems, [username, sectorId, roleId, surveyId])
        .then((result: any) => {
          return this.getSurveyItemOfDisplayGroup(
            result,
            username,
            sectorId,
            roleId,
            surveyId,
            isAdmin
          );
        });
    }
  }

  public createSurveyItemResultInstance(surveyItemId, loopingIndex?: any) {
    return {
      SurveyInstanceId: 0,
      SurveyItemID: surveyItemId,
      ResultOptionId: null,
      ResultText: '',
      IsSelected: null,
      LoopingIndex: loopingIndex === undefined ? 0 : loopingIndex,
      FilterIdentifier: null
    };
  }

  public launchAsk(survey, originalSurveyId, deviceId, roleId) {
    const request: APIRequest = new APIRequest(
      `api/survey/${originalSurveyId}/copy?deviceId=${deviceId}&roleId=${roleId}`,
      APIMethod.POST
    );
    request.addAll(survey);
    return this.dataService.execute(request);
  }

  public launchAskCustomSurvey(
    adminCustomSurveyResults,
    originalSurveyId,
    deviceId,
    roleId
  ) {
    const request: APIRequest = new APIRequest(
      `api/survey/surveys/${originalSurveyId}/copy/${deviceId}?roleId=${roleId}`,
      APIMethod.POST
    );

    request.addAll(adminCustomSurveyResults);
    return this.dataService.execute(request);
  }

  public saveSurveyItemResult(
    username,
    sectorId,
    roleId,
    questionResultId,
    surveyId,
    surveyItemValues,
    endSurvey
  ) {
    let surveyItemResult = [];
    return this.getResults(
      username,
      sectorId,
      roleId,
      true,
      surveyItemValues,
      questionResultId
    ).then((_surveyItemResult: any) => {
      surveyItemResult = _surveyItemResult;
      if (questionResultId === -1) {
        questionResultId = 0;
      }
      return this.saveSurveyItemData(
        username,
        sectorId,
        roleId,
        questionResultId,
        surveyId,
        surveyItemResult,
        endSurvey
      );
    });
  }

  /**
        This function is used to save the answers of survey in local database.
        **/
  public saveSurveyItemData(
    username,
    sectorId,
    roleId,
    questionResultId,
    surveyId,
    surveryItemResult,
    endSurvey
  ) {
    let locationId = '';
    const status = 'InProgress';
    const survey_item_date = moment
      .utc()
      .local()
      .format('MM-DD-YYYY HH:mm');
    const surveyInstanceId = 0;
    if (questionResultId === 0) {
      return this.db
        .executeSql(`SELECT LocationId FROM location`, [])
        .then((locationResult: any) => {
          if (
            locationResult.rows &&
            locationResult.rows.length > 0 &&
            locationResult.rows.item(0)
          ) {
            locationId = locationResult.rows.item(0).LocationId;
            const location = localStorage.getItem('selectedLocation');
            const insertQuery = `INSERT INTO user_survey_item_result(username, sector_id, role_id, survey_id, survey_item_result,
                 status, start_date, modified_date, survey_instance_id, location_id, location, is_end)
                  VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`;
            return this.db
              .executeSql(insertQuery, [
                username,
                sectorId,
                roleId,
                surveyId,
                JSON.stringify(surveryItemResult),
                status,
                survey_item_date,
                survey_item_date,
                surveyInstanceId,
                locationId,
                location,
                endSurvey
              ])
              .then(
                function(response: any) {
                  return response.insertId;
                },
                function(error) {
                  console.log(
                    'Error in saveSurveyItemData form' + error.message
                  ); // TO DO :- Replace this code with logger service.
                }
              );
          }
        });
    } else {
      return this.db
        .executeSql(
          `UPDATE user_survey_item_result set survey_item_result = ?, modified_date=?, is_end = ?
           WHERE username = ? AND sector_id = ? AND role_id = ? AND survey_item_result_id = ?`,
          [
            JSON.stringify(surveryItemResult),
            survey_item_date,
            endSurvey,
            username,
            sectorId,
            roleId,
            questionResultId
          ]
        )
        .then(
          function(result) {
            return questionResultId;
          },
          function(error) {
            console.log('Error in saveSurveyItemData' + error.message); // TO DO :- Replace this code with logger service.
            return false;
          }
        );
    }
  }

  public insertSurveys(surveyList, surveyCount, username, sectorId, roleId) {
    if (surveyList.length > 0) {
      return this.saveSurvey(
        username,
        sectorId,
        roleId,
        surveyList[surveyCount].Header.SurveyId,
        JSON.stringify(surveyList[surveyCount].Header),
        surveyList[surveyCount].Header.Category
      ).then(response => {
        if (response > 0) {
          const surveyItems = [];
          const userSurveysDetails = [];
          for (const key in surveyList[surveyCount].Details) {
            if (surveyList[surveyCount].Details.hasOwnProperty(key)) {
              const detail = surveyList[surveyCount].Details[key];
              surveyItems.push({
                detailUniqueId: parseInt(key, 10),
                items: detail.SurveyItems
              });
              if (key === '0') {
                userSurveysDetails.push(
                  `(${response},
                  ${parseInt(key, 10)},
                  ${detail.ParentId},
                  ${detail.ContextGroupId},
                  '${detail.ContextGroupTitle}',
                  ${detail.DisplayGroupId},
                  '${detail.IsLooping}',
                  '${detail.IsLast}',
                  ${detail.IsFirstChild},
                  '${detail.NoDataRuleCode}',
                  '${detail.NoDataMessage}')`
                );
              } else {
                userSurveysDetails.push(
                  `(${response},
                  ${parseInt(key, 10)},
                  ${detail.ParentId},
                  ${detail.ContextGroupId},
                  '${detail.ContextGroupTitle}',
                  ${detail.DisplayGroupId},
                  '${detail.ContextGroupTitle}',
                  '${detail.IsLast}',
                  ${detail.IsFirstChild},
                  '${detail.NoDataRuleCode}',
                  '${detail.NoDataMessage}')`
                );
              }
            }
          }
          return this.saveSurveyContent(
            response,
            response,
            surveyItems,
            userSurveysDetails
          ).then(saveResponse => {
            surveyCount++;
            if (surveyCount !== surveyList.length) {
              return this.insertSurveys(
                surveyList,
                surveyCount,
                username,
                sectorId,
                roleId
              );
            }
          });
        }
      });
    }
    return Promise.resolve();
  }

  public fetchDisplayGroupData(
    username,
    sectorId,
    roleId,
    contextGroupId,
    displayGroupId,
    loopingIndex,
    uniqueId,
    parentId,
    surveyId,
    canBacktrackBranches,
    inChild
  ) {
    const partOfQuery = `SELECT user_survey_items.survey_unique_id, user_survey_items.survey_detail_unique_id,
       survey_item_id, item_text, item_id, item_type_code, control_type_code,
        parent_trigger_result_option_id, action_type, is_required, collapse_answer,
         result_text_max_length, limit_date_range, result_text_max_date_day_offset,
          result_text_min_date_day_offset, numbering, help_text, info_text, class_name,
           result_text_data_type, result_options, scaffold_item_text, scaffold_control_type_id,
            scaffold_result_option_type_id, scaffold_default_value, scaffold_is_read_only,
             scaffold_is_visible, result_option_type_id, scaffold_control_type_code,
             user_surveys_details.context_group_id, user_surveys_details.display_group_id,
              user_surveys_details.unique_id, user_surveys_details.parent_id FROM
              user_surveys_details INNER JOIN user_surveys ON user_surveys_details.survey_unique_id =
               user_surveys.survey_unique_id INNER JOIN user_survey_items ON
                user_survey_items.survey_unique_id = user_surveys.survey_unique_id AND
                 user_survey_items.survey_detail_unique_id = user_surveys_details.unique_id AND
                 user_surveys.username = ? AND user_surveys.sector_id = ? AND user_surveys.role_id = ?`;
    if (canBacktrackBranches !== true) {
      if (displayGroupId === undefined) {
        /* when showing user form for first time, 'displayGroupId' is undefined. So fetch first
       available display group for that survey */
        return this.db.executeSql(
          partOfQuery +
            ` WHERE user_surveys.survey_id = ? AND user_surveys_details.unique_id = ? ORDER BY
             user_surveys_details.display_group_id ASC `,
          [username, sectorId, roleId, surveyId, uniqueId]
        );
      } else {
        /* when 'displayGroupId' is present */
        if (!inChild) {
          /* if that display group is not child of any other display group */
          if (!isNaN(parentId) && parentId !== 'null' && parentId !== null) {
            /* for current parent if any further display group is present */
            return this.db
              .executeSql(
                partOfQuery +
                  ` WHERE user_surveys_details.context_group_id = ? AND user_surveys_details.parent_id =?
                   AND user_surveys_details.display_group_id > ? AND user_surveys.survey_id = ?  ORDER BY
                    display_group_id ASC`,
                [
                  username,
                  sectorId,
                  roleId,
                  contextGroupId,
                  parentId,
                  displayGroupId,
                  surveyId
                ]
              )
              .then(
                (displayGroupResponse: any) => {
                  if (displayGroupResponse.rows.length === 0) {
                    /* for current parent no further display group is present */
                    return this.fetchDisplayGroupData(
                      username,
                      sectorId,
                      roleId,
                      contextGroupId,
                      displayGroupId,
                      loopingIndex,
                      uniqueId,
                      parentId,
                      surveyId,
                      true,
                      undefined
                    );
                  } else {
                    /* for current parent further display group is present, so fetch it */
                    return this.db.executeSql(
                      partOfQuery +
                        ` WHERE user_surveys_details.context_group_id = ? AND
                         user_surveys_details.parent_id =?  AND user_surveys_details.display_group_id
                          > ? AND user_surveys.survey_id = ?  ORDER BY display_group_id ASC`,
                      [
                        username,
                        sectorId,
                        roleId,
                        contextGroupId,
                        parentId,
                        displayGroupId,
                        surveyId
                      ]
                    );
                  }
                },
                function(error) {
                  console.log(
                    'Error in getting display group data ' + error.message,
                    'survey-resource',
                    'getDisplayGroupData'
                  ); // TO DO :- Replace this code with logger service.
                }
              );
          } else {
            return this.db
              .executeSql(
                partOfQuery +
                  ` WHERE user_surveys_details.context_group_id = ? AND user_surveys_details.parent_id
                   ISNULL AND user_surveys_details.display_group_id > ? AND user_surveys.survey_id = ?
                     ORDER BY display_group_id ASC`,
                [
                  username,
                  sectorId,
                  roleId,
                  contextGroupId,
                  displayGroupId,
                  surveyId
                ]
              )
              .then(
                (displayGroupResponse: any) => {
                  if (displayGroupResponse.rows.length === 0) {
                    return this.fetchDisplayGroupData(
                      username,
                      sectorId,
                      roleId,
                      contextGroupId,
                      displayGroupId,
                      loopingIndex,
                      uniqueId,
                      parentId,
                      surveyId,
                      true,
                      undefined
                    );
                  } else {
                    return this.db.executeSql(
                      partOfQuery +
                        ` WHERE user_surveys_details.context_group_id = ? AND
                         user_surveys_details.parent_id ISNULL AND user_surveys_details.display_group_id
                          > ? AND user_surveys.survey_id = ?  ORDER BY display_group_id ASC`,
                      [
                        username,
                        sectorId,
                        roleId,
                        contextGroupId,
                        displayGroupId,
                        surveyId
                      ]
                    );
                  }
                },
                function(error) {
                  console.log(
                    'Error in getting display group data ' + error.message,
                    'survey-resource',
                    'getDisplayGroupData'
                  ); // TO DO :- Replace this code with logger service.
                }
              );
          }
        } else {
          /* if that display group is child of any other display group */
          if (!isNaN(parentId) && parentId !== 'null' && parentId !== null) {
            /* for current parent if any further display group is present */
            return this.db
              .executeSql(
                partOfQuery +
                  ` WHERE user_surveys_details.context_group_id = ? AND user_surveys_details.parent_id =
                   ? AND user_surveys_details.display_group_id > ? AND user_surveys.survey_id = ?
                     ORDER BY display_group_id ASC`,
                [
                  username,
                  sectorId,
                  roleId,
                  contextGroupId,
                  parentId,
                  displayGroupId,
                  surveyId
                ]
              )
              .then(
                (displayGroupResponse: any) => {
                  if (displayGroupResponse.rows.length === 0) {
                    /* for current parent no further display group is present.
                     Check if any further context group is present or not. */
                    return this.db
                      .executeSql(
                        partOfQuery +
                          ` WHERE user_surveys_details.parent_id = ? AND
                           user_surveys_details.context_group_id >? AND user_surveys.survey_id = ? AND
                            user_surveys_details.isFirstChild = 'false'  ORDER BY display_group_id ASC`,
                        [
                          username,
                          sectorId,
                          roleId,
                          parentId,
                          contextGroupId,
                          surveyId
                        ]
                      )
                      .then((nextChildResponse: any) => {
                        if (nextChildResponse.rows.length === 0) {
                          /* for current parent no further context group is present */
                          return this.fetchDisplayGroupData(
                            username,
                            sectorId,
                            roleId,
                            contextGroupId,
                            displayGroupId,
                            loopingIndex,
                            uniqueId,
                            parentId,
                            surveyId,
                            true,
                            undefined
                          );
                        } else {
                          /* for current parent context group is present */
                          return this.db.executeSql(
                            partOfQuery +
                              ` WHERE user_surveys_details.parent_id = ? AND
                               user_surveys_details.context_group_id >? AND user_surveys.survey_id = ? AND
                                user_surveys_details.isFirstChild = 'false'  ORDER BY display_group_id ASC`,
                            [
                              username,
                              sectorId,
                              roleId,
                              parentId,
                              contextGroupId,
                              surveyId
                            ]
                          );
                        }
                      });
                  } else {
                    /* for current parent further display group is present, so fetch it. */
                    return this.db.executeSql(
                      partOfQuery +
                        ` WHERE user_surveys_details.context_group_id = ? AND
                         user_surveys_details.parent_id = ? AND user_surveys_details.display_group_id > ?
                          AND user_surveys.survey_id = ?  ORDER BY display_group_id ASC `,
                      [
                        username,
                        sectorId,
                        roleId,
                        contextGroupId,
                        parentId,
                        displayGroupId,
                        surveyId
                      ]
                    );
                  }
                },
                function(error) {
                  console.log(
                    'Error in getting display group data ' + error.message,
                    'survey-resource',
                    'getDisplayGroupData'
                  ); // TO DO :- Replace this code with logger service.
                }
              );
          }
        }
      }
    } else {
      if (!isNaN(parentId) && parentId !== 'null' && parentId !== null) {
        /* if parent id for current display group is present, search to it's parent */
        return this.db
          .executeSql(
            partOfQuery +
              ` WHERE user_surveys_details.unique_id = ? AND user_surveys.survey_id = ?`,
            [username, sectorId, roleId, parentId, surveyId]
          )
          .then((response: any) => {
            if (
              response.rows.item(0).parent_id === null ||
              response.rows.item(0).parent_id === 'null'
            ) {
              /* if current display group has no grandparent, search it's parent's sibling */
              return this.fetchDisplayGroupData(
                username,
                sectorId,
                roleId,
                response.rows.item(0).context_group_id,
                response.rows.item(0).display_group_id,
                loopingIndex,
                response.rows.item(0).unique_id,
                response.rows.item(0).parent_id,
                surveyId,
                false,
                false
              );
            } else {
              /* if current display group has grandparent, search it's sibling  */
              return this.fetchDisplayGroupData(
                username,
                sectorId,
                roleId,
                response.rows.item(0).context_group_id,
                response.rows.item(0).display_group_id,
                loopingIndex,
                response.rows.item(0).unique_id,
                response.rows.item(0).parent_id,
                surveyId,
                false,
                true
              );
            }
          });
      } else {
        /* if parent id for current display group is absent, search if there is another context group.
        if present, display that context group */
        return this.db.executeSql(
          partOfQuery +
            ` WHERE user_surveys_details.context_group_id >? AND user_surveys_details.parent_id ISNULL AND user_surveys.survey_id = ?
               order by user_surveys_details.context_group_id ASC `,
          [username, sectorId, roleId, contextGroupId, surveyId]
        );
      }
    }
  }

  private getSurveyItemOfDisplayGroup(
    result,
    username,
    sectorId,
    roleId,
    surveyId,
    isAdmin
  ) {
    if (result.rows && result.rows.length > 0 && result.rows.item(0)) {
      const surveyUniqueId = result.rows.item(0).survey_unique_id;
      const surveyDetailUniqueId = result.rows.item(0).survey_detail_unique_id;
      const SurveyItems = [];
      for (let i = 0; i < result.rows.length; i++) {
        if (
          isAdmin ||
          surveyDetailUniqueId === result.rows.item(i).survey_detail_unique_id
        ) {
          const SurveyItem = {}; // Object
          SurveyItem['SurveyItemID'] = result.rows.item(i).survey_item_id;
          SurveyItem['Text'] = result.rows.item(i).item_text;
          SurveyItem['ItemID'] = result.rows.item(i).item_id;
          SurveyItem['ItemTypeCode'] = result.rows.item(i).item_type_code;
          SurveyItem['ControlTypeCode'] = result.rows.item(i).control_type_code;
          SurveyItem['ParentTriggerResultOptionID'] = result.rows.item(
            i
          ).parent_trigger_result_option_id;
          SurveyItem['ActionType'] = result.rows.item(i).action_type;
          SurveyItem['IsRequired'] = result.rows.item(i).is_required;
          SurveyItem['CollapseAnswer'] = result.rows.item(i).collapse_answer;
          SurveyItem['ResultTextMaxLength'] = result.rows.item(
            i
          ).result_text_max_length;
          SurveyItem['LimitDateRange'] = result.rows.item(i).limit_date_range;
          SurveyItem['ResultTextMaxDateDayOffset'] = result.rows.item(
            i
          ).result_text_max_date_day_offset;
          SurveyItem['ResultTextMinDateDayOffset'] = result.rows.item(
            i
          ).result_text_min_date_day_offset;
          SurveyItem['Numbering'] = result.rows.item(i).numbering;
          SurveyItem['HelpText'] = result.rows.item(i).help_text;
          SurveyItem['InfoText'] = result.rows.item(i).info_text;
          SurveyItem['ClassName'] = result.rows.item(i).class_name;
          SurveyItem['ResultTextDataType'] = result.rows.item(
            i
          ).result_text_data_type;
          SurveyItem['ResultOptions'] = result.rows.item(i).result_options;
          SurveyItem['ScaffoldItemText'] = result.rows.item(
            i
          ).scaffold_item_text;
          SurveyItem['ScaffoldControlTypeID'] = result.rows.item(
            i
          ).scaffold_control_type_id;
          SurveyItem['ScaffoldResultOptionTypeID'] = result.rows.item(
            i
          ).scaffold_result_option_type_id;
          SurveyItem['ScaffoldDefaultValue'] = result.rows.item(
            i
          ).scaffold_default_value;
          SurveyItem['ScaffoldIsReadOnly'] = result.rows.item(
            i
          ).scaffold_is_read_only;
          SurveyItem['ScaffoldIsVisible'] = result.rows.item(
            i
          ).scaffold_is_visible;
          SurveyItem['ResultOptionTypeID'] = result.rows.item(
            i
          ).result_option_type_id;
          SurveyItem['ScaffoldControlTypeCode'] = result.rows.item(
            i
          ).scaffold_control_type_code;
          SurveyItems.push(SurveyItem);
        }
      }
      let SurveyItemJson = JSON.stringify(SurveyItems);
      // tslint:disable-next-line: quotemark
      SurveyItemJson = SurveyItemJson.replace(/'/g, '""');
      const createTempTableQuery = `CREATE TEMP TABLE _Variables(survey_unique_id integer, survey_item blob);`;
      return this.db.executeSql(createTempTableQuery, []).then(() => {
        const insertSurveyItemQuery =
          `INSERT INTO _Variables (survey_unique_id, survey_item) VALUES (` +
          surveyUniqueId +
          ",'" +
          SurveyItemJson +
          "');";
        return this.db.executeSql(insertSurveyItemQuery, []).then(() => {
          const selectQuery = `SELECT _Variables.survey_item, user_surveys_details.*, user_surveys.* FROM
             user_surveys_details INNER JOIN user_surveys ON (user_surveys_details.survey_unique_id =
               user_surveys.survey_unique_id) INNER JOIN _Variables ON (_Variables.survey_unique_id =
                 user_surveys.survey_unique_id) AND user_surveys.sector_id
                  = ? AND user_surveys.role_id = ?  AND user_surveys_details.unique_Id = ? WHERE user_surveys.survey_id = ? ORDER BY
                   user_surveys_details.display_group_id ASC LIMIT 1`;
          return this.db
            .executeSql(selectQuery, [
              sectorId,
              roleId,
              surveyDetailUniqueId,
              surveyId
            ])
            .then((response: any) => {
              const dropTempTableQuery = 'DROP TABLE _Variables;';
              return this.db.executeSql(dropTempTableQuery, []).then(resp => {
                return response.rows.item(0);
              });
            });
        });
      });
    }
  }

  public updateSurveyItemStatus(username, sectorId, roleId, surveyId, status) {
    let query = '';
    const params = [];
    if (status === 'Completed') {
      query = `UPDATE user_survey_item_result set status = ? WHERE username = ? AND sector_id = ? AND
         role_id = ? AND survey_item_result_id = ? AND status!='Submitted'`;
      params.push(status, username, sectorId, roleId, surveyId);
    }
    return this.db.executeSql(query, params).then(
      function(result) {
        return true;
      },
      function(error) {
        return false;
      }
    );
  }

  public getResults(
    username,
    sectorId,
    roleId,
    storeResult,
    surveyItemValues,
    uniqueResultId
  ) {
    const surveryItemResults = [];
    if (uniqueResultId !== -1) {
      /* “uniqueResultId” = -1 indicate it is first record to be inserted in
       user_questionnaire_result table in sqlite. We have not used zero
       because zero represent identity column first entry. */
      return this.fetchSurveyAnswerData(
        username,
        sectorId,
        roleId,
        uniqueResultId
      ).then((response: any) => {
        if (response.rows.length > 0) {
          const answerData = JSON.parse(
            response.rows.item(0).survey_item_result
          );
          for (let i = 0; i < answerData.length; i++) {
            surveryItemResults.push(answerData[i]);
          }
          surveyItemValues.forEach(surveyItem => {
            if (surveyItem.Result) {
              // TODO: Need handle surveyItem.Result as an array when it will have multiple items
              const result = surveyItem.Result;
              let isExist = false;
              for (let i = 0; i < surveryItemResults.length; i++) {
                if (
                  surveryItemResults[i].SurveyItemID !== undefined &&
                  result.SurveyItemID === surveryItemResults[i].SurveyItemID &&
                  result.LoopingIndex === surveryItemResults[i].LoopingIndex
                ) {
                  isExist = true;
                  surveryItemResults[i] = result;
                  break;
                }
              }
              if (!isExist) {
                surveryItemResults.push(result);
              }
            }
          });
        }
        return Promise.resolve(surveryItemResults);
      });
    } else {
      this.getCurrentSurveyItemResult(
        surveyItemValues,
        surveryItemResults,
        storeResult
      );
      return Promise.resolve(surveryItemResults);
    }
  }

  public getCurrentSurveyItemResult(
    surveyItemValues,
    surveyItemResult,
    storeResult
  ) {
    surveyItemValues.forEach(surveyItem => {
      if (surveyItem.Result) {
        surveyItemResult.push(surveyItem.Result);
      }
    });
    return surveyItemResult;
  }

  /**
    This function is used to get the answers from the local databse for a survey based on survey unqiue id.
  **/
  public fetchSurveyAnswerData(username, sectorId, roleId, uniqueId) {
    return this.db.executeSql(
      `SELECT survey_item_result from user_survey_item_result WHERE
      survey_item_result_id= ? AND username =? AND sector_id =? AND role_id =?`,
      [uniqueId, username, sectorId, roleId]
    );
  }

  /**
    This function is used to get location from local db
  **/
  public getLocation() {
    return this.db.executeSql(`SELECT * FROM location`, []);
  }

  /**
    This function is used to get last suervey of this deveice from local db
  **/
  public getSurvey() {
    return this.db.executeSql(
      `SELECT * FROM user_surveys where survey_category != 'ACCO'  order by survey_unique_id  desc limit 1`,
      []
    );
  }

  public getSurveyUniqueId(surveyId) {
    return this.db
      .executeSql(
        `SELECT survey_unique_id FROM user_surveys WHERE survey_id=?`,
        [surveyId]
      )
      .then((response: any) => {
        const surveyUniqueId = response.rows.item(0).survey_unique_id;
        return surveyUniqueId;
      });
  }

  public getSurveyDetails(surveyUniqueId) {
    return this.db
      .executeSql(
        `SELECT * FROM user_surveys_details WHERE survey_unique_id = ?`,
        [surveyUniqueId]
      )
      .then((response: any) => {
        return response;
      });
  }

  /**
         Get survey from the SQLite database by survey id.
        **/
  public getSurveyById(surveyId) {
    const query = `SELECT DISTINCT * FROM user_surveys WHERE survey_id=?`;
    return this.db.executeSql(query, [surveyId]);
  }

  public getSurveyByIdAndUsername(surveyId, userName) {
    const query = `SELECT DISTINCT * FROM user_surveys WHERE survey_id=? AND username=?`;
    return this.db.executeSql(query, [surveyId, userName]);
  }

  /**
   *
   * @param surveyId
   * @param surveyItemResultId
   */
  public insertActiveSurveyDetails(surveyId, surveyItemResultId) {
    const activeSurveyDetailsData = [];

    // Get unique survey id for given survey id
    return this.db
      .executeSql(
        `SELECT survey_unique_id FROM user_surveys where survey_id = ?`,
        [surveyId]
      )
      .then((response: any) => {
        const surveyUniqueId = response.rows.item(0).survey_unique_id;

        // Get all user survey details data required for the active survey table.
        return this.db
          .executeSql(
            `SELECT * FROM user_surveys_details where survey_unique_id = ?`,
            [surveyUniqueId]
          )
          .then((response: any) => {
            const queries: string[] = [];
            const levelParentId = null;

            for (let key = 0; key < response.rows.length; key++) {
              const insertQuery = `INSERT INTO active_survey_details (survey_item_result_id, survey_unique_id, unique_id, display_group_id,
                level_parent_id) VALUES ('
                ${surveyItemResultId}',
                ' ${response.rows.item(key).survey_unique_id}',
                ' ${response.rows.item(key).unique_id}',
                '${response.rows.item(key).display_group_id}',
                '${levelParentId}')`;
              queries.push(insertQuery);
            }
            return this.db.executeBatch(queries).then(() => {
              return true;
            });
          });
      });
  }

  public deleteActiveSurveyDetails() {
    return this.db
      .executeSql(`DELETE FROM active_survey_details`, [])
      .then((response: any) => {
        return this.db
          .executeSql(
            `DELETE FROM user_survey_item_result where status = "InProgress"`,
            []
          )
          .then((response: any) => {
            return true;
          });
      });
  }

  public addActiveSurvey(surveyId) {
    const roleId = Number(localStorage.getItem('roleId'));
    const sectorId = localStorage.getItem('sectorId');
    const username = localStorage.getItem('username');

    const surveyItemResul = [];

    return this.saveSurveyItemData(
      username,
      sectorId,
      roleId,
      0,
      surveyId,
      surveyItemResul,
      false
    ).then(surveyItemResultId => {
      this.questionResultId = surveyItemResultId;
      return this.insertActiveSurveyDetails(surveyId, surveyItemResultId).then(
        () => {
          return true;
        }
      );
    });
  }

  public fetchPreviousGroupData(
    username,
    sectorId,
    roleId,
    uniqueId,
    surveyId
  ) {
    const query = `SELECT user_survey_items.survey_unique_id, user_survey_items.survey_detail_unique_id,
       survey_item_id, item_text, item_id, item_type_code, control_type_code,
        parent_trigger_result_option_id, action_type, is_required, collapse_answer,
         result_text_max_length, limit_date_range, result_text_max_date_day_offset,
          result_text_min_date_day_offset, numbering, help_text, info_text, class_name,
           result_text_data_type, result_options, scaffold_item_text, scaffold_control_type_id,
            scaffold_result_option_type_id, scaffold_default_value, scaffold_is_read_only,
             scaffold_is_visible, result_option_type_id, scaffold_control_type_code FROM
              user_surveys_details INNER JOIN user_surveys ON user_surveys_details.survey_unique_id =
               user_surveys.survey_unique_id INNER JOIN user_survey_items ON
                user_survey_items.survey_unique_id = user_surveys.survey_unique_id AND
                 user_survey_items.survey_detail_unique_id = user_surveys_details.unique_id AND
                 user_surveys.username = ? AND user_surveys.sector_id = ? AND user_surveys.role_id = ?
                  WHERE user_surveys.survey_id = ? AND user_surveys_details.unique_id = ? ORDER BY
             user_surveys_details.display_group_id ASC`;

    return this.db
      .executeSql(query, [username, sectorId, roleId, surveyId, uniqueId])
      .then(result => {
        return this.getSurveyItemOfDisplayGroup(
          result,
          username,
          sectorId,
          roleId,
          surveyId,
          false
        );
      });
  }

  public fetchParentIdForCurrentSurveyItem(uniqueId) {
    const query = `SELECT * from active_survey_details where unique_id = ?`;

    return this.db.executeSql(query, [uniqueId]).then((result: any) => {
      return result.rows.item(0).level_parent_id;
    });
  }

  updateTableActiveSurveyDetails(uniqueId, levelParentId) {
    const updateQuery = `UPDATE active_survey_details set level_parent_id = ? WHERE unique_id = ?`;
    return this.db
      .executeSql(updateQuery, [levelParentId, uniqueId])
      .then(result => {
        return result;
      });
  }

  public launchACSurvey(originalSurveyId, deviceId) {
    const request: APIRequest = new APIRequest(
      `api/survey/copyAcSurvey?originalSurveyId=${originalSurveyId}&deviceId=${deviceId}`,
      APIMethod.POST
    );
    return this.dataService.execute(request);
  }

  getUserSurveyItemResult(uniqueId) {
    const selectQuery = `SELECT * from user_survey_item_result as USIR inner join active_survey_details as ASD on
    USIR.survey_item_result_id = ASD.survey_item_result_id where ASD.unique_id = ?`;

    return this.db.executeSql(selectQuery, [uniqueId]).then(result => {
      return result;
    });
  }

  public getLatestVersionSurveyId(
    surveyId: number,
    locationId: string,
    canHideSpinner: boolean
  ) {
    const params = `api/survey/${surveyId}/locations/${locationId}`;
    const request: APIRequest = new APIRequest(
      params,
      APIMethod.GET,
      canHideSpinner
    );
    return this.dataService.executeAPI(request);
  }

  public getSectorConfig(surveyId: number) {
    const selectedSectorId = Number(localStorage.getItem('selectedSectorId'));
    const locationIdentifier = localStorage.getItem('selectedLocationId');
    const locationDataSource = 'SAP';
    // tslint:disable-next-line: max-line-length
    const params = `api/sector/${selectedSectorId}/styles?locationIdentifier=${locationIdentifier}&locationDataSource=${locationDataSource}&surveyId=${surveyId}`;
    const request: APIRequest = new APIRequest(params, APIMethod.GET);
    return this.dataService.executeAPI(request);
  }
}
