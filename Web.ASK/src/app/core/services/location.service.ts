import { Injectable } from '@angular/core';
import { APIMethod, DataService, DbService } from './index';
import { APIRequest, Location } from '../entities';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  private readonly locationTableName = 'location';
  constructor(private dataService: DataService, private db: DbService) {}

  public searchCostCenter(number, city, state, name, zip_code) {
    const params: string =
      'api/location/searchLocations?sectorAdamId=&costCenterNumber=' +
      number +
      '&city=' +
      city +
      '&state=' +
      state +
      '&costCenterDescription=' +
      name +
      '&zipCode=' +
      zip_code;
    const request: APIRequest = new APIRequest(params, APIMethod.GET);
    return this.dataService.execute(request);
  }

  public saveLocation(data) {
    const location: Location = new Location();
    location.LocationId = data.LocationId;
    location.SectorAdamId = data.SectorAdamId;
    location.LocationDescription = data.LocationDescription;
    location.LocationDataSource = data.DataSourceCode;
    return this.deleteAll().then(() => {
      return this.createLocation(location);
    });
  }

  private deleteAll() {
    return this.db.executeSql('DELETE FROM location', []);
  }

  private createLocation(location: Location) {
    const query =
      'INSERT INTO location (LocationId , DataSourceCode, SectorAdamId , LocationDescription ) VALUES (?,?,?,?)';

    return this.db.executeSql(query, [
      location.LocationId,
      location.LocationDataSource,
      location.SectorAdamId,
      location.LocationDescription
    ]);
  }

  public getAskAdminRole(sectorAdamId) {
    const params = 'api/user/GetAskAdminRole/' + sectorAdamId;
    const request: APIRequest = new APIRequest(params, APIMethod.GET);
    return this.dataService.execute(request);
  }
}
