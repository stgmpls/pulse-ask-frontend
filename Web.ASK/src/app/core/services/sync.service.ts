import { Injectable } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';

import { APIRequest } from '../entities';
import { APIMethod } from './data.service';
import { DbService } from './db.service';
import { DataService } from './data.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class SyncService {
  constructor(
    private dataService: DataService,
    private db: DbService,
    private deviceService: DeviceDetectorService
  ) {}

  public submitFormDetails(
    username: string,
    sectorId: string,
    roleId: number,
    isNetworkConnected = true,
    showProgress = false
  ) {
    return this.getAllOutboxForms(username, sectorId, roleId).then(
      (response: any) => {
        if (response.length > 0 && isNetworkConnected) {
          this.submitOutboxForms(
            response,
            username,
            sectorId,
            roleId,
            showProgress
          ).then((result: any) => {
            const currentTime = moment
              .utc()
              .local()
              .format('MM-DD-YYYY HH:mm:ss');
            localStorage.setItem('lastSyncTime', currentTime);
          });
        }
      }
    );
  }

  /**
         Insert or update survey into the database.
        **/
  public getAllOutboxForms(username, sectorId, roleId) {
    const thisService = this;

    const status = 'Completed';
    const surveyPromises = [];
    const surveyData = [];
    return new Promise((resolve, reject) => {
      thisService.db
        .executeSql(
          `SELECT * from user_survey_item_result INNER JOIN user_surveys ON user_survey_item_result.survey_id = user_surveys.survey_id
          and user_survey_item_result.username = user_surveys.username and user_survey_item_result.sector_id = user_surveys.sector_id
          and user_survey_item_result.role_id = user_surveys.role_id where user_survey_item_result.username=?
          and user_survey_item_result.sector_id=? and user_survey_item_result.role_id=? and user_survey_item_result.status=?;`,
          [username, sectorId, roleId, status]
        )
        .then(
          (response: any) => {
            for (let i = 0; i < response.rows.length; i++) {
              surveyPromises.push(
                new Promise(resolve => {
                  this.getFinalSurveyAnswerData(
                    username,
                    sectorId,
                    roleId,
                    response.rows.item(i).survey_item_result_id,
                    response.rows.item(i).survey_id,
                    response.rows.item(i)
                  ).then((surveyItemResultData: any) => {
                    surveyData.push({
                      survey: JSON.parse(
                        surveyItemResultData.response.survey_content
                      ),
                      surveyItemResult: surveyItemResultData.surveyItemResponse,
                      uniqueId:
                        surveyItemResultData.response.survey_item_result_id,
                      startDate: surveyItemResultData.response.start_date,
                      endDate: surveyItemResultData.response.modified_date,
                      locationValue:
                        surveyItemResultData.response.survey_item_location,
                      surveyInstanceId:
                        surveyItemResultData.response.survey_instance_id,
                      locationId: surveyItemResultData.response.location_id,
                      type: surveyItemResultData.response.type,
                      locationDataSource:
                        surveyItemResultData.response.location_data_source,
                      location: surveyItemResultData.response.location
                    });

                    resolve();
                  });
                })
              );
            }
            Promise.all(surveyPromises).then((data: any) => {
              resolve(surveyData);
            });
          },
          function(error) {}
        );
    });
  }

  /**
    This function gets Active surveyItem and their looping index form user_questionarray table
  **/

  public getFinalSurveyAnswerData(
    username,
    sectorId,
    roleId,
    surveyUniqueId,
    surveyId,
    response
  ) {
    // this code gives only Active pages SurveyItems from questionaary table
    return this.fetchSurveyAnswerData(
      username,
      sectorId,
      roleId,
      surveyUniqueId
    ).then((answerResp: any) => {
      const finalSurveyAnswerData = JSON.parse(
        answerResp.rows.item(0).survey_item_result
      );

      return {
        surveyItemResponse: finalSurveyAnswerData,
        response: response
      };
    });
  }

  /**
        This function is used to get the answers from the local databse for a survey based on survey unqiue id.
        **/
  public fetchSurveyAnswerData(username, sectorId, roleId, uniqueId) {
    return this.db.executeSql(
      `SELECT survey_item_result from user_survey_item_result where
      survey_item_result_id= ? and username =? and sector_id =? and role_id =?`,
      [uniqueId, username, sectorId, roleId]
    );
  }

  /**
       This function is used to submit outbox form on server.
       **/
  public submitOutboxForms(
    outboxContent,
    username,
    sectorId,
    roleId,
    showProgress
  ) {
    const count = 0;
    let surveyItemResult = [];

    let surveyIds = this.filterSurveyIds(outboxContent, true);
    const surveyIdswithoutInstanceId = this.filterSurveyIds(
      outboxContent,
      false
    );

    if (surveyIdswithoutInstanceId.length > 0) {
      return this.getAllSurveyInstanceId(
        surveyIdswithoutInstanceId,
        showProgress
      ).then(response => {
        const responseData = response.Data;
        const updateProm = [];
        for (let i = 0; i < responseData.length; i++) {
          updateProm.push(
            this.updateSurveyItemSurveyInstanceId(
              username,
              sectorId,
              roleId,
              responseData[i].AppInstanceID,
              responseData[i].SurveyInstanceID
            )
          );
        }
        return Promise.all(updateProm).then(
          () => {
            surveyIds = surveyIds.concat(responseData);
            surveyItemResult = this.createSurveyResultItems(
              outboxContent,
              surveyIds
            );

            return this.submitAllForms(surveyItemResult, showProgress).then(
              response => {
                if (response && response.Data.length > 0) {
                  surveyIds.forEach(item => {
                    item = this.checkSurveyInstanceID(response.Data, item);
                  });
                  for (let i = 0; i < surveyIds.length; i++) {
                    if (surveyIds[i].Success) {
                      this.updateSurveyItemStatus(
                        username,
                        sectorId,
                        roleId,
                        surveyIds[i].AppInstanceID,
                        'true',
                        'Submitted',
                        surveyIds[i].SurveyInstanceID
                      ).then((updateresponse: any) => {
                        return true;
                      });
                    }
                  }
                }
                return response.Data;
              }
            );
          },
          function(error) {}
        );
      });
    } else {
      surveyItemResult = this.createSurveyResultItems(outboxContent, surveyIds);
      return this.submitAllForms(surveyItemResult, showProgress).then(
        (response: any) => {
          const responseData = response.Data;
          if (responseData.length > 0 && responseData instanceof Array) {
            surveyIds.forEach(item => {
              item = this.checkSurveyInstanceID(response.Data, item);
            });
            for (let i = 0; i < surveyIds.length; i++) {
              if (surveyIds[i].Success) {
                this.updateSurveyItemStatus(
                  username,
                  sectorId,
                  roleId,
                  surveyIds[i].AppInstanceID,
                  'true',
                  'Submitted',
                  surveyIds[i].SurveyInstanceID
                ).then((updateresponse: any) => {
                  return true;
                });
              }
            }
          }
          return true;
        }
      );
    }
  }

  public filterSurveyIds(outboxContent, flagWithSurveyInstanceId) {
    const surveyIds = [];
    const platform = this.deviceService.getDeviceInfo().os;
    const versionNumber = null;

    outboxContent.forEach(item => {
      if (item.location instanceof Array) {
        item.location = item.location[0];
      }

      if (flagWithSurveyInstanceId && item.surveyInstanceId > 0) {
        surveyIds.push({
          SurveyInstanceID: item.surveyInstanceId,
          AppInstanceID: item.uniqueId,
          SurveyID: item.survey.SurveyId,
          Location: item.location ? JSON.parse(item.location) : '',
          Platform: platform,
          Version: versionNumber
        });
      } else if (!flagWithSurveyInstanceId && item.surveyInstanceId <= 0) {
        surveyIds.push({
          SurveyInstanceID: null,
          AppInstanceID: item.uniqueId,
          SurveyID: item.survey.SurveyId,
          Location: item.location ? JSON.parse(item.location) : '',
          Platform: platform,
          Version: versionNumber
        });
      }
    });

    return surveyIds;
  }

  public getAllSurveyInstanceId(surveyIds, showProgress) {
    const request: APIRequest = new APIRequest(
      'api/Survey/GetSurveyInstance',
      APIMethod.POST,
      showProgress
    );
    request.addAll(surveyIds);
    return this.dataService.executeAPI(request).toPromise();
  }

  public submitAllForms(surveyResult, showProgress) {
    const totalCount = surveyResult.length;
    surveyResult[0].TotalCount = totalCount;
    const request: APIRequest = new APIRequest(
      'api/Survey/InsertSurveyItemResults',
      APIMethod.POST,
      showProgress
    );
    request.addAll(surveyResult);
    return this.dataService.executeAPI(request).toPromise();
  }

  /**
        This function is used to update the status of an survey in local database.
        **/
  public updateSurveyItemSurveyInstanceId(
    username,
    sectorId,
    roleId,
    surveyId,
    surveyInstanceId
  ) {
    let query = '';
    const params = [];
    const thisService = this;
    query = `UPDATE user_survey_item_result set survey_instance_id = ? where
    username = ? and sector_id = ? and role_id = ? and survey_item_result_id = ?`;
    params.push(surveyInstanceId, username, sectorId, roleId, surveyId);
    return thisService.db.executeSql(query, params).then(
      (result: any) => {
        return true;
      },
      function(error) {
        return false;
      }
    );
  }

  /**
        This function is used to update the status of an survey in local database.
        **/
  public updateSurveyItemStatus(
    username,
    sectorId,
    roleId,
    surveyId,
    flag,
    status,
    assignmentId
  ) {
    let query = '';
    let params = [];
    const thisService = this;
    if (status === 'Completed') {
      query = `UPDATE user_survey_item_result set status = ?
      where username = ? and sector_id = ? and role_id = ? and survey_item_result_id = ? and status!='Submitted' and assignment_id = ?`;
      params = [status, username, sectorId, roleId, surveyId, assignmentId];
    } else {
      query = `UPDATE user_survey_item_result set status = ?,
      survey_instance_id=? where username = ? and sector_id = ? and role_id = ? and survey_item_result_id = ? and status='Completed'`;
      params = [status, assignmentId, username, sectorId, roleId, surveyId];
    }
    return thisService.db.executeSql(query, params).then(
      (result: any) => {
        return true;
      },
      function(error) {
        return false;
      }
    );
  }

  public createSurveyResultItems(outboxContent, surveyIds) {
    const surveyItemResult = [];
    outboxContent.forEach(item => {
      item.surveyItemResult.forEach(resultObj => {
        let matchinInstance = surveyIds.filter(function(i) {
          return i.AppInstanceID === item.uniqueId;
        });
        if (matchinInstance && matchinInstance.length === 1) {
          matchinInstance = matchinInstance[0];
          resultObj.SurveyInstanceId = matchinInstance.SurveyInstanceID;

          if (
            resultObj.ResultOptionId &&
            Array.isArray(resultObj.ResultOptionId)
          ) {
            // Multiple result option Ids
            for (let i = 0; i < resultObj.ResultOptionId.length; i++) {
              surveyItemResult.push({
                IsSelected: resultObj.IsSelected,
                LoopingIndex: resultObj.LoopingIndex,
                ResultOptionId: resultObj.ResultOptionId[i],
                ResultText: resultObj.ResultText,
                SurveyInstanceId: resultObj.SurveyInstanceId,
                SurveyItemID: resultObj.SurveyItemID
              });
            }
          } else if (Array.isArray(resultObj.ResultText)) {
            // Multiple result option texts
            for (let i = 0; i < resultObj.ResultText.length; i++) {
              surveyItemResult.push({
                IsSelected: resultObj.IsSelected,
                LoopingIndex: resultObj.LoopingIndex,
                ResultOptionId: resultObj.ResultOptionId,
                ResultText: resultObj.ResultText[i],
                SurveyInstanceId: resultObj.SurveyInstanceId,
                SurveyItemID: resultObj.SurveyItemID
              });
            }
          } else {
            if (resultObj.ResultOptionId || resultObj.ResultText !== '') {
              surveyItemResult.push(resultObj);
            }
          }
        }
      });
    });

    return surveyItemResult;
  }

  public getSyncLeftCount() {
    return this.db.executeSql(
      `SELECT count(*) as syncLeftCount from user_survey_item_result where
      survey_instance_id=0`,
      []
    );
  }

  public checkSurveyInstanceID(data, item) {
    const surveyItem = data.filter(function(result) {
      if (item.SurveyInstanceID === result.SurveyInstanceId) {
        return true;
      }
    });
    item.Success = surveyItem[0].Success;
    item.Message = surveyItem[0].Message;
    return item;
  }
}
