import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignatureService {
  public signatureImage: BehaviorSubject<any> = new BehaviorSubject('');
  modal: any;
  constructor() {}

  public setSignatureImage(signatureImage) {
    this.signatureImage.next(signatureImage);
  }
}
