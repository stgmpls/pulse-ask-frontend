import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { Platform } from '@ionic/angular';
import { SyncService } from './sync.service';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {
  isAvailiable = false;
  offlineSyncCompleted = false;
  constructor(
    private network: Network,
    private platform: Platform,
    private syncService: SyncService
  ) {
    // watch network for a disconnection
    network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-('); // TO DO :- Replace this code with logger service.
      this.isAvailiable = false;
    });
    if (this.platform.is('cordova') && !this.platform.is('desktop')) {
      // On original device
      if (this.network.type === 'none' || this.network.type === 'unknown') {
        this.isAvailiable = false;
      } else {
        this.isAvailiable = true;
        console.log('Network', this.isAvailiable); // TO DO :- Replace this code with logger service.
      }
    } else {
      // On browser
      this.isAvailiable = navigator.onLine;
    }

    // watch network for a connection
    network.onConnect().subscribe(() => {
      console.log('network connected!'); // TO DO :- Replace this code with logger service.
      this.isAvailiable = true;
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        if (this.network.type === 'wifi') {
          console.log('we got a wifi connection, woohoo!'); // TO DO :- Replace this code with logger service.
        }
        const roleId = Number(localStorage.getItem('roleId'));
        const sectorId = localStorage.getItem('sectorId');
        const username = localStorage.getItem('username');

        this.syncService
          .submitFormDetails(username, sectorId, roleId)
          .then(() => {});
      }, 3000);
    });
  }

  get isConnected() {
    return (
      this.network &&
      this.network.type !== 'none' &&
      this.network.type !== 'unknown'
    );
  }
}
