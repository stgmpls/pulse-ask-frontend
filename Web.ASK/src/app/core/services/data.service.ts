// All Imports
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { APIRequest } from '../entities';
import { environment } from '../../../environments/environment';
import { LoadingController } from '@ionic/angular';
import { of, from } from 'rxjs';
import { HelperService } from './helper.service';
import { AuthenticationService } from './authentication.service';
import { LoadingService } from './loading.service';

export enum APIMethod {
  GET,
  POST,
  PUT,
  DELETE,
  IMAGE_GET,
  JSON_GET
}
@Injectable({
  providedIn: 'root'
})
export class DataService {
  // Variable declarations
  result: string;
  baseURl: string = environment.BASE_API;
  headers: HttpHeaders;
  pendingRequests: APIRequest[] = [];
  public loading: any;

  // Error: Uncaught (in promise): no views in the stack to be removed
  // https://github.com/ionic-team/ionic/issues/11688
  /**
   * Constructor for ApiService.
   * @param {Http} public apiExecuter [HTTP client to execute methods like GET, POST, PUT, DELETE]
   */
  constructor(
    public apiExecuter: HttpClient,
    private loadingCtrl: LoadingController,
    private helperService: HelperService,
    private authService: AuthenticationService,
    private loadingService: LoadingService
  ) {
    this.headers = new HttpHeaders();
  }

  /**
   * Executes request.
   * @param {RequestDataModel} requestDataModel [Model to get metadata of request.]
   */
  execute(apiRequest: APIRequest) {
    if (!this.helperService.validateToken()) {
      this.pendingRequests.push(apiRequest);
      return this.refreshToken();
    } else {
      return this.executeAPI(apiRequest);
    }
  }

  refreshToken() {
    return Observable.create((observer: any) => {
      this.getRefreshToken().subscribe(
        (data: any) => {
          if (data) {
            this.helperService.setAccessToken(data.access_token);
            this.helperService.setRefreshToken(data.refresh_token);
            this.pendingRequests.forEach(apiRequest => {
              this.executeAPI(apiRequest).subscribe(res => {
                observer.next(res);
                observer.complete();
              });
            });
            this.pendingRequests = [];
          }
        },
        err => {
          this.pendingRequests = [];
          this.helperService.clearAccessToken();
        }
      );
    });
  }

  getRefreshToken() {
    return this.authService.getTokenByRefreshToken(
      this.helperService.getRefreshToken()
    );
  }

  executeAPI(apiRequest: APIRequest, baseURl: string = this.baseURl) {
    if (!apiRequest.getCanHideSpinner()) {
      this.loadingService.showLoader();
    }
    this.populateHeaders(apiRequest, apiRequest.getContentType());
    switch (apiRequest.method) {
      case APIMethod.GET:
        return this.apiExecuter
          .get(baseURl + apiRequest.endpoint, { headers: this.headers })
          .pipe(
            map(res => this.handleResponse(apiRequest, res)),
            catchError(data => {
              this.hideLoading(apiRequest);
              return of(data);
            })
          );

      case APIMethod.POST:
        return this.apiExecuter
          .post(baseURl + apiRequest.endpoint, apiRequest.getRawBody(), {
            headers: this.headers
          })
          .pipe(
            map(res => this.handleResponse(apiRequest, res)),
            catchError(data => {
              this.hideLoading(apiRequest);
              if (data.error) {
                this.showToaster(data.error.status, data.error.message);
              }
              return Observable.throw(data);
            })
          );

      case APIMethod.PUT:
        return this.apiExecuter
          .put(this.baseURl + apiRequest.endpoint, apiRequest.getRawBody(), {
            headers: this.headers
          })
          .pipe(
            map(res => this.handleResponse(apiRequest, res)),
            catchError(data => {
              this.hideLoading(apiRequest);
              return Observable.throw(data);
            })
          );

      case APIMethod.DELETE:
        return this.apiExecuter
          .delete(this.baseURl + apiRequest.endpoint, { headers: this.headers })
          .pipe(
            map(res => this.handleResponse(apiRequest, res)),
            catchError(data => {
              this.hideLoading(apiRequest);
              return Observable.throw(data);
            })
          );

      case APIMethod.JSON_GET:
        return this.apiExecuter
          .get(apiRequest.endpoint, { headers: this.headers })
          .pipe(
            map(res => this.handleResponse(apiRequest, res)),
            catchError(data => {
              this.hideLoading(apiRequest);
              return Observable.throw(data);
            })
          );

      default:
        return this.apiExecuter
          .get(this.baseURl + apiRequest.endpoint, { headers: this.headers })
          .pipe(
            map(res => this.handleResponse(apiRequest, res)),
            catchError(data => {
              this.hideLoading(apiRequest);
              return Observable.throw(data);
            })
          );
    }
  }

  populateHeaders(apiRequest, contentType: string) {
    if (contentType) {
      this.headers = this.headers.set('Content-Type', contentType);
    }
  }

  hideLoading(apiRequest: any) {
    if (!apiRequest.getCanHideSpinner()) {
      this.loadingService.dismissLoader();
    }
  }

  handleResponse(apiRequest: any, res: any) {
    this.hideLoading(apiRequest);
    const parseResponse = res;
    return parseResponse;
  }

  private showToaster(status: any, message: string) {}
}
