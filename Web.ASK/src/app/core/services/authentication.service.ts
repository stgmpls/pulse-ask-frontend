import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { BehaviorSubject, Subject } from 'rxjs';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { DbService } from './db.service';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';

const TOKEN_KEY = 'token';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public result: string;
  authenticationState = new BehaviorSubject(false);
  tokenRefreshedSource = new Subject();
  constructor(
    private httpClient: HttpClient,
    private plt: Platform,
    private storage: Storage,
    private db: DbService,
    private handler: HttpBackend
  ) {
    this.plt.ready().then(() => {
      this.checkToken();
    });
  }

  checkToken() {
    this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.authenticationState.next(true);
      }
    });
  }

  /** login */
  login(username: string, password: string, deviceDetails?: any) {
    const params =
      'grant_type=password&username=' +
      username +
      '&password=' +
      password +
      '&deviceDetails=' +
      (deviceDetails || '{}');
    return this.httpClient.post(`${environment.BASE_API}token`, params);
  }

  /** logout */
  logout() {
    this.clearInProgressSurveyItemResult();
    const query = 'DELETE FROM user';
    return this.db.executeSql(query, []);
  }

  getTokenByRefreshToken(refreshToken: string) {
    const params = 'grant_type=refresh_token&refresh_token=' + refreshToken;
    return Observable.create((observer: any) => {
      return this.httpClient
        .post(`${environment.BASE_API}token`, params)
        .subscribe((data: any) => {
          observer.next(data);
          observer.complete();
        });
    });
  }
  getCurrentUserDetails() {
    let resp;
    const query = 'select * from user';
    return this.db.executeSql(query, []).then((results: any) => {
      if (results.rows && results.rows.length > 0 && results.rows.item(0)) {
        resp = results.rows.item(0);
        const userDetail = JSON.parse(resp.user_data);
        resp.user_data = userDetail;
      }
      return resp;
    });
  }

  isAuthenticated() {
    return this.authenticationState.value;
  }

  clearInProgressSurveyItemResult() {
    const query = `DELETE  FROM user_survey_item_result WHERE status='InProgress'`;
    return this.db.executeSql(query, []);
  }
}
