import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpErrorResponse,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { HelperService } from './helper.service';
import 'rxjs/add/operator/do';
import { ToastController } from '@ionic/angular';
import { AuthenticationService } from './authentication.service';
import { observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

@Injectable()
export class APIInterceptor implements HttpInterceptor {
  // @BlockUI() public blockUI: NgBlockUI;

  constructor(
    private helperService: HelperService,
    private toastCtrl: ToastController,
    private authService: AuthenticationService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // Get the auth header from the service.
    let authHeader;
    if (!req.url.includes('token')) {
      authHeader = this.GetAuthHeaders();
    }
    // Clone the request to add the new header.
    if (
      this.helperService.isEmpty(authHeader) ||
      !req.url.includes('compass') ||
      req.url.includes('token')
    ) {
      if (req.body && !req.body.includes('refresh_token')) {
        return next.handle(req).do(event => {});
      } else {
        return next.handle(req);
      }
    } else {
      const headers = req.headers.set('Authorization', authHeader);
      const authReq = req.clone({ headers: headers });
      // Pass on the cloned request instead of the original request.
      return next.handle(authReq);
    }
  }

  private GetAuthHeaders() {
    let authHeader;
    const token: string = this.helperService.getAccessToken();
    if (!this.helperService.isEmpty(token)) {
      authHeader = `bearer ${token}`;
      return authHeader;
    }
  }
}
