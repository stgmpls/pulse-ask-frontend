import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  isLoading = false;

  constructor(public loadingController: LoadingController) {}

  async showLoader() {
    this.isLoading = true;
    return await this.loadingController
      .create({
        message: 'Please wait...',
        duration: 30000
      })
      .then(resp => {
        resp.present().then(() => {
          console.log('loading Presented');
          if (!this.isLoading) {
            resp.dismiss().then(() => console.log('Abort loading presenting'));
          }
        });
      });
  }

  async dismissLoader() {
    this.isLoading = false;
    return await this.loadingController
      .dismiss()
      .then(() => console.log('loading dismissed'));
  }
}
