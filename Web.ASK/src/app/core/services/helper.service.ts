import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { AlertController } from '@ionic/angular';
import { DbService } from './db.service';
import { NavController } from '@ionic/angular';
import { Loggedinuser } from '../entities';
import { Router } from '@angular/router';

@Injectable()
export class HelperService {
  tokenData: any;
  appTokenKey = 'appToken';
  appRefreshTokenKey = 'refreshToken';
  alerts: any[] = [];
  constructor(
    private alertCtrl: AlertController,
    private authService: AuthenticationService,
    private db: DbService,
    private router: Router
  ) {}

  isEmpty(data: any): boolean {
    if (data instanceof Array) {
      return data === null || data === undefined || data.length === 0;
    } else {
      return data === null || data === undefined || data === '';
    }
  }
  validateToken(): boolean {
    const refreshTokenExpiryTime = Number(
      localStorage.getItem('refreshTokenExpiryTime')
    );
    const tokenExpiryTime = Number(localStorage.getItem('tokenExpiryTime'));
    const currentTime = new Date().getTime();
    if (currentTime < refreshTokenExpiryTime) {
      if (currentTime >= tokenExpiryTime) {
        return false;
      } else {
        return true;
      }
    } else {
      this.router.navigate(['/login']);
      this.showAlert('Invalid token');
    }
  }

  setAccessToken(value: any) {
    return localStorage.setItem(this.appTokenKey, value);
  }

  getAccessToken(): string {
    return localStorage.getItem(this.appTokenKey);
  }

  setRefreshToken(value: any) {
    return localStorage.setItem(this.appRefreshTokenKey, value);
  }

  getRefreshToken(): string {
    return localStorage.getItem(this.appRefreshTokenKey);
  }

  clearAccessToken() {
    const lastSyncTime = localStorage.getItem('lastSyncTime');
    localStorage.clear();
    localStorage.setItem('lastSyncTime', lastSyncTime);
    return true;
  }

  dismissAlert() {
    if (this.alerts.length) {
      this.alerts.forEach(element => {
        element.dismiss();
      });
    }
    this.alerts = [];
  }

  convertObjectCollectionToArray(collection) {
    const newArray = [];
    Object.keys(collection).map(function(key) {
      newArray.push({ [key]: collection[key] });
      return newArray;
    });
    return newArray;
  }

  async showAlert(message, showOKBtn = true) {
    this.dismissAlert();
    const alert = await this.alertCtrl.create({
      message: message,
      buttons: ['OK']
    });
    this.alerts.push(alert);
    await alert.present();
  }
}
