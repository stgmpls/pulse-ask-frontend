import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { SmileyListComponent } from './smiley-list.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [IonicModule, CommonModule],
  declarations: [SmileyListComponent],
  exports: [SmileyListComponent]
})
export class SmileyListModule {}
