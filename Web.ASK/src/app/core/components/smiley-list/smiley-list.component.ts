import {
  Component,
  Input,
  OnInit,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'smiley-list',
  templateUrl: './smiley-list.component.html',
  styleUrls: ['./smiley-list.component.scss']
})
export class SmileyListComponent implements OnChanges {
  @Input() smileyList;
  @Input() isParentItemActive = false;
  @Input() activeSmileyText = '';
  @Input() surveyItems;
  @Input() activeItemID;
  @Input() requiredSurveyItemIdArray = [];
  @Input() currentActiveIndex;
  mappedSurveyItemIdObj;
  indexMapObj;
  smileyData = [];

  @Output() selectSmiley: EventEmitter<any> = new EventEmitter();

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes &&
      changes.smileyList &&
      this.smileyList.ResultOptions.length > 0
    ) {
      this.mappedSurveyItemIdObj = {};
      this.indexMapObj = {};
      let surveyItemIndex;
      const resultOptions = JSON.parse(
        this.surveyItems.find((item, index) => {
          if (item.SurveyItemID === this.activeItemID) {
            surveyItemIndex = index;
            return true;
          }
          return false;
        }).ResultOptions
      );

      resultOptions.map(resultOption => {
        this.smileyList.ResultOptions.map(option => {
          if (
            option.Text === resultOption.Text &&
            resultOption.SurveyDetailsId > 0
          ) {
            for (
              let i = surveyItemIndex + 1;
              i < this.surveyItems.length;
              i++
            ) {
              if (
                JSON.parse(this.surveyItems[i].ParentTriggerResultOptionID) ===
                null
              ) {
                break;
              }
              if (
                JSON.parse(this.surveyItems[i].ParentTriggerResultOptionID) ===
                resultOption.ResultOptionId
              ) {
                this.mappedSurveyItemIdObj[
                  option.ResultOptionId
                ] = this.surveyItems[i].SurveyItemID;
                this.indexMapObj[this.surveyItems[i].SurveyItemID] = i;
              }
            }
          }
        });
      });
    }
  }

  onClickSmiley(e, SurveyItemID) {
    if (SurveyItemID) {
      this.selectSmiley.emit({
        SurveyItemID,
        index: this.indexMapObj[SurveyItemID]
      });
    }
    e.stopPropagation();
  }

  inRequiredSurveyItemIdArray(surveyItemID) {
    return this.requiredSurveyItemIdArray.indexOf(surveyItemID) !== -1;
  }
}
