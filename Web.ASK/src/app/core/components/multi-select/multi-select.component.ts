import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SurveyService } from '../../../core/services';

@Component({
  selector: 'app-multi-select',
  templateUrl: './multi-select.component.html',
  styleUrls: ['./multi-select.component.scss']
})
export class MultiSelectComponent implements OnInit {
  @Input() surveyItem: any;
  @Output() resultOptionSelected: EventEmitter<any> = new EventEmitter<any>();
  public loopingIndex = 0;
  public resultOptions: any = [];
  public selectedResultOptionIds: any = [];
  constructor(private surveyService: SurveyService) {}

  ngOnInit() {
    this.initializeSurveyItemResult(this.surveyItem.SurveyItemID);
    this.resultOptions = JSON.parse(this.surveyItem.ResultOptions);
    if (this.surveyService.isEditMode) {
      this.resultOptions.forEach(resultOption => {
        this.selectedResultOptionIds.push(resultOption.ResultOptionId);
      });
      this.surveyItem.Result.ResultOptionId = this.selectedResultOptionIds;
    }
  }

  public initializeSurveyItemResult(surveyItemId) {
    this.surveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      surveyItemId
    );
  }

  public onResultOptionSelection() {
    this.surveyItem.Result.ResultOptionId = this.selectedResultOptionIds;
  }
}
