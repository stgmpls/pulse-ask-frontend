import { NgModule } from '@angular/core';
import { AccordionComponent } from './accordion.component';

@NgModule({
  declarations: [AccordionComponent],
  exports: [AccordionComponent]
})
export class AccordionModule {}
