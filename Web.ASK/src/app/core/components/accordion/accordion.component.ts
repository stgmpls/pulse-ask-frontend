import {
  Component,
  ViewChild,
  ElementRef,
  Input,
  AfterViewInit,
  Renderer
} from '@angular/core';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements AfterViewInit {
  @ViewChild('expandWrapper', { read: ElementRef }) expandWrapper;
  @Input() expanded;
  @Input() expandHeight;

  constructor(public renderer: Renderer) {}

  ngAfterViewInit() {
    this.renderer.setElementStyle(
      this.expandWrapper.nativeElement,
      'max-height',
      this.expandHeight + 'px'
    );
  }
}
