import { Component, OnInit, Input } from '@angular/core';
import { SurveyService } from '../../../core/services';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements OnInit {
  @Input() surveyItem: any;
  public resultOptions: any = [];
  constructor(private surveyService: SurveyService) {}

  ngOnInit() {
    if (!this.surveyItem.Result) {
      this.initializeSurveyItemResult(this.surveyItem.SurveyItemID);
      this.surveyItem.Result.ResultOptionId = [];
    }
    this.resultOptions = JSON.parse(this.surveyItem.ResultOptions);
  }

  public initializeSurveyItemResult(surveyItemId) {
    this.surveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      surveyItemId
    );
  }

  public onResultOptionSelection(checked, id) {
    if (checked) {
      this.surveyItem.Result.ResultOptionId.push(id);
    } else {
      const index = this.surveyItem.Result.ResultOptionId.indexOf(id);
      this.surveyItem.Result.ResultOptionId.splice(index, 1);
    }
  }
}
