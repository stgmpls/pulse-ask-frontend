import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { SurveyService } from '../../../core/services';

@Component({
  selector: 'app-label-button',
  templateUrl: './label-button.component.html',
  styleUrls: ['./label-button.component.scss']
})
export class LabelButtonComponent implements OnInit {
  @Input() surveyItem: any;
  @Output() resultOptionSelected: EventEmitter<any> = new EventEmitter<any>();
  public resultOptions: any = [];
  constructor(private surveyService: SurveyService) {}

  ngOnInit() {
    this.initializeSurveyItemResult(this.surveyItem.SurveyItemID);
    this.resultOptions = JSON.parse(this.surveyItem.ResultOptions);
  }

  public initializeSurveyItemResult(surveyItemId) {
    this.surveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      surveyItemId
    );
  }

  public onResultOptionSelection(selectedResultOptionId) {
    this.surveyItem.Result.ResultOptionId = selectedResultOptionId;
    this.resultOptionSelected.emit({
      surveyItem: this.surveyItem,
      selectedResultOptionId: selectedResultOptionId
    });
  }
}
