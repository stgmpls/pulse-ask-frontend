import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SurveyService } from '../../services';
import * as moment from 'moment';

@Component({
  selector: 'app-time-input',
  templateUrl: './time-input.component.html',
  styleUrls: ['./time-input.component.scss']
})
export class TimeInputComponent implements OnInit {
  @Input() surveyItem: any;
  constructor(private surveyService: SurveyService) {}

  ngOnInit() {
    if (!this.surveyItem.Result) {
      this.initializeSurveyItemResult();
    }
  }

  public initializeSurveyItemResult() {
    this.surveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      this.surveyItem.SurveyItemID
    );
  }

  public updateCurrentDateTime() {
    this.surveyItem.Result.ResultText = moment
      .utc()
      .local()
      .format('DD/MM/YYYY HH:mm:ss');
  }
}
