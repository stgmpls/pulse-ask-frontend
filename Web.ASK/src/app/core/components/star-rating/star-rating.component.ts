import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { SurveyService } from '../../../core/services';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss']
})
export class StarRatingComponent implements OnInit, OnChanges {
  @Input() surveyItem: any;
  // @Output() resultOptionSelected: EventEmitter<any> = new EventEmitter<any>();
  public loopingIndex = 0;
  isdisabled: boolean;
  rate = 0;
  constructor(private surveyService: SurveyService) {}

  ngOnInit() {
    if (!this.surveyItem.Result) {
      this.initializeSurveyItemResult();
    }
    this.rate = parseInt(this.surveyItem.Result.ResultText, 10);
  }

  public initializeSurveyItemResult() {
    this.surveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      this.surveyItem.SurveyItemID
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.surveyItem && this.surveyItem.Result) {
      this.rate = parseInt(this.surveyItem.Result.ResultText, 10);
    }
  }
  public onRateChange(rating) {
    this.surveyItem.Result.ResultText = String(rating);
    // this.resultOptionSelected.emit({
    //   surveyItem: this.surveyItem,
    //   selectedResultOptionId: null
    // });
  }
}
