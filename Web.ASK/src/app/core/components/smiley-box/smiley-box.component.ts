import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { SurveyService } from '../../../core/services';


@Component({
  selector: 'app-smiley-box',
  templateUrl: './smiley-box.component.html',
  styleUrls: ['./smiley-box.component.scss']
})
export class SmileyBoxComponent implements OnInit {
  @Input() surveyItem: any;
  @Output() resultOptionSelected: EventEmitter<any> = new EventEmitter<any>();
  public loopingIndex = 0;
  public resultOptions: any = [];
  public selectResultOptionId: any = '';

  constructor(private router: Router, private surveyService: SurveyService) { }

  ngOnInit() {
    this.initializeSurveyItemResult(this.surveyItem.SurveyItemID);
    this.resultOptions = JSON.parse(this.surveyItem.ResultOptions);
  }

  public initializeSurveyItemResult(surveyItemId) {
    this.surveyItem.Result = this.surveyService.createSurveyItemResultInstance(surveyItemId);
  }

  public emojiSelected(selectedResultOptionID) {
    this.selectResultOptionId = selectedResultOptionID;
    this.surveyItem.Result.ResultOptionId = selectedResultOptionID;
    this.resultOptionSelected.emit({'surveyItem': this.surveyItem, 'selectedResultOptionId': selectedResultOptionID});
  }

}
