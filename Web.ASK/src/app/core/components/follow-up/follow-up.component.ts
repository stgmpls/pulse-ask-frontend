import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Platform } from '@ionic/angular';

import { SurveyService } from '../../../core/services';
import { SURVEY_TYPE } from '../../constant/constants';

@Component({
  selector: 'app-follow-up',
  templateUrl: './follow-up.component.html',
  styleUrls: ['./follow-up.component.scss']
})
export class FollowUpComponent implements OnInit {
  @Input() surveyItem: any;
  @Input() surveyType: any;
  @Input() isKiosk: boolean;
  @Output() resultOptionSelected: EventEmitter<any> = new EventEmitter<any>();
  public loopingIndex = 0;
  public resultOptions: any = [];
  roleId: number;
  sectorId: string;
  username: string;
  timer: any;
  SURVEY_TYPE = SURVEY_TYPE;
  isPrevious = false;

  constructor(
    private surveyService: SurveyService,
    private platform: Platform
  ) {}

  ionViewWillLeave() {
    clearTimeout(this.timer);
  }

  ngOnInit() {
    if (this.surveyType !== this.SURVEY_TYPE.ACUITY) {
      this.setTimeOut();
    }
    if (!this.surveyItem.Result) {
      this.initializeSurveyItemResult(this.surveyItem.SurveyItemID);
    } else {
      this.isPrevious = true;
    }
    this.resultOptions = JSON.parse(this.surveyItem.ResultOptions);
  }

  public initializeSurveyItemResult(surveyItemId) {
    this.surveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      surveyItemId
    );
  }

  public selectedAnswer(resultOption) {
    this.isPrevious = false;
    this.surveyItem.Result.ResultOptionId = resultOption.ResultOptionId;
  }

  public onChanged(e: any) {
    if (this.timer && this.surveyType !== this.SURVEY_TYPE.ACUITY) {
      clearTimeout(this.timer);
      this.setTimeOut();
    } else if (
      this.surveyType === this.SURVEY_TYPE.ACUITY &&
      this.isKiosk &&
      !this.isPrevious
    ) {
      this.resultOptionSelected.emit({
        surveyItem: this.surveyItem,
        selectedResultOptionId: this.surveyItem.Result.ResultOptionId,
        isSurveyCompleted: false
      });
    }
  }

  public submitModal() {
    const isSurveyCompleted = true;
    this.resultOptionSelected.emit({
      surveyItem: this.surveyItem,
      selectedResultOptionId: this.surveyItem.Result.ResultOptionId,
      isSurveyCompleted: isSurveyCompleted
    });
  }

  setTimeOut() {
    this.timer = setTimeout(() => {
      this.submitModal();
    }, 15000);
  }
}
