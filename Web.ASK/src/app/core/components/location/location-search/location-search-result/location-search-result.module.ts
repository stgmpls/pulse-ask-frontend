import { NgModule } from '@angular/core';
import { LocationSearchResultComponent } from './location-search-result.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [CommonModule, IonicModule],
  declarations: [LocationSearchResultComponent],
  exports: [LocationSearchResultComponent]
})
export class LocationSearchResultModule {}
