import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'location-search-result',
  templateUrl: './location-search-result.component.html',
  styleUrls: ['./location-search-result.component.scss']
})
export class LocationSearchResultComponent implements OnInit {
  @Input() locations: any = [];
  @Input() isNetworkAvailable = true;
  @Input() isNetworkConnected = true;
  @Input() theme = 'ask';
  @Output() onSelectLocation: EventEmitter<any> = new EventEmitter();
  @Output() networkNotAvailable: EventEmitter<any> = new EventEmitter();
  constructor() { }
  ngOnInit() { }

  onConfigure(location) {
    if (this.isNetworkAvailable) {
      this.onSelectLocation.emit(location);
    } else {
      this.networkNotAvailable.emit();
    }
  }
}
