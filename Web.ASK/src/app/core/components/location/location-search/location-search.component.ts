import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  ViewChild,
  Output,
  EventEmitter
} from '@angular/core';
import { Subscription } from 'rxjs';
import { NetworkService, HelperService } from '../../../services';
import { LocationService } from '../../../services/location.service';
import { ILocationSearch, State, FormConfig } from '../../../entities';
import { NavController, LoadingController } from '@ionic/angular';
import { LocationSearchFormComponent } from './location-search-form';
import { LOC_FORM_CONFIG, STATES_ARRAY } from '../../../constant/constants';
import { Router } from '@angular/router';

@Component({
  selector: 'location-search',
  templateUrl: './location-search.component.html',
  styleUrls: ['./location-search.component.scss']
})
export class LocationSearchComponent implements OnInit, OnDestroy {
  @Input() theme = 'ask';
  @Input() states: State[] = STATES_ARRAY;
  @Input() formConfig: FormConfig = LOC_FORM_CONFIG;
  @Input() navigateUrl: null;
  @Output() onGettingLocation: EventEmitter<boolean> = new EventEmitter();
  @Output() searchLocation: EventEmitter<any> = new EventEmitter();
  @Output() networkNotAvailable: EventEmitter<any> = new EventEmitter();
  @Output() getSelectedLocation: EventEmitter<any> = new EventEmitter();
  @ViewChild('locForm') locationSearchComponent: LocationSearchFormComponent;
  listOfLocations: any = [];
  public loading: any;

  constructor(
    public networkMgr: NetworkService,
    private searchCostCenterService: LocationService,
    private helperService: HelperService,
    private router: Router,
    private loadingCtrl: LoadingController
  ) {}

  ngOnInit() {}

  ngOnDestroy() {}

  searchLocations(location: ILocationSearch) {
    this.searchLocation.emit(location);
    this.searchCostCenterService
      .searchCostCenter(
        location.number,
        location.city,
        location.state,
        location.name,
        location.zip_code
      )
      .subscribe(data => {
        if (data.Data) {
          this.listOfLocations = data.Data;
        } else {
          this.helperService.showAlert(
            'No locations found. Please try other search criteria.'
          );
          this.resetLocationSearchForm();
          this.listOfLocations = [];
        }
      });
  }

  resetLocationSearchForm() {
    this.locationSearchComponent.resetLocationForm();
  }

  networkIsNotAvailable() {
    this.helperService.showAlert(
      'No internet connection available. Please check your settings.'
    );
    this.networkNotAvailable.emit(true);
  }

  public listClick(location): void {
    this.searchCostCenterService
      .getAskAdminRole(location.SectorAdamId)
      .subscribe(response => {
        if (response.Data && response.Data.RoleID > 0) {
          this.searchCostCenterService.saveLocation(location).then(() => {
            localStorage.setItem('roleId', response.Data.RoleID);
            localStorage.setItem('theme', 'theme-' + location.SectorAdamId);
            localStorage.setItem('selectedLocation', JSON.stringify(location));
            localStorage.setItem('selectedLocationId', location.LocationId);
            localStorage.setItem('sectorId', location.SectorAdamId);
            this.onGettingLocation.emit(true);
            this.getSelectedLocation.emit(location);
          });
        } else {
          this.showLocationError();
        }
      });
  }

  async showLocationError() {
    this.loading = await this.loadingCtrl.create({
      message:
        'Invalid location! ASK admin role is not configured for this sector.',
      duration: 3000
    });
    await this.loading.present();
  }
}
