import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { STATES_ARRAY, LOC_FORM_CONFIG } from '../../../../constant/constants';
import { State, FormConfig } from '../../../../entities';

@Component({
  selector: 'location-search-form',
  templateUrl: './location-search-form.component.html',
  styleUrls: ['./location-search-form.component.scss']
})
export class LocationSearchFormComponent implements OnInit {
  @Input() states: State[] = STATES_ARRAY;
  @Input() theme = 'ask';
  @Input() formConfig: FormConfig = LOC_FORM_CONFIG;
  @Input() isNetworkAvailable = true;
  @Input() isNetworkConnected = true;
  @Output() searchLocation: EventEmitter<any> = new EventEmitter();
  @Output() networkNotAvailable: EventEmitter<any> = new EventEmitter();
  locationSearchForm: FormGroup;
  configurationMap: Map<string, string>;
  formGroupObject: any = {};
  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.setConfiguration();
    this.createLocationSearchForm();
  }

  /* Add entries in map so as know which form
   field gets configuration from which
   configuration attribute of 'formConfig' object. */
  setConfiguration() {
    this.configurationMap = new Map();
    this.configurationMap.set('number', 'costCenterNumber');
    this.configurationMap.set('name', 'costCenterName');
    this.configurationMap.set('state', 'state');
    this.configurationMap.set('city', 'city');
    this.configurationMap.set('zip_code', 'zipCode');
  }

  /* Initialize location search form such that all form fields have default values
   that are specified in 'formConfig' object. */
  createLocationSearchForm() {
    this.setDefaultValuesForFormFields();
    this.locationSearchForm = this.fb.group(this.formGroupObject);
  }

  /* Initialize 'formGroupObject' with default values for all fields if 'locationSearchForm'
   is not present. Otherwise assign default values to those fields which have been set to
    'null', when user attempted to empty previously filled form fields, with the help of
     'formConfig' and 'configurationMap'. */
  setDefaultValuesForFormFields() {
    this.formGroupObject = this.locationSearchForm
      ? this.locationSearchForm.value
      : {};
    const configurationMapIterator = this.configurationMap.entries();
    let i = 0;
    for (i = 0; i < this.configurationMap.size; i++) {
      const key = configurationMapIterator.next().value[0];
      if (
        this.formGroupObject[key] === null ||
        this.formGroupObject[key] === undefined
      ) {
        this.formGroupObject[key] = this.formConfig[
          this.configurationMap.get(key)
        ].defaultValue;
      }
    }
  }

  onSearch() {
    this.setDefaultValuesForFormFields();
    if (this.isNetworkAvailable) {
      this.searchLocation.emit(this.locationSearchForm.value);
    } else {
      this.networkNotAvailable.emit();
    }
  }

  resetLocationForm() {
    this.locationSearchForm.reset({
      number: '',
      name: '',
      state: ' ',
      city: '',
      zip_code: ''
    });
  }
}
