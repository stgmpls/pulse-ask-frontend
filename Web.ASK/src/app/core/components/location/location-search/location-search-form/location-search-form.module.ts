import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { LocationSearchFormComponent } from './location-search-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InputNumberCheckModule } from '../../../../directives/input-number/input-number.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    InputNumberCheckModule
  ],
  declarations: [LocationSearchFormComponent],
  exports: [LocationSearchFormComponent]
})
export class LocationSearchFormModule {}
