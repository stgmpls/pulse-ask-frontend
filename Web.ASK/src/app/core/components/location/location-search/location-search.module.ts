import { NgModule } from '@angular/core';
import { LocationSearchFormModule } from './location-search-form';
import { LocationSearchResultModule } from './location-search-result';
import { LocationSearchComponent } from './location-search.component';

@NgModule({
    imports: [
        LocationSearchFormModule,
        LocationSearchResultModule
    ],
    declarations: [
        LocationSearchComponent
    ],
    exports: [
        LocationSearchComponent
    ]
})
export class LocationSearchModule {

}
