import {
  Component,
  Input,
  OnInit,
  OnChanges,
  SimpleChanges
} from '@angular/core';

interface IResultOption {
  FilterIdentifier: any;
  IsActive: boolean;
  IsLooping: boolean;
  LocationDataSource: any;
  ResultOptionId: number;
  SurveyDetailsId: string;
  Text: string;
  Value: any;
}

@Component({
  selector: 'app-result-option',
  templateUrl: './result-option.component.html',
  styleUrls: ['./result-option.component.scss']
})
export class ResultOptionComponent implements OnChanges {
  @Input() surveyItem;

  addResultOption() {
    if (
      this.surveyItem.Result.FollowupOptions.filter(
        item => item.Text.trim() === ''
      ).length === 0
    ) {
      this.surveyItem.Result.FollowupOptions.push({ Text: '' });
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.surveyItem.Result && !this.surveyItem.Result.FollowupOptions) {
      this.surveyItem.Result.FollowupOptions = [];
      JSON.parse(this.surveyItem.ResultOptions).map(item => {
        this.surveyItem.Result.FollowupOptions.push({ Text: item.Text });
      });
    }
  }
  removeResultOption(i) {
    this.surveyItem.Result.FollowupOptions.splice(i, 1);
  }

  prohibitDuplicate(followupOption) {
    if (followupOption && followupOption.Text) {
      if (
        this.surveyItem.Result.FollowupOptions.filter(
          item => item.Text.trim() === followupOption.Text.trim()
        ).length > 1
      ) {
        followupOption.Text = '';
      }
    }
  }
}
