import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  Output,
  EventEmitter
} from '@angular/core';
import {
  SurveyService,
  SyncService,
  NetworkService
} from '../../../core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { SurveySetupService } from '../../../survey-setup/survey-setup.service';
import { ModalController, Platform } from '@ionic/angular';
import { ThankYouModalComponent } from '../thank-you-modal/thank-you-modal.component';
import { SURVEY_TYPE } from '../../constant/constants';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit, OnDestroy {
  surveySegmentSubscription: Subscription;
  surveyIdSubscription: Subscription;
  surveysByCategorySubscription: Subscription;
  public surveyItemValues = [];
  public surveySegment: string;
  public surveysByCategory: any = [];
  public listOfSurvey: any = [];
  public surveyHeader: any = {};
  public selectedSurvey: any;
  @Input() surveyItems: any;
  roleId: number;
  sectorId: string;
  username: string;
  selectedSurveyId = 0;
  public isAdmin = true;
  public isKiosk = true;
  public currentDisplayGroupData: any;
  public readonly segmentTypeNew = 'new';
  public currentIndex = 0;
  public maxDisplayGroupId = 0;
  public surveyCategory;
  public SURVEY_TYPE = SURVEY_TYPE;
  public surveyItemsCopy;
  public surveyProgressValue;
  public showNextPrevBtn = false;
  public showResetBtn = false;
  @Output() displayGroupSelectionChanged: EventEmitter<any> = new EventEmitter<
    any
  >();
  timer;
  sessionTimer;
  isCurrentSurveyOutdated = false;
  isTemplateOutdated = false;
  private selectedLocationId: string;
  lastItem = false;
  public isDesktopBrowser = false;
  constructor(
    private surveyService: SurveyService,
    private syncService: SyncService,
    private networkService: NetworkService,
    private router: Router,
    private surveySetupService: SurveySetupService,
    private route: ActivatedRoute,
    private modalCtrl: ModalController,
    private screenOrientation: ScreenOrientation,
    private platform: Platform
  ) {
    if (this.platform.is('desktop')) {
      this.isDesktopBrowser = true;
    } else {
      this.isDesktopBrowser = false;
    }
    this.subscribeToServiceVariables();
  }

  private subscribeToServiceVariables() {
    this.surveySegmentSubscription = this.surveySetupService.surveySegment$.subscribe(
      surveySegment => {
        this.surveySegment = surveySegment;
      }
    );

    this.surveysByCategorySubscription = this.surveySetupService.surveysByCategory$.subscribe(
      surveysByCategory => {
        this.surveysByCategory = surveysByCategory;
        this.prepareListOfSurveys();
      }
    );
  }
  ngOnInit() {
    this.surveyService.questionResultId = -1;
    this.surveyProgressValue = 0;
    this.roleId = Number(localStorage.getItem('roleId'));
    this.sectorId = localStorage.getItem('sectorId');
    this.username = localStorage.getItem('username');
    this.selectedLocationId = localStorage.getItem('selectedLocationId');
    this.isCurrentSurveyOutdated = false;
    clearTimeout(this.timer);
    clearTimeout(this.sessionTimer);
    if (this.router.url.indexOf('/user-response') > -1) {
      this.isAdmin = false;
    } else {
      this.isAdmin = true;
    }
    this.init();
    if (!this.isAdmin) {
      this.getLatestSurveyId(
        Number(this.selectedSurveyId.toString()),
        this.selectedLocationId
      );
      this.surveyService
        .getSurveyById(this.selectedSurveyId)
        .then((response: any) => {
          this.surveyHeader = JSON.parse(response.rows.item(0).survey_content);
          if (this.surveyHeader.ShowResetButton) {
            this.showResetBtn = this.surveyHeader.ShowResetButton;
          }
        });
      this.setDataInteval();
    }
  }

  private fetchDataFromLocalStorage() {
    this.roleId = Number(localStorage.getItem('roleId'));
    this.sectorId = localStorage.getItem('sectorId');
    this.username = localStorage.getItem('username');
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.unsubscribeFromServiceVariables();
    if (this.timer) {
      clearTimeout(this.timer);
    }
    if (this.sessionTimer) {
      clearTimeout(this.sessionTimer);
    }
  }

  private unsubscribeFromServiceVariables() {
    if (this.surveySegmentSubscription) {
      this.surveySegmentSubscription.unsubscribe();
    }
    if (this.surveyIdSubscription) {
      this.surveyIdSubscription.unsubscribe();
    }
    if (this.surveysByCategorySubscription) {
      this.surveysByCategorySubscription.unsubscribe();
    }
  }

  public init() {
    this.route.params.subscribe((data: any) => {
      if (data.id) {
        this.selectedSurveyId = data.id;
      } else {
        this.selectedSurveyId = this.surveyService.surveyId;
        if (this.isAdmin) {
          this.surveyService.isEditMode = false;
        }
      }
      if (!this.isAdmin) {
        this.resetThemeVariables();
        this.getSectorConfig();
      }
      if (this.selectedSurveyId !== 0) {
        this.getSurveyCategory(this.selectedSurveyId).then(res => {
          this.surveyCategory = res.survey_category;
          if (this.surveyCategory === SURVEY_TYPE.ACUITY) {
            this.surveyService.deleteActiveSurveyDetails().then(() => {
              this.surveyService
                .addActiveSurvey(this.selectedSurveyId)
                .then(() => {
                  this.getItemsForSurvey(this.selectedSurveyId);
                  this.showNextPrevBtn = true;
                });
            });
          } else {
            this.getItemsForSurvey(this.selectedSurveyId);
          }
        });
      }
    });
  }

  public onClickResetButton() {
    this.reStartSurvey(this.selectedSurveyId);
  }

  private prepareListOfSurveys() {
    this.listOfSurvey = [];
    for (let i = 0; i < this.surveysByCategory.length; i++) {
      const survey = {
        surveyId: this.surveysByCategory[i].Header.SurveyId,
        originalSurveyId: this.surveysByCategory[i].Header.OriginalSurveyID,
        description: this.surveysByCategory[i].Header.Description
      };
      this.listOfSurvey.push(survey);
    }
  }

  public onSurveyChange() {
    this.getDisplayGroupData(
      this.username,
      this.sectorId,
      this.roleId,
      this.surveyService.surveyId,
      undefined,
      undefined,
      0,
      0,
      null,
      false,
      false,
      true
    );
  }

  public getSurveyById() {
    this.getItemsForSurvey(this.selectedSurvey.surveyId);
  }

  public getItemsForSurvey(surveyId) {
    this.getDisplayGroupData(
      this.username,
      this.sectorId,
      this.roleId,
      surveyId,
      undefined,
      undefined,
      0,
      0,
      null,
      false,
      false,
      this.isAdmin
    );
  }

  public onResultOptionSelection(
    surveyItemAndItsResponseData,
    isPrevSurvey = false
  ) {
    const isSurveyCompleted = surveyItemAndItsResponseData.isSurveyCompleted;
    const surveyItemData = new Array(surveyItemAndItsResponseData.surveyItem);
    this.saveSurveyItemResult(surveyItemData).then(data => {
      this.nextEvent(this.selectedSurveyId, isSurveyCompleted);
    });
  }

  public onRadioResultOptionSelection(
    surveyItemAndItsResponseData,
    isPrevSurvey = false
  ) {
    this.lastItem = JSON.parse(this.currentDisplayGroupData.isLast);
  }

  saveSurveyItemResult(surveyItemAndItsResponseData) {
    const endSurvey = false;
    this.surveyItemValues = surveyItemAndItsResponseData;

    return this.surveyService
      .saveSurveyItemResult(
        this.username,
        this.sectorId,
        this.roleId,
        this.surveyService.questionResultId,
        this.selectedSurveyId,
        this.surveyItemValues,
        endSurvey
      )
      .then((surveyItemResultId: any) => {
        return this.surveyService
          .saveSurveyItemResult(
            this.username,
            this.sectorId,
            this.roleId,
            surveyItemResultId,
            this.selectedSurveyId,
            this.surveyItemValues,
            endSurvey
          )
          .then((data: any) => {
            this.surveyService.questionResultId = data;
            return data;
          });
      });
  }

  public async showThankyouModal(surveyId) {
    const modal = await this.modalCtrl.create({
      component: ThankYouModalComponent,
      cssClass: 'inset-modal thanks-page'
    });
    await modal.present();
    modal.present();
    setTimeout(() => {
      modal.dismiss();
      this.lastItem = false;
      this.getDisplayGroupData(
        this.username,
        this.sectorId,
        this.roleId,
        surveyId,
        undefined,
        undefined,
        0,
        0,
        null,
        false,
        false,
        false
      );
    }, 2000);
  }

  public nextEvent(surveyId, isSurveyCompleted) {
    // Once survey last question is properly configured we need to use this this.currentDisplayGroupData.isLast conditon in if
    // As last question should not be branching or looping question
    if (
      isSurveyCompleted === true &&
      (this.surveyCategory === SURVEY_TYPE.PREDEFINED ||
        this.surveyCategory === SURVEY_TYPE.TASTING)
    ) {
      this.CompleteTheSurvey(surveyId);
    } else {
      // Assumming that only the last surveyItem on a page will contain branching.
      const lastSurveyItemIndex = this.surveyItems.length - 1;
      const lastSurveyItem = this.surveyItems[lastSurveyItemIndex];
      this.SetSurveyProgress(surveyId);

      if (lastSurveyItem.ItemTypeCode !== 'ML') {
        if (this.sessionTimer) {
          clearTimeout(this.sessionTimer);
          this.setSurveySessionTimeout();
        } else {
          this.setSurveySessionTimeout();
        }
      }

      let selectedOption;
      if (this.isKiosk && lastSurveyItem.ItemTypeCode === 'ML') {
        selectedOption = {
          ResultOptionId: this.selectedLocationId,
          Text: 'ASK Location',
          IsLooping: false,
          IsActive: true,
          SurveyDetailsId: null
        };
      } else if (lastSurveyItem.Result !== undefined) {
        selectedOption = JSON.parse(lastSurveyItem.ResultOptions).find(
          resultOption =>
            resultOption.ResultOptionId === lastSurveyItem.Result.ResultOptionId
        );
      }

      if (selectedOption && selectedOption.SurveyDetailsId !== null) {
        this.getDisplayGroupData(
          this.username,
          this.sectorId,
          this.roleId,
          surveyId,
          undefined,
          undefined,
          0,
          selectedOption.SurveyDetailsId,
          this.currentDisplayGroupData.parent_id,
          false,
          true,
          false
        );
      } else {
        if (
          this.currentDisplayGroupData.parent_id !== 'null' &&
          this.currentDisplayGroupData.parent_id !== null
        ) {
          this.getDisplayGroupData(
            this.username,
            this.sectorId,
            this.roleId,
            surveyId,
            this.currentDisplayGroupData.context_group_id,
            this.currentDisplayGroupData.display_group_id,
            0, // this.currentLoopingIndex,
            this.currentDisplayGroupData.unique_id,
            this.currentDisplayGroupData.parent_id,
            false,
            true,
            false
          );
        } else {
          this.getDisplayGroupData(
            this.username,
            this.sectorId,
            this.roleId,
            surveyId,
            this.currentDisplayGroupData.context_group_id,
            this.currentDisplayGroupData.display_group_id,
            0, // this.currentLoopingIndex,
            this.currentDisplayGroupData.unique_id,
            this.currentDisplayGroupData.parent_id,
            false,
            false,
            false
          );
        }
      }
    }
  }

  public getDisplayGroupData(
    username,
    sectorId,
    roleId,
    surveyId,
    contextGroup,
    displayGroup,
    loopingIndex,
    uniqueId,
    parentId,
    canBacktrackBranches,
    inChild,
    isAdmin
  ) {
    this.surveyService
      .getDisplayGroupData(
        username,
        sectorId,
        roleId,
        surveyId,
        contextGroup,
        displayGroup,
        loopingIndex,
        uniqueId,
        parentId,
        canBacktrackBranches,
        inChild,
        isAdmin
      )
      .then((response: any) => {
        if (response && response.survey_item) {
          if (this.surveyCategory === SURVEY_TYPE.ACUITY) {
            this.renderACSurveyItem(response, surveyId);
          } else {
            this.renderSurveyItem(response, surveyId);
          }
        } else {
          this.CompleteTheSurvey(surveyId);
        }
      });
  }

  public SetSurveyProgress(surveyId) {
    this.surveyService
      .getSurveyUniqueId(surveyId)
      .then((uniqueSurveyId: number) => {
        this.surveyService
          .getSurveyDetails(uniqueSurveyId)
          .then((resp: any) => {
            const maxDispayGroupCount = resp.rows.length;
            let currentSurveyIndex = 0;
            if (
              this.currentDisplayGroupData &&
              this.currentDisplayGroupData.display_group_id
            ) {
              for (let i = 0; i < resp.rows.length; i++) {
                if (
                  resp.rows.item(i).display_group_id ===
                  this.currentDisplayGroupData.display_group_id
                ) {
                  currentSurveyIndex = i;
                }
              }
            }
            this.surveyProgressValue =
              (currentSurveyIndex + 1) / maxDispayGroupCount;
          });
      });
  }

  public CompleteTheSurvey(surveyId) {
    this.surveyService
      .updateSurveyItemStatus(
        this.username,
        this.sectorId,
        this.roleId,
        this.surveyService.questionResultId,
        'Completed'
      )
      .then((resp: any) => {
        const roleId = Number(localStorage.getItem('roleId'));
        const sectorId = localStorage.getItem('sectorId');
        const username = localStorage.getItem('username');

        let isNetworkConnected = false;
        if (
          this.networkService.isAvailiable &&
          this.networkService.isConnected
        ) {
          isNetworkConnected = true;
        }
        this.syncService
          .submitFormDetails(
            username,
            sectorId,
            roleId,
            isNetworkConnected,
            true
          )
          .then(() => {
            this.reStartSurvey(surveyId);
          });
      });
  }

  public getPreviousSurveyItems() {
    this.lastItem = false;
    this.displayGroupSelectionChanged.emit();
    const surveyItemAndItsResponseData = [];
    this.surveyItems.forEach(surveyItem => {
      if (surveyItem.Result) {
        surveyItemAndItsResponseData.push(surveyItem);
      }
    });

    this.saveSurveyItemResult(surveyItemAndItsResponseData).then(data => {
      this.prevEvent(this.selectedSurveyId);
    });
  }

  public checkForLastItem() {
    // check if current survey item is second-last
    let inChild = false;
    if (
      this.currentDisplayGroupData.parent_id !== 'null' &&
      this.currentDisplayGroupData.parent_id !== null
    ) {
      inChild = true;
    }
    return this.surveyService.getDisplayGroupData(
      this.username,
      this.sectorId,
      this.roleId,
      this.selectedSurveyId,
      this.currentDisplayGroupData.context_group_id,
      this.currentDisplayGroupData.display_group_id,
      0, // this.currentLoopingIndex,
      this.currentDisplayGroupData.unique_id,
      this.currentDisplayGroupData.parent_id,
      false,
      inChild,
      false
    );
  }

  public isLastItem(isFollowupQuestion: boolean) {
    let searchBySurveyDetailsId = false;
    if (!isFollowupQuestion && this.lastItem === true) {
      return Promise.resolve({ isLast: 'true' });
    } else {
      if (isFollowupQuestion && this.surveyItems.length > 0) {
        const lastSurveyItemIndex = this.surveyItems.length - 1;
        const lastSurveyItem = this.surveyItems[lastSurveyItemIndex];
        if (
          lastSurveyItem.ResultOptions.length > 2 &&
          lastSurveyItem.Result &&
          lastSurveyItem.Result.ResultOptionId
        ) {
          const selectedOption = JSON.parse(lastSurveyItem.ResultOptions).find(
            resultOption =>
              resultOption.ResultOptionId ===
              lastSurveyItem.Result.ResultOptionId
          );
          if (selectedOption && selectedOption.SurveyDetailsId !== null) {
            searchBySurveyDetailsId = true;
            return this.surveyService.getDisplayGroupData(
              this.username,
              this.sectorId,
              this.roleId,
              this.selectedSurveyId,
              undefined,
              undefined,
              0,
              selectedOption.SurveyDetailsId,
              this.currentDisplayGroupData.parent_id,
              false,
              true,
              false
            );
          }
        }
        if (!searchBySurveyDetailsId) {
          return this.checkForLastItem();
        }
      } else {
        return this.checkForLastItem();
      }
    }
  }
  public getNextSurveyItems() {
    // check if current survey item is second-last
    this.isLastItem(false).then((response: any) => {
      this.lastItem = JSON.parse(this.currentDisplayGroupData.isLast);
      this.displayGroupSelectionChanged.emit();
      const surveyItemAndItsResponseData = [];
      const isSurveyCompleted = JSON.parse(this.currentDisplayGroupData.isLast);
      this.surveyItems.forEach(surveyItem => {
        if (surveyItem.Result) {
          surveyItemAndItsResponseData.push(surveyItem);
        }
      });

      this.saveSurveyItemResult(surveyItemAndItsResponseData).then(data => {
        this.nextEvent(this.selectedSurveyId, isSurveyCompleted);
      });
    });
  }

  public prevEvent(surveyId) {
    this.surveyService
      .fetchParentIdForCurrentSurveyItem(this.currentDisplayGroupData.unique_id)
      .then(parentId => {
        this.surveyService
          .fetchPreviousGroupData(
            this.username,
            this.sectorId,
            this.roleId,
            parentId,
            surveyId
          )
          .then((response: any) => {
            if (response && response.survey_item) {
              if (this.surveyCategory === SURVEY_TYPE.ACUITY) {
                this.setCurrentActiveSurveyItemData(response);
                this.surveyItemsCopy = JSON.parse(response.survey_item);
                this.getUserSurveyItemResult(response.unique_id);
              } else {
                this.renderSurveyItem(response, surveyId);
              }
            } else {
              this.CompleteTheSurvey(surveyId);
            }
            this.SetSurveyProgress(surveyId);
          });
      });
  }

  resetToStartOftheSurvey(surveyId) {
    // Need to clear out timer for the first display group.
    if (this.sessionTimer) {
      clearTimeout(this.sessionTimer);
    }
    this.surveyProgressValue = 0;
    this.getDisplayGroupData(
      this.username,
      this.sectorId,
      this.roleId,
      surveyId,
      undefined,
      undefined,
      0,
      0,
      null,
      false,
      false,
      false
    );
  }

  renderACSurveyItem(response, surveyId) {
    let levelParentId = null;
    if (this.currentDisplayGroupData) {
      levelParentId = this.currentDisplayGroupData.unique_id;
    }
    this.surveyService
      .updateTableActiveSurveyDetails(response.unique_id, levelParentId)
      .then(() => {
        this.setCurrentActiveSurveyItemData(response);
        this.lastItem = JSON.parse(response.isLast);
        this.surveyItemsCopy = JSON.parse(response.survey_item);
        this.getUserSurveyItemResult(response.unique_id);
      });
  }

  renderSurveyItem(response, surveyId) {
    this.setCurrentActiveSurveyItemData(response);

    if (
      this.surveyCategory === SURVEY_TYPE.PREDEFINED ||
      this.surveyCategory === SURVEY_TYPE.TASTING
    ) {
      this.setCurrentSurveyItems(JSON.parse(response.survey_item));
    } else if (this.surveyCategory === SURVEY_TYPE.CUSTOM) {
      if (JSON.parse(this.currentDisplayGroupData.isLast) === false) {
        this.setCurrentSurveyItems(JSON.parse(response.survey_item));
      } else {
        this.CompleteTheSurvey(surveyId);
      }
    }
  }

  setCurrentActiveSurveyItemData(response) {
    this.currentDisplayGroupData = response;
  }

  setCurrentSurveyItems(surveyItems) {
    this.surveyItems = surveyItems;
    this.surveyService.surveyData = this.surveyItems;
    surveyItems.forEach(survey => {
      survey.Text = survey.Text.replace('""', "'");
      survey.ScaffoldItemText = survey.ScaffoldItemText.replace('""', "'");
    });

    if (
      this.surveyItems[0].ItemTypeCode === 'ML' &&
      this.surveyHeader.Category === SURVEY_TYPE.ACUITY
    ) {
      this.nextEvent(this.selectedSurveyId, 'false');
    }
  }

  getSurveyCategory(surveyId) {
    return this.surveyService.getSurveyById(surveyId).then((response: any) => {
      if (response && response.rows && response.rows.length > 0) {
        return response.rows.item(0);
      }
    });
  }

  setUserSurveyItemResult(surveyItemsResult) {
    surveyItemsResult.map(surveyItemResult => {
      this.surveyItemsCopy.map(surveyItem => {
        if (surveyItemResult.SurveyItemID === surveyItem.SurveyItemID) {
          surveyItem.Result = surveyItemResult;
        }
      });
    });
  }

  getUserSurveyItemResult(uniqueId) {
    this.surveyService.getUserSurveyItemResult(uniqueId).then((res: any) => {
      if (res && res.rows && res.rows.length > 0) {
        this.setUserSurveyItemResult(
          JSON.parse(res.rows.item(0).survey_item_result)
        );
        this.setCurrentSurveyItems(this.surveyItemsCopy);
      }
    });
  }

  isInValidSurveyItems() {
    let flag = false;
    if (!this.surveyItems || this.surveyItems.length < 0) {
      return true;
    }
    for (let i = 0; i < this.surveyItems.length; i++) {
      const activeSurveyItem = this.surveyItems[i];
      if (
        JSON.parse(activeSurveyItem.IsRequired) &&
        activeSurveyItem.Result &&
        activeSurveyItem.Result.ResultText.trim() === '' &&
        (activeSurveyItem.Result.ResultOptionId === null ||
          activeSurveyItem.Result.ResultOptionId.length === 0)
      ) {
        flag = true;
      }
    }
    return flag;
  }

  reStartSurvey(surveyId) {
    this.surveyProgressValue = 0;
    this.lastItem = true;
    this.surveyService.questionResultId = -1;
    if (
      this.surveyCategory === SURVEY_TYPE.PREDEFINED ||
      this.surveyCategory === SURVEY_TYPE.TASTING
    ) {
      this.showThankyouModal(surveyId);
    } else if (this.surveyCategory === SURVEY_TYPE.ACUITY) {
      this.surveyService.deleteActiveSurveyDetails().then(() => {
        this.surveyService.addActiveSurvey(surveyId).then(() => {
          this.showThankyouModal(surveyId);
        });
      });
    } else if (this.surveyCategory === SURVEY_TYPE.CUSTOM) {
      this.showThankyouModal(surveyId);
    }
  }

  setDataInteval() {
    this.timer = setInterval(() => {
      this.getLatestSurveyId(
        Number(this.selectedSurveyId.toString()),
        this.selectedLocationId
      );
    }, 600000);
  }

  setSurveySessionTimeout() {
    let sessionTimeout = 60000;
    if (
      this.surveyHeader &&
      this.surveyHeader.SessionTimeout &&
      this.surveyHeader.SessionTimeout > 0
    ) {
      sessionTimeout = this.surveyHeader.SessionTimeout;
    }

    this.sessionTimer = setInterval(() => {
      this.resetToStartOftheSurvey(this.selectedSurveyId);
    }, sessionTimeout);
  }

  getLatestSurveyId(surveyId: number, locationId: string) {
    let latestSurveyId = 0;
    this.surveyService
      .getLatestVersionSurveyId(surveyId, locationId, true)
      .subscribe(response => {
        latestSurveyId = response.Data.LatestSurveyId;
        if (response.Data.IsTemplateUpdatedAfterSurveyCreation) {
          this.isTemplateOutdated = true;
        } else {
          this.isTemplateOutdated = false;
          if (latestSurveyId !== surveyId) {
            this.isCurrentSurveyOutdated = true;
          } else {
            this.isCurrentSurveyOutdated = false;
          }
        }
      });
  }

  ionViewWillUnload() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    if (this.sessionTimer) {
      clearTimeout(this.sessionTimer);
    }
  }

  getSectorConfig() {
    this.surveyService
      .getSectorConfig(this.selectedSurveyId)
      .subscribe((themeConfig: any) => {
        this.setThemeVariables(themeConfig);
      });
  }

  setThemeVariables(themeConfig: any) {
    const that = this;
    if (themeConfig.Data && themeConfig.Data.StyleConfigs.length > 0) {
      themeConfig.Data.StyleConfigs.forEach((themeData: any) => {
        if (themeData.Name === 'Radio Selected Color') {
          document.body.style.setProperty('--radio-checked', themeData.Value);
        } else if (themeData.Name === 'Radio Not Selected Color') {
          document.body.style.setProperty('--radio-unchecked', themeData.Value);
        } else if (themeData.Name === 'Title Color') {
          document.body.style.setProperty('--title', themeData.Value);
        } else if (themeData.Name === 'SubTitle Color') {
          document.body.style.setProperty('--sub-title', themeData.Value);
        } else if (themeData.Name === 'Question Font Color') {
          document.body.style.setProperty(
            '--question-font-color',
            themeData.Value
          );
        } else if (themeData.Name === 'Submit Button Color') {
          document.body.style.setProperty(
            '--submit-button-color',
            themeData.Value
          );
        } else if (themeData.Name === 'Background Image') {
          document.body.style.setProperty(
            '--theme-img',
            'url(' + themeData.Value + ')'
          );
        } else if (themeData.Name === 'Thankyou Image') {
          document.body.style.setProperty(
            '--thank-you-image',
            'url(' + themeData.Value + ')'
          );
        } else if (themeData.Name === 'Screen Orientation') {
          if (!that.isDesktopBrowser) {
            if (themeData.Value === 'Landscape') {
              this.screenOrientation.lock(
                this.screenOrientation.ORIENTATIONS.LANDSCAPE_PRIMARY
              );
            } else {
              this.screenOrientation.lock(
                this.screenOrientation.ORIENTATIONS.PORTRAIT_PRIMARY
              );
            }
          }
        }
      });
    } else {
      // Compass theme is used for default theme
      document.body.style.setProperty('--radio-checked', '#E78B29');
      document.body.style.setProperty('--radio-unchecked', '#000000');
      document.body.style.setProperty('--title', '#E78B29');
      document.body.style.setProperty('--sub-title', '#3B3D3E');
      document.body.style.setProperty('--submit-button-color', '#505050');
      document.body.style.setProperty('--question-font-color', '#000000');
      document.body.style.setProperty(
        '--theme-img',
        'url("./assets/imgs/background.png")'
      );
      document.body.style.setProperty(
        '--thank-you-image',
        'url("./assets/imgs/tick_compass.png")'
      );
      if (!that.isDesktopBrowser) {
        this.screenOrientation.lock(
          this.screenOrientation.ORIENTATIONS.LANDSCAPE_PRIMARY
        );
      }
    }
  }

  resetThemeVariables() {
    document.body.style.removeProperty('--radio-checked');
    document.body.style.removeProperty('--radio-unchecked');
    document.body.style.removeProperty('--title');
    document.body.style.removeProperty('--sub-title');
    document.body.style.removeProperty('--submit-button-color');
    document.body.style.removeProperty('--theme-img');
    document.body.style.removeProperty('--thank-you-image');
    document.body.style.removeProperty('--question-font-color');
    if (!this.isDesktopBrowser) {
      this.screenOrientation.lock(
        this.screenOrientation.ORIENTATIONS.PORTRAIT_PRIMARY
      );
    }
  }
}
