import { Component, OnInit } from '@angular/core';
import { User, Loggedinuser } from '../../entities';
import {
  HelperService,
  NetworkService,
  LoginService,
  SyncService,
  SurveyService
} from '../../services';
import { Router } from '@angular/router';
import { Device } from '@ionic-native/device/ngx';
import { Platform } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  public user: User = new User();
  private loggedInUser = new Loggedinuser();
  public loading: any;
  public syncCount = 0;
  public lastSyncTime = localStorage.getItem('lastSyncTime');
  private stopTimer: boolean;
  private readonly userPagePath = '/user';
  showFooter = false;
  packageName;
  versionNumber;

  constructor(
    private loginService: LoginService,
    private syncService: SyncService,
    private helperService: HelperService,
    private networkService: NetworkService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private device: Device,
    private platform: Platform,
    private surveyService: SurveyService,
    private appVersion: AppVersion
  ) {}

  ngOnInit() {
    this.syncService.getSyncLeftCount().then((result: any) => {
      this.syncCount = result.rows.item(0).syncLeftCount;
    });
    this.stopTimer = false;
    this.setTimeOut();
    this.setAppInfo();
  }

  authenticateUser(): void {
    if (this.networkService.isAvailiable) {
      this.showLoader();
      if (this.platform.is('cordova') && !this.platform.is('desktop')) {
        this.user.device.Uuid = this.device.uuid;
        this.user.device.Platform = this.device.platform;
        this.user.device.Manufacturer = this.device.manufacturer;
        this.user.device.Model = this.device.model;
        this.user.device.OsVersion = this.device.version;
        this.user.device.RegistrationId = '';
      }
      const deviceDetail = JSON.stringify(this.user.device);
      this.loginService
        .authenticateUser(this.user.UserName, this.user.Password, deviceDetail)
        .subscribe(
          (data: any) => {
            const userDetails = {
              username: data.Username,
              name: data.Name,
              apiToken: data.access_token,
              refreshToken: data.refresh_token,
              tokenExpiryTime: new Date().setSeconds(data.expires_in),
              refreshTokenExpiryTime: new Date().setHours(
                24 * data.RefreshTokenExpiry
              ),
              isADUser: data.IsADUser,
              isLocationSearchAllowed: data.IsLocationSearchAllowed,
              loginTime: new Date().getTime()
            };
            localStorage.setItem('username', data.Username);
            this.loggedInUser.name = data.Name;
            this.loggedInUser.user_name = data.Username;
            this.loggedInUser.user_data = userDetails;
            this.loggedInUser.user_id = 1;
            this.loginService
              .setUser(this.loggedInUser)
              .then(() => {
                this.loadingCtrl.dismiss();
                this.helperService.setAccessToken(data.access_token);
                localStorage.setItem('refreshToken', data.refresh_token);
                localStorage.setItem(
                  'tokenExpiryTime',
                  userDetails.tokenExpiryTime.toString()
                );
                localStorage.setItem(
                  'refreshTokenExpiryTime',
                  userDetails.refreshTokenExpiryTime.toString()
                );
                this.loginService.getCurrentUserInfo().subscribe(userInfo => {
                  if (
                    userInfo &&
                    userInfo.Data &&
                    userInfo.Data.SelectedSector
                  ) {
                    localStorage.setItem(
                      'selectedSectorId',
                      userInfo.Data.SelectedSector.Id
                    );
                  }
                  this.router.navigate(['/home']);
                });
              })
              .catch(() => {
                this.loadingCtrl.dismiss();
              });
          },
          (error: any) => {
            this.loadingCtrl.dismiss();
            console.error('Login Page error', error);
            this.helperService.showAlert('Invalid Username or Password!');
          }
        );
    } else {
      this.helperService.showAlert(
        'No internet connection available. Please check your settings.'
      );
    }
  }

  async showLoader() {
    this.loading = await this.loadingCtrl.create({ message: 'Please wait...' });
    await this.loading.present();
  }

  setTimeOut() {
    let surveyIdLocalStorage = localStorage.getItem('latestSurveyId');
    Observable.interval(45000)
      .skipWhile(
        () =>
          !surveyIdLocalStorage ||
          Number(surveyIdLocalStorage) === 0 ||
          this.stopTimer
      )
      .subscribe(() => {
        if (
          this.router.url.indexOf('/login') > -1 &&
          surveyIdLocalStorage &&
          Number(surveyIdLocalStorage) > 0 &&
          moment().diff(this.surveyService.logoutTime, 'seconds') > 45
        ) {
          surveyIdLocalStorage = localStorage.getItem('latestSurveyId');
          this.stopTimer = true;
          this.router.navigate([
            this.userPagePath,
            Number(surveyIdLocalStorage)
          ]);
        }
      });
  }

  setAppInfo() {
    if (this.platform.is('cordova')) {
      this.showFooter = true;
      this.platform.ready().then(() => {
        this.appVersion.getPackageName().then(packageName => {
          this.packageName = packageName;
        });
        this.appVersion.getVersionNumber().then(version => {
          this.versionNumber = version;
        });
      });
    }
  }
}
