import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import { SurveyService, SignatureService } from '../../../core/services';
import { ModalController } from '@ionic/angular';
import { SignatureModalComponent } from './components/signature-modal/signature-modal.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-signature',
  templateUrl: './signature.component.html',
  styleUrls: ['./signature.component.scss']
})
export class SignatureComponent implements OnInit, OnDestroy {
  @Input() surveyItem: any;
  @ViewChild(SignatureModalComponent)
  public signatureModalComponent: SignatureModalComponent;
  public loopingIndex = 0;
  resultOptions: any = [];
  signatureImage: string;
  signatueSubscription: Subscription;
  constructor(
    private surveyService: SurveyService,
    private modalCtrl: ModalController,
    private signatureService: SignatureService
  ) {}

  ngOnInit() {
    if (!this.surveyItem.Result) {
      this.initializeSurveyItemResult();
      this.signatureService.setSignatureImage('');
    }
    this.resultOptions = JSON.parse(this.surveyItem.ResultOptions);
    this.signatueSubscription = this.signatureService.signatureImage.subscribe(
      data => {
        this.signatureImage = data;
        this.surveyItem.Result.ResultText = data;
      }
    );
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.signatueSubscription.unsubscribe();
  }

  public initializeSurveyItemResult() {
    this.surveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      this.surveyItem.SurveyItemID,
      this.loopingIndex
    );
  }

  public async openSignatureModal() {
    this.signatureService.modal = await this.modalCtrl.create({
      component: SignatureModalComponent,
      cssClass: 'signature',
      backdropDismiss: false
    });
    await this.signatureService.modal.present();
  }
}
