import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  AfterViewInit
} from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { NavController, NavParams } from '@ionic/angular';
import { SignatureService } from '../../../../services';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-signature-modal',
  templateUrl: './signature-modal.component.html',
  styleUrls: ['./signature-modal.component.scss']
})
export class SignatureModalComponent
  implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(SignaturePad) public signaturePad: SignaturePad;
  signaturePadOptions: Object = {
    // passed through to szimek/signature_pad constructor
    minWidth: 5,
    canvasWidth: 500,
    canvasHeight: 130
  };
  public signatureImage: string;
  signatueSubscription: Subscription;
  constructor(
    private signatureService: SignatureService,
    public navParams: NavController,
    public navCtrl: NavController
  ) {}

  ngOnInit() {
    this.signatueSubscription = this.signatureService.signatureImage.subscribe(
      data => {
        this.signatureImage = data;
      }
    );
  }

  ngAfterViewInit() {
    if (this.signaturePad) {
      this.signaturePad.fromDataURL(this.signatureImage);
    }
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.signatueSubscription.unsubscribe();
  }

  drawComplete() {
    // this.signatureService.signatureImage = this.signaturePad.toDataURL();
    const isEmpty = this.signaturePad.isEmpty();
    console.log(isEmpty);
    if (!isEmpty) {
      this.signatureService.setSignatureImage(this.signaturePad.toDataURL());
    } else {
      this.signatureService.setSignatureImage('');
    }
    this.closeSignature();
  }

  clearSignature() {
    this.signaturePad.clear();
  }

  closeSignature() {
    this.signatureService.modal.dismiss();
  }
}
