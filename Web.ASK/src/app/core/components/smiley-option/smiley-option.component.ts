import {
  Component,
  Input,
  OnInit,
  OnChanges,
  EventEmitter,
  Output
} from '@angular/core';

@Component({
  selector: 'app-smiley-option',
  templateUrl: './smiley-option.component.html',
  styleUrls: ['./smiley-option.component.scss']
})
export class SmileyOptionComponent {
  @Input() surveyItem;
  @Input() resultOptionsForCustomSurvey;
  @Output() smileyOptionChange: EventEmitter<any> = new EventEmitter();

  // emit event on change of smiley selection dropdown
  onSmileyOptionChange(resultOptionTypeId) {
    this.smileyOptionChange.emit(resultOptionTypeId);
  }
}
