import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SurveyService } from '../../../core/services';

@Component({
  selector: 'app-radio-vertical',
  templateUrl: './radio-vertical.component.html',
  styleUrls: ['./radio-vertical.component.scss']
})
export class RadioVerticalComponent implements OnInit {
  @Input() surveyItem: any;
  @Input() isKiosk: boolean;
  @Output() resultOptionSelected: EventEmitter<any> = new EventEmitter<any>();
  resultOptions: any = [];
  constructor(private surveyService: SurveyService) {}

  ngOnInit() {
    if (!this.surveyItem.Result) {
      this.initializeSurveyItemResult();
    }
    this.resultOptions = JSON.parse(this.surveyItem.ResultOptions);
  }

  public initializeSurveyItemResult() {
    this.surveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      this.surveyItem.SurveyItemID
    );
  }

  changeSurveyItemResult(resultOption) {
    this.surveyItem.Result.ResultOptionId = resultOption.ResultOptionId;
    this.resultOptionSelected.emit({
      surveyItem: this.surveyItem,
      selectedResultOptionId: resultOption.ResultOptionId
    });
  }
}
