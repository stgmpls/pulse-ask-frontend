import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { SurveyService } from '../../services';
import * as moment from 'moment';
import { from } from 'rxjs';

@Component({
  selector: 'app-currency-input',
  templateUrl: './currency-input.component.html',
  styleUrls: ['./currency-input.component.scss']
})
export class CurrencyInputComponent implements OnInit, OnChanges {
  @Input() surveyItem: any;
  public value: any;
  public validationError = false;
  public validationErrorText = 'Please enter valid input';
  constructor(
    private surveyService: SurveyService,
    public currencyPipe: CurrencyPipe
  ) {}

  ngOnInit() {
    if (!this.surveyItem.Result) {
      this.initializeSurveyItemResult();
    }
    this.value = this.surveyItem.Result.ResultText;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.surveyItem && this.surveyItem.Result) {
      this.value = parseInt(this.surveyItem.Result.ResultText, 10);
    }
  }

  public initializeSurveyItemResult() {
    this.surveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      this.surveyItem.SurveyItemID
    );
  }

  onValueChange(value) {
    let currencyVal = '';
    this.validationError = false;
    try {
      value = value.replace(/[$,]/g, '');
      currencyVal = this.currencyPipe.transform(value);
      this.surveyItem.Result.ResultText = value;
      this.value = value;
    } catch (error) {
      this.value = currencyVal;
      this.surveyItem.Result.ResultText = currencyVal;
      this.validationError = true;
    }
  }
}
