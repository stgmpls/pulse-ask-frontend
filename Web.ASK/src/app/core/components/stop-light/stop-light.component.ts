import { Component, OnInit, Input } from '@angular/core';
import { SurveyService } from '../../../core/services';

@Component({
  selector: 'app-stop-light',
  templateUrl: './stop-light.component.html',
  styleUrls: ['./stop-light.component.scss']
})
export class StopLightComponent implements OnInit {
  @Input() surveyItem: any;

  resultOptions: any = [];
  constructor(private surveyService: SurveyService) {}

  ngOnInit() {
    if (!this.surveyItem.Result) {
      this.initializeSurveyItemResult();
    }
    this.resultOptions = JSON.parse(this.surveyItem.ResultOptions);
  }

  public initializeSurveyItemResult() {
    this.surveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      this.surveyItem.SurveyItemID
    );
  }

  changeSurveyItemResult(resultOption) {
    this.surveyItem.Result.ResultOptionId = resultOption.ResultOptionId;
  }
}
