import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SurveyService } from '../../../core/services';

@Component({
  selector: 'app-comment-formatted',
  templateUrl: './comment-formatted.component.html',
  styleUrls: ['./comment-formatted.component.scss']
})
export class CommentFormattedComponent implements OnInit {
  @Input() surveyItem: any;
  constructor(private surveyService: SurveyService) {}

  ngOnInit() {
    this.initializeSurveyItemResult();
  }

  public initializeSurveyItemResult() {
    this.surveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      this.surveyItem.SurveyItemID
    );

    this.surveyItem.Result.ResultText = this.surveyItem.Text;
  }
}
