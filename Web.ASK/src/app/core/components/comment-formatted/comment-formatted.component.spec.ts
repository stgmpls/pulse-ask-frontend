import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentFormattedComponent } from './comment-formatted.component';

describe('CommentFormattedComponent', () => {
  let component: CommentFormattedComponent;
  let fixture: ComponentFixture<CommentFormattedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentFormattedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentFormattedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
