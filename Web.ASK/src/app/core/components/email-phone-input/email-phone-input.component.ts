import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SurveyService } from '../../services';

@Component({
  selector: 'app-email-phone-input',
  templateUrl: './email-phone-input.component.html',
  styleUrls: ['./email-phone-input.component.scss']
})
export class EmailPhoneInputComponent implements OnInit {
  @Input() surveyItem: any;
  @Input() inputDataType: string;
  public emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  public phonePattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
  public validationError = false;
  public validationErrorText = '';

  constructor(private surveyService: SurveyService) {}

  ngOnInit() {
    if (!this.surveyItem.Result) {
      this.initializeSurveyItemResult();
    }
    this.validationErrorText =
      this.inputDataType == 'email'
        ? 'Please enter valid email'
        : 'Please enter valid phone';
  }

  public initializeSurveyItemResult() {
    this.surveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      this.surveyItem.SurveyItemID
    );
  }

  public validateInput() {
    this.validationError = false;
    switch (this.inputDataType) {
      case 'email':
        if (!this.emailPattern.test(this.surveyItem.Result.ResultText)) {
          this.validationError = true;
          this.surveyItem.Result.ResultText = '';
        }
        break;
      case 'number':
        if (!this.phonePattern.test(this.surveyItem.Result.ResultText)) {
          this.validationError = true;
          this.surveyItem.Result.ResultText = '';
        }
        break;

      default:
        this.validationError = false;
    }
  }
}
