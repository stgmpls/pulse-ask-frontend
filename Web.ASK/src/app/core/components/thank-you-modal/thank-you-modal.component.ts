import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { AppState } from '../../../core/services/global.service';

@Component({
  selector: 'app-thank-you-modal',
  templateUrl: './thank-you-modal.component.html',
  styleUrls: ['./thank-you-modal.component.scss']
})
export class ThankYouModalComponent implements OnInit {
  items: any;
  message: any;
  question: any;
  constructor(public global: AppState, private navParams: NavParams) {
    this.message = 'test';
  }

  ngOnInit() {}
}
