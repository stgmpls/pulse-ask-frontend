import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SurveyService } from '../../../core/services';
import { MAX_LENGTH_FOR_RENDERING_AS_TEXT_BOX } from '../../../core/constant/constants';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss']
})
export class TextInputComponent implements OnInit {
  @Input() surveyItem: any;
  public loopingIndex = 0;
  surveyDescription: any;
  @Input() placeHolder = '';
  @Input() isAdmin = false;
  public maxLengthForRenderingAsTextBox = MAX_LENGTH_FOR_RENDERING_AS_TEXT_BOX;
  @Input() isInitilized = true;
  constructor(private surveyService: SurveyService) {}

  ngOnInit() {
    this.surveyDescription = this.surveyService.surveyDescription;
    if (!this.isInitilized || !this.surveyItem.Result) {
      this.initializeSurveyItemResult();
    }
  }

  public initializeSurveyItemResult() {
    this.surveyItem.Result = this.surveyService.createSurveyItemResultInstance(
      this.surveyItem.SurveyItemID
    );
    if (this.surveyService.isEditMode) {
      this.surveyItem.Result.ResultText = this.surveyItem.Text;
    } else if (
      this.surveyItem.ResultTextMaxLength <=
        this.maxLengthForRenderingAsTextBox &&
      this.isAdmin
    ) {
      this.surveyItem.Result.ResultText = this.surveyDescription;
    }
  }
}
