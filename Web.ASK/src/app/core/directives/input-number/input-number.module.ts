import { NgModule } from '@angular/core';
import { InputNumberCheckDirective } from './input-number.directive';

@NgModule({
  imports: [],
  declarations: [InputNumberCheckDirective],
  exports: [InputNumberCheckDirective]
})
export class InputNumberCheckModule {}
