import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appInputNumberCheck]'
})
export class InputNumberCheckDirective {
  constructor() {}
  @HostListener('paste') onPaste() {
    return false;
  }

  @HostListener('keydown', ['$event']) onKeydown(e) {
    const key = e.charCode || e.keyCode || 0;
    return (
      key === 8 ||
      key === 9 ||
      key === 13 ||
      key === 46 ||
      (key >= 35 && key <= 40) ||
      (key >= 96 && key <= 105) ||
      (key >= 48 && key <= 57 && !isNaN(e.key))
    );
  }
}
