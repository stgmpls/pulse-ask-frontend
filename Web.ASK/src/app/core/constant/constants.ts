import { FormConfig } from '../entities/search-location-form-config';

export const USER_INFO_COLLECTION = 'userInfo';
export const BATCH_COUNT = 500;
export const STATES_ARRAY = [
  {
    name: 'Select State',
    abbreviation: ''
  },
  {
    name: 'Alabama',
    abbreviation: 'AL'
  },
  {
    name: 'Alaska',
    abbreviation: 'AK'
  },
  {
    name: 'American Samoa',
    abbreviation: 'AS'
  },
  {
    name: 'Arizona',
    abbreviation: 'AZ'
  },
  {
    name: 'Arkansas',
    abbreviation: 'AR'
  },
  {
    name: 'California',
    abbreviation: 'CA'
  },
  {
    name: 'Colorado',
    abbreviation: 'CO'
  },
  {
    name: 'Connecticut',
    abbreviation: 'CT'
  },
  {
    name: 'Delaware',
    abbreviation: 'DE'
  },
  {
    name: 'District Of Columbia',
    abbreviation: 'DC'
  },
  {
    name: 'Federated States Of Micronesia',
    abbreviation: 'FM'
  },
  {
    name: 'Florida',
    abbreviation: 'FL'
  },
  {
    name: 'Georgia',
    abbreviation: 'GA'
  },
  {
    name: 'Guam',
    abbreviation: 'GU'
  },
  {
    name: 'Hawaii',
    abbreviation: 'HI'
  },
  {
    name: 'Idaho',
    abbreviation: 'ID'
  },
  {
    name: 'Illinois',
    abbreviation: 'IL'
  },
  {
    name: 'Indiana',
    abbreviation: 'IN'
  },
  {
    name: 'Iowa',
    abbreviation: 'IA'
  },
  {
    name: 'Kansas',
    abbreviation: 'KS'
  },
  {
    name: 'Kentucky',
    abbreviation: 'KY'
  },
  {
    name: 'Louisiana',
    abbreviation: 'LA'
  },
  {
    name: 'Maine',
    abbreviation: 'ME'
  },
  {
    name: 'Marshall Islands',
    abbreviation: 'MH'
  },
  {
    name: 'Maryland',
    abbreviation: 'MD'
  },
  {
    name: 'Massachusetts',
    abbreviation: 'MA'
  },
  {
    name: 'Michigan',
    abbreviation: 'MI'
  },
  {
    name: 'Minnesota',
    abbreviation: 'MN'
  },
  {
    name: 'Mississippi',
    abbreviation: 'MS'
  },
  {
    name: 'Missouri',
    abbreviation: 'MO'
  },
  {
    name: 'Montana',
    abbreviation: 'MT'
  },
  {
    name: 'Nebraska',
    abbreviation: 'NE'
  },
  {
    name: 'Nevada',
    abbreviation: 'NV'
  },
  {
    name: 'New Hampshire',
    abbreviation: 'NH'
  },
  {
    name: 'New Jersey',
    abbreviation: 'NJ'
  },
  {
    name: 'New Mexico',
    abbreviation: 'NM'
  },
  {
    name: 'New York',
    abbreviation: 'NY'
  },
  {
    name: 'North Carolina',
    abbreviation: 'NC'
  },
  {
    name: 'North Dakota',
    abbreviation: 'ND'
  },
  {
    name: 'Northern Mariana Islands',
    abbreviation: 'MP'
  },
  {
    name: 'Ohio',
    abbreviation: 'OH'
  },
  {
    name: 'Oklahoma',
    abbreviation: 'OK'
  },
  {
    name: 'Oregon',
    abbreviation: 'OR'
  },
  {
    name: 'Palau',
    abbreviation: 'PW'
  },
  {
    name: 'Pennsylvania',
    abbreviation: 'PA'
  },
  {
    name: 'Puerto Rico',
    abbreviation: 'PR'
  },
  {
    name: 'Rhode Island',
    abbreviation: 'RI'
  },
  {
    name: 'South Carolina',
    abbreviation: 'SC'
  },
  {
    name: 'South Dakota',
    abbreviation: 'SD'
  },
  {
    name: 'Tennessee',
    abbreviation: 'TN'
  },
  {
    name: 'Texas',
    abbreviation: 'TX'
  },
  {
    name: 'Utah',
    abbreviation: 'UT'
  },
  {
    name: 'Vermont',
    abbreviation: 'VT'
  },
  {
    name: 'Virgin Islands',
    abbreviation: 'VI'
  },
  {
    name: 'Virginia',
    abbreviation: 'VA'
  },
  {
    name: 'Washington',
    abbreviation: 'WA'
  },
  {
    name: 'West Virginia',
    abbreviation: 'WV'
  },
  {
    name: 'Wisconsin',
    abbreviation: 'WI'
  },
  {
    name: 'Wyoming',
    abbreviation: 'WY'
  }
];
export const NUMBER_OF_SMILEY_ARRAY = [
  {
    smileyNumber: 2,
    type: '',
    emotions: ['Very Happy, ', 'Very Unhappy']
  },
  {
    smileyNumber: 3,
    type: '',
    emotions: ['Very Happy, ', 'Satisfied, ', 'Very Unhappy']
  },
  {
    smileyNumber: 4,
    type: 'Default',
    emotions: ['Very Happy, ', 'Happy, ', 'Unhappy, ', 'Very Unhappy']
  },
  {
    smileyNumber: 5,
    type: '',
    emotions: [
      'Very Happy, ',
      'Happy, ',
      'Satisfied, ',
      'Unhappy, ',
      'Very Unhappy'
    ]
  }
];

export const LOC_FORM_CONFIG: FormConfig = {
  costCenterNumber: {
    size: '4',
    placeholder: 'Cost Center #',
    defaultValue: ''
  },
  costCenterName: {
    size: '8',
    placeholder: 'Cost Center Name',
    defaultValue: ''
  },
  zipCode: {
    size: '4',
    placeholder: 'Zip Code',
    defaultValue: ''
  },
  state: {
    size: '8',
    placeholder: 'Select State',
    defaultValue: ' '
  },
  city: {
    size: '12',
    placeholder: 'City',
    defaultValue: ''
  },
  searchBtn: {
    placeholder: 'SEARCH',
    size: '12'
  }
};
export const TIMEZONE = 'America/New_York';

export const MAX_LENGTH_FOR_RENDERING_AS_TEXT_BOX = 50;

export enum SURVEY_TYPE {
  PREDEFINED = 'Predefined',
  TASTING = 'Tasting',
  CUSTOM = 'Custom',
  ACUITY = 'Acuity Connect Content'
}
