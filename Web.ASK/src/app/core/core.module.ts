import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { InputNumberCheckModule } from './directives/input-number/input-number.module';
import { SignaturePadModule } from 'angular2-signaturepad';
import { IonicRatingModule } from 'ionic4-rating';
import { CurrencyPipe } from '@angular/common';

import {
  AuthenticationService,
  AuthGuardService,
  HelperService,
  APIInterceptor,
  DbService,
  DataService
} from './services';

import {
  LoginPage,
  AccordionModule,
  SmileyListModule,
  SmileyOptionComponent,
  TimeInputComponent
} from './components';
import { Routes, RouterModule } from '@angular/router';
import {
  TextInputComponent,
  SmileyBoxComponent,
  ThankYouModalComponent,
  FollowUpComponent,
  MultiSelectComponent,
  LabelButtonComponent,
  SurveyComponent,
  CommentFormattedComponent,
  RadioVerticalComponent,
  StarRatingComponent,
  CheckboxComponent,
  CurrencyInputComponent,
  EmailPhoneInputComponent,
  DateInputComponent,
  StopLightComponent,
  SignatureComponent,
  SignatureModalComponent,
  ProgressBarComponent
} from './components';
import { LocationSearchModule } from './components/location/location-search';
import { OrderByPipe } from './pipes/order-by.pipe';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { ResultOptionComponent } from './components/result-option/result-option.component';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
@NgModule({
  declarations: [
    LoginPage,
    TextInputComponent,
    SmileyBoxComponent,
    ThankYouModalComponent,
    FollowUpComponent,
    MultiSelectComponent,
    LabelButtonComponent,
    SurveyComponent,
    OrderByPipe,
    CommentFormattedComponent,
    RadioVerticalComponent,
    StarRatingComponent,
    CheckboxComponent,
    CurrencyInputComponent,
    EmailPhoneInputComponent,
    DateInputComponent,
    StopLightComponent,
    SignatureComponent,
    SignatureModalComponent,
    SmileyOptionComponent,
    ResultOptionComponent,
    TimeInputComponent,
    ProgressBarComponent
  ],
  providers: [
    AuthenticationService,
    AuthGuardService,
    HelperService,
    APIInterceptor,
    DbService,
    DataService,
    AppVersion,
    OrderByPipe,
    CurrencyPipe,
    ScreenOrientation
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeviceDetectorModule.forRoot(),
    LocationSearchModule,
    InputNumberCheckModule,
    AccordionModule,
    SignaturePadModule,
    IonicRatingModule,
    SmileyListModule
  ],
  exports: [
    LoginPage,
    TextInputComponent,
    SmileyBoxComponent,
    ThankYouModalComponent,
    FollowUpComponent,
    MultiSelectComponent,
    LabelButtonComponent,
    LocationSearchModule,
    SurveyComponent,
    OrderByPipe,
    CommentFormattedComponent,
    RadioVerticalComponent,
    StarRatingComponent,
    InputNumberCheckModule,
    CheckboxComponent,
    CurrencyInputComponent,
    EmailPhoneInputComponent,
    DateInputComponent,
    StopLightComponent,
    SignatureComponent,
    SignatureModalComponent,
    InputNumberCheckModule,
    AccordionModule,
    SmileyListModule,
    SmileyOptionComponent,
    ResultOptionComponent,
    TimeInputComponent,
    ProgressBarComponent
  ],
  entryComponents: [ThankYouModalComponent, SignatureModalComponent]
})
export class CoreModule {}
