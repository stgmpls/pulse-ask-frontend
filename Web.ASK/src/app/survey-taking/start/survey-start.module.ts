import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CoreModule } from '../../core';

import { IonicModule } from '@ionic/angular';

import { SurveyStartPage } from './survey-start.page';

const routes: Routes = [
  {
    path: '',
    component: SurveyStartPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoreModule,
    RouterModule.forChild(routes),
  ],
  declarations: [SurveyStartPage],
})
export class SurveyStartPageModule {}
