import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CoreModule } from '../../core';

import { IonicModule } from '@ionic/angular';

import { UserResponsePage } from './user-response.page';

const routes: Routes = [
  {
    path: '',
    component: UserResponsePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoreModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UserResponsePage]
})
export class UserResponsePageModule {}
