import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SurveyService } from 'src/app/core/services';
import { IonContent } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-userresponse',
  templateUrl: './user-response.page.html',
  styleUrls: ['./user-response.page.scss']
})
export class UserResponsePage implements OnInit {
  public surveyId: any;
  public surveyItems: any = [];
  @ViewChild(IonContent) content: IonContent;
  constructor(
    private activatedRoute: ActivatedRoute,
    private surveyService: SurveyService,
    public screenOrientation: ScreenOrientation
  ) {}

  ngOnInit() {
    this.surveyItems = [];
    this.activatedRoute.params.subscribe(params => {
      this.surveyId = params['id'];
      localStorage.setItem('latestSurveyId', this.surveyId);
    });
  }

  public onDisplayGroupSelectionChanged($event) {
    this.content.scrollToTop(1000);
  }
}
