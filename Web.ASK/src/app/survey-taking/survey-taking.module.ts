import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SurveyTakingModule } from './survey-taking-routing.module';
import { IonicModule } from '@ionic/angular';
import { SurveyTakingPage } from './survey-taking.page';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, SurveyTakingModule],
  declarations: [SurveyTakingPage],
  providers: [ScreenOrientation]
})
export class SurveyTakingPageModule {}
