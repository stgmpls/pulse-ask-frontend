import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SurveyTakingPage } from './survey-taking.page';

const routes: Routes = [
  {
    path: '',
    component: SurveyTakingPage,
    children: [
      {
        path: 'user-response',

        children: [
          {
            path: '',
            loadChildren:
              './user-response/user-response.module#UserResponsePageModule',
            data: { preload: true }
          },
          {
            path: ':id',
            loadChildren:
              './user-response/user-response.module#UserResponsePageModule',
            data: { preload: true }
          }
        ]
      },
      {
        path: '',
        redirectTo: 'user-response',
        pathMatch: 'full'
      },
      {
        path: ':id',
        redirectTo: 'user-response/:id',
        pathMatch: 'full'
      },
      {
        path: ':id/start',
        loadChildren: './start/survey-start.module#SurveyStartPageModule',
        data: { preload: true },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SurveyTakingModule {}
