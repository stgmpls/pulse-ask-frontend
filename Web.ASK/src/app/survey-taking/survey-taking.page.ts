import { Component, OnInit } from '@angular/core';
import { AppState } from '../core/services/global.service';
import { SurveySetupService } from '../survey-setup/survey-setup.service';
import { NavController, Platform } from '@ionic/angular';
import { SurveyService } from '../core/services';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
@Component({
  selector: 'app-user',
  templateUrl: './survey-taking.page.html',
  styleUrls: ['./survey-taking.page.scss']
})
export class SurveyTakingPage implements OnInit {
  public clickCounter = 0;
  public isDesktopBrowser = false;
  constructor(
    public global: AppState,
    private surveySetupService: SurveySetupService,
    private router: Router,
    private surveyService: SurveyService,
    private screenOrientation: ScreenOrientation,
    private platform: Platform
  ) {
    this.clickCounter = 0;
    if (this.platform.is('desktop')) {
      this.isDesktopBrowser = true;
    } else {
      this.isDesktopBrowser = false;
    }
  }

  ngOnInit() {
    this.global.set('theme', localStorage.getItem('theme'));
  }

  logout() {
    this.clickCounter += 1;
    if (this.clickCounter === 5) {
      this.surveySetupService.logout().then((data: any) => {
        if (!this.isDesktopBrowser) {
          this.screenOrientation.lock(
            this.screenOrientation.ORIENTATIONS.LANDSCAPE_PRIMARY
          );
        }
        this.surveyService.surveyData = [];
        this.surveySetupService.selectTypeOfSurvey('');
        this.surveySetupService.selectSurveysByCategory([]);
        this.surveyService.logoutTime = moment();
        this.router.navigate(['/login']);
      });
    }
  }
}
