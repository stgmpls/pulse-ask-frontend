## Local Development Environment Setup (for windows system)

1. Install latest Node.js(http://nodejs.org/). This should also install `npm`.
2. Install latest [Git CLI](https://git-scm.com/downloads/). While installing make sure to select below options,
   - Use Git from the Windows Command Prompt
   - Use Windows default console window
   - Enable file system caching
3. Install [Java Development Kit (JDK)](https://cordova.apache.org/docs/en/latest/guide/platforms/android/#java-development-kit-jdk).
4. Setup the Web API in local IIS as per its [Readme.md](https://bitbucket.org/stgmpls/acuityconnect-backend/src/develop/Compass.Client.WebApi/README.md)
5. Install latest ionic 'npm install -g ionic'
6. Install Python 2.7 (https://www.python.org/download/releases/2.7/)
7. Install latest cordova 'npm install -g cordova'
8. Open a command window as administrator
   1. Get the latest code from bitbucket repo with command git clone. Source https://bitbucket.org/stgmpls/pulse-ask-frontend/src/master/
   2. Enter code directory with command cd pulse-ask-frontend
   3. Get the latest code from Dev branch since master branch is empty, with command git pull origin Dev
   4. Enter the folder Web.Ask using command cd Web.Ask
   5. Execute command: ionic cordova prepare android --verbose to restore packages and add android platform. The --verbose
      flag prints debugging messages which may narrow down the issue, if any. During the execution of this command,
      it may ask permission for installing a few dependencies like android platform and @angular/cli. These
      permissions should be granted for the successful execution of this command.
   6. If you get some errors with a specific package after running ionic cordova prepare try to run npm install for a specific package. For example 'npm install node-sass'
9. If you want to run it through Ionic locally use following command: 'ionic serve'

## Android App - Local Testing (for windows system)

1. Install the latest [Android Studio](https://developer.android.com/studio/).
2. Install [Android SDK 16](https://cordova.apache.org/docs/en/latest/guide/platforms/android/#adding-sdk-packages).
3. Set up local [environment variable](https://cordova.apache.org/docs/en/latest/guide/platforms/android/#windows) for Android SDK.
4. Create a [Android Virtual Device](https://developer.android.com/studio/run/managing-avds) for testing.
5. Install [Gradle](https://gradle.org/install/)
6. Run `ionic cordova build android --configuration=demo` to generate the APK file using demo environment file (environment.demo.ts)
7. Run `ionic cordova emulate android --configuration=demo` to build and run the application in emulator using demo environment file (environment.demo.ts)

## Running and Debugging via VSCode

1.)Install VSCode [Visual Studio Code](https://code.visualstudio.com/)
2.)In VSCode Extention, install `Debugger for Chrome` and `Cordova Tools`
3.)In VSCode Run and Debug(Icon like a literal bug) and make sure a launch.json is set up:

"version": "0.2.0",
"configurations": [
{
"type": "chrome",
"request": "launch",
"name": "Launch Chrome against localhost",
"url": "http://localhost:8100",
"webRoot": "${workspaceFolder}"
}
]
}
4.) Make sure the port in the launch.json is the same as the port ionic is using.
